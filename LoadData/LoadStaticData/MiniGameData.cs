﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;


namespace LoadData
{
    public class BinsData
    {
        public string type;
    }

    public class GarbageData
    {
        public string type;
    }

    public class MiniGameData
    {
        public string type;
        public List<GarbageData> garbageList;
        public List<BinsData> binsList;
    }
}
