﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LoadData
{

    public class KitInfo
    {
        public String typeKit;
        public float plastics_percent;
        public float glass_percent;
        public float paper_percent;
        public float food_percent;
        public float cans_percent;
    }

    public class KitData
    {
        public int TotalKit;
        public List<KitInfo> Kits;
    }
}
