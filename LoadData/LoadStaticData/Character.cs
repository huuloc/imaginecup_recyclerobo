﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LoadData
{
    public class MonsterData
    {
        public string name;
        public int amount;
        public float speed;
        public float health;
        public float damage;
    }

    public class RoboBaseData
    {
        public string name;
        public int health;
        public float coefficientHealth;
        public int speed;
        public float coefficientSpeed;
    }

    public class AttackerBaseData : RoboBaseData
    {
        public int damage;
        public float coefficientDamage;
    }

    public class RangerBaseData : AttackerBaseData
    {
        public int range;
        public float coefficientRange;
    }

    public class HealerBaseData : RoboBaseData
    {
        public int healAmount;
        public float coefficientHealAmount;
    }

    public class NumberOneBase : AttackerBaseData
    {
    }
    public class NumberTwoBase : RangerBaseData
    {
    }
    public class NumberThreeBase : AttackerBaseData
    {
    }
    public class NumberFourBase : HealerBaseData
    {
    }
    public class NumberFiveBase : RangerBaseData
    {
    }
}
