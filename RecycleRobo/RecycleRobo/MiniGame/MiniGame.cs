using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace RecycleRobo
{
    public abstract class MiniGame
    {
        public static Vector2 TargetEffectPos = new Vector2(938, 583);
        public static AnimationPlayer effect;
        public static Animation energyBall = GlobalClass.getAnimationByName("ENERGY_BALL");
        public static Dictionary<string, int> typeNAmount;
        public MiniGame() { ;}
        public static void InitEffect()
        {
            effect.OrderLayer = 0.0096f;
            effect.Scale = 0.625f;
            effect.color = Color.White;
        }
        public abstract void Play(GameTime gameTime);
        public abstract void Draw(SpriteBatch spriteBatch, GameTime gameTime);
        public abstract bool isWin();
        public abstract int calculateEnergyBall();
    }
}
