﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LoadData;
using RecycleRobo.Scenes;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using RecycleRobo.MGame.Catcher;
namespace RecycleRobo
{
    public class LoadCatcher
    {
        GameManager game;
        string[] typeGarbage = { "compost0", "glass0", "compost1", "metal1", "metal0", "paper0", "plastic0", "compost1", "glass1","plastic1" };
        public LoadCatcher(GameManager g) { game = g; }
        public void loadData(List<List<Garbage>> garbageList, List<Bins> binsList)
        {
            List<Garbage> gbList = new List<Garbage>();

            Random rd = new Random();
            int type = rd.Next(0,4);
            binsList.Add(new Bins(this.game, Bins.typeBinInit[type], new Vector2(300, GlobalClass.ScreenHeight - 150)));
            binsList.Add(new Bins(this.game, Bins.typeBinInit[(type+1) % 5], new Vector2(500, GlobalClass.ScreenHeight - 280)));


            List<Vector2> coordsOne = Enumerable.Range(0, 5).Select(i => new Vector2(rd.Next(300, 850), -rd.Next(100))).ToList();
            List<string> type_Gb = new List<string>();
            type_Gb.Add(Bins.typeBinInit[type]);
            type_Gb.Add(Bins.typeBinInit[(type+1) % 5]);


            List<string> typeGb = Enumerable.Range(0, 10).Select(k => type_Gb[rd.Next(type_Gb.Count)] + Convert.ToString(rd.Next(8))).ToList();

            for (int i = 0; i < rd.Next(1, 4); i++)
            {
                gbList.Add(new Garbage(game, typeGb[i], coordsOne[i], rd.Next(3, 5), rd.Next(5, 10)));
            }
            garbageList.Add(gbList);
            gbList = new List<Garbage>();

            typeGb = Enumerable.Range(0, 5).Select(k => type_Gb[rd.Next(type_Gb.Count)] + Convert.ToString(rd.Next(8))).ToList();
            coordsOne = Enumerable.Range(0, 5).Select(i => new Vector2(rd.Next(300, 850), -rd.Next(20, 50))).ToList();

            for (int i = 0; i < rd.Next(1, 5); i++)
            {
                gbList.Add(new Garbage(game, typeGb[i], coordsOne[i], rd.Next(3, 5), rd.Next(5, 9)));
            }

            garbageList.Add(gbList);
            gbList = new List<Garbage>();

            typeGb = Enumerable.Range(0, 5).Select(k => type_Gb[rd.Next(type_Gb.Count)] + Convert.ToString(rd.Next(8))).ToList();
            coordsOne = Enumerable.Range(0, 5).Select(i => new Vector2(rd.Next(300, 850), -rd.Next(50))).ToList();

            for (int i = 0; i < rd.Next(1, 5); i++)
            {
                gbList.Add(new Garbage(game, typeGb[i], coordsOne[i], rd.Next(3, 5), rd.Next(5, 7)));
            }
            garbageList.Add(gbList);


            gbList = new List<Garbage>();

            typeGb = Enumerable.Range(0, 5).Select(k => type_Gb[rd.Next(type_Gb.Count)] + Convert.ToString(rd.Next(8))).ToList();
            coordsOne = Enumerable.Range(0, 5).Select(i => new Vector2(rd.Next(300, 850), -rd.Next(40))).ToList();

            for (int i = 0; i < rd.Next(1, 5); i++)
            {
                gbList.Add(new Garbage(game, typeGb[i], coordsOne[i], rd.Next(3, 5), rd.Next(5, 8)));
            }
            garbageList.Add(gbList);
            gbList = new List<Garbage>();

            typeGb = Enumerable.Range(0, 5).Select(k => type_Gb[rd.Next(type_Gb.Count)] + Convert.ToString(rd.Next(8))).ToList();
            coordsOne = Enumerable.Range(0, 5).Select(i => new Vector2(rd.Next(300, 850), -rd.Next(40))).ToList();

            for (int i = 0; i < rd.Next(1, 5); i++)
            {
                gbList.Add(new Garbage(game, typeGb[i], coordsOne[i], rd.Next(3, 5), rd.Next(5, 6)));
            }

            garbageList.Add(gbList);

        }
    }
}
