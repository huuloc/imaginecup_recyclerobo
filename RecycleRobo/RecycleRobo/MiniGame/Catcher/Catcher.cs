using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using RecycleRobo.Scenes;
using RecycleRobo.MGame.Catcher;

namespace RecycleRobo
{

    public class Catcher: MiniGame
    {

        GameManager game;
        List<Bins> binsList;
        List<List<Garbage>> garbageList;
        Background bg;
        Texture2D guide, energyBar;
        LiveTile tile1, tile2;
        Texture2D guideTexture, barTexture;
        public Catcher(GameManager game)
        {

            this.game = game;
            guideTexture = game.Content.Load<Texture2D>("Minigame/Guide/catcher");
            barTexture = game.Content.Load<Texture2D>("Minigame/energybar");

            binsList = new List<Bins>();
            garbageList = new List<List<Garbage>>();
            guide = game.Content.Load<Texture2D>("MiniGame\\Catcher\\guide");
            energyBar = game.Content.Load<Texture2D>("MiniGame\\Catcher\\energy bar");
            tile1 = new LiveTile(game,3);
            tile2 = new LiveTile(game,1);
            tile1.position = new Vector2(0, 0);
            tile2.position = new Vector2(GlobalClass.ScreenWidth - 258, 0);
            bg = new Background(Asset.bgMiniGame, 0.091f);

            Load();
        }
        

        public override void Play(GameTime gameTime)
        {
            tile1.Update(gameTime);
            tile2.Update(gameTime);
            if (!isWin())
            {
                foreach (Bins bin in binsList)
                {
                    bin.Update(gameTime, garbageList[0], binsList);
                }
                foreach (Garbage gb in garbageList[0])
                {
                    if (gb.position.Y >= GlobalClass.ScreenHeight - 40)
                    {
                        gb.isOut = true;
                    }
                    gb.Update(gameTime);
                }
                if (nextWave())
                {
                    garbageList.RemoveAt(0);
                }
            }


        }
        public bool nextWave()
        {
            foreach (Garbage gb in garbageList[0])
            {
                if (!gb.isOut)
                {
                    return false;
                }
            }
            foreach (Bins bin in binsList)
            {
               if (bin.isEffect == true)
               {
                   return false;
               }
            }
            return true;
        }
        public override bool isWin(){
            if (garbageList.Count == 0)
            {
                return true;
            }
            return false;
        }
        private void Load()
        {
            LoadCatcher minigame;
            minigame = new LoadCatcher(game);
            minigame.loadData(garbageList, binsList);
        }
        public override int calculateEnergyBall()
        {
            return Bins.energyBall;
        }
        public override void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {

            tile1.Draw(spriteBatch);
            tile2.Draw(spriteBatch);
            spriteBatch.Draw(guideTexture, new Vector2(0, GlobalClass.ScreenHeight - guideTexture.Height * 0.625f), null, Color.White*MiniGameScene.opacity, 0f, new Vector2(0.0f, 0.0f), 0.625f, SpriteEffects.None, 0.09f);
            spriteBatch.Draw(barTexture, new Vector2(GlobalClass.ScreenWidth - barTexture.Width * 0.625f, GlobalClass.ScreenHeight - barTexture.Height * 0.625f), null, Color.White * MiniGameScene.opacity, 0f, new Vector2(0.0f, 0.0f), 0.625f, SpriteEffects.None, 0.09f);

            bg.Draw(spriteBatch);
            spriteBatch.DrawString(Asset.fontMinigame, Convert.ToString(Bins.energyBall), new Vector2(GlobalClass.ScreenWidth - 165, GlobalClass.ScreenHeight - 80), Color.Black*MiniGameScene.opacity, 0f,Vector2.Zero,1f,SpriteEffects.None,0.03f);

            foreach (Bins bin in binsList)
            {
                bin.Draw(spriteBatch, gameTime);
            }
            if (garbageList.Count > 0)
                foreach (Garbage gb in garbageList[0])
                {
                    gb.Draw(spriteBatch);
                }
        }
    }
}
