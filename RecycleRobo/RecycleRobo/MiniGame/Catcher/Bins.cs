﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using RecycleRobo.Scenes;
namespace RecycleRobo.MGame.Catcher
{
    public class Bins
    {
        Vector2 effectPosition;
        public static string[] typeBinInit = { "compost","metal", "paper", "plastic","glass"};
        public Texture2D texture;
        public Texture2D pipline;
        public Vector2 position;
        int timeMove = 0;
        bool isWarning = false;
        float scale = 0.5f;
        public string type;
        MouseState currentMouseState, preMouseState;
        GameManager game;
        bool isDragging = true;
        int i = 0;
        Vector2 mousePosition;
        public static int energyBall = 0;
        public bool isEffect = false;
        public int timeShowEffect = 0;
        public Vector2 basePosition = new Vector2(0, 0);
        public int bonus;
        public Bins(GameManager game, string type, Vector2 position)
        {

            MiniGame.InitEffect();
            this.type = type;
            this.game = game;
            this.position = position;
            texture = game.Content.Load<Texture2D>("Minigame\\Catcher\\" + type);
            pipline = game.Content.Load<Texture2D>("Minigame\\Catcher\\pipline");
        }
        public void Update(GameTime gametime, List<Garbage> garbageList, List<Bins> binsList)
        {
            timeMove += gametime.ElapsedGameTime.Milliseconds;
            currentMouseState = Mouse.GetState();
            mousePosition = new Vector2(MouseHelper.MousePosition(currentMouseState).X, MouseHelper.MousePosition(currentMouseState).Y);
            if (currentMouseState.LeftButton == ButtonState.Released)
            {
                isDragging = false;
            }
            if (currentMouseState.LeftButton == ButtonState.Pressed && preMouseState.LeftButton == ButtonState.Released && checkBoundBin(mousePosition))
            {
                isDragging = true;
            }
            if (isDragging)
            {
                Vector2 mouseMove = new Vector2(MouseHelper.MousePosition(preMouseState).X - MouseHelper.MousePosition(currentMouseState).X, MouseHelper.MousePosition(preMouseState).Y - MouseHelper.MousePosition(currentMouseState).Y);
                position.X -= mouseMove.X;
                if (position.X < 258)
                {
                    position.X = 258;
                }
                if (position.X > GlobalClass.ScreenWidth - 285 - texture.Width*scale)
                {
                    position.X = GlobalClass.ScreenWidth - texture.Width * scale - 258;
                }

            }
            //Mouse release
            foreach (Garbage gb in garbageList)
            {
                if (checkBoundLock(gb))
                {
                    if (gb != null)
                    {
                        if (checkMatchBinsAndGar(gb))
                        {
                            if (!gb.isUsed)
                            {
                                Asset.correctSound.Play();
                                energyBall += gb.bonus;
                                this.bonus = gb.bonus;
                                isEffect = true;
                                effectPosition = position;
                                gb.isUsed = true;

                            }
                        }
                        else if (!gb.isUsed)
                        {
                            Asset.wrongSound.Play();
                            energyBall -= (int)gb.bonus/2;
                            effectPosition = position;
                            gb.isUsed = true;

                        }
                    }
                }
            }
            if (isEffect)
            {
                Vector2 direction = MiniGame.TargetEffectPos - effectPosition;
                direction.Normalize();
                effectPosition += direction * (float)gametime.ElapsedGameTime.TotalMilliseconds* 1.2f;
                if (effectPosition.X >= MiniGame.TargetEffectPos.X && effectPosition.Y >= MiniGame.TargetEffectPos.Y)
                    isEffect = false;
          
            }
            preMouseState = currentMouseState;
        }
        public bool checkBoundLock(Garbage gb)
        {
            Rectangle boxBin = new Rectangle((int)this.position.X, (int)this.position.Y, Convert.ToInt32(this.texture.Width * this.scale), Convert.ToInt32((this.texture.Height * gb.scale)/4));

            Rectangle box = new Rectangle((int)gb.position.X, (int)gb.position.Y, Convert.ToInt32(gb.texture.Width * gb.scale), Convert.ToInt32(gb.texture.Height * gb.scale));
            if (box.Intersects(boxBin))
            {
                return true;
            }
            return false;
        }
        public bool checkMatchBinsAndGar(Garbage gb)
        {
            if (this.type.Contains(gb.type.Remove(gb.type.Length-1,1)) || this.type.Equals("all"))
            {
                return true;
            }
            return false;
        }
        public bool checkBoundBin(Vector2 mousePosition)
        {
            Point mouse = new Point((int)mousePosition.X, (int)mousePosition.Y);

            Rectangle box = new Rectangle((int)position.X, (int)position.Y, Convert.ToInt32(texture.Width * scale), Convert.ToInt32(texture.Height * scale));
            if (box.Contains(mouse))
            {
                return true;
            }
            return false;
        }
        public void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            if (isEffect)
            {
                MiniGame.effect.PlayAnimation(MiniGame.energyBall);
                MiniGame.effect.Draw(gameTime, spriteBatch, effectPosition);
                //spriteBatch.DrawString(Asset.mediumFont, "+" + Convert.ToString(bonus), this.position + new Vector2(60, -30), Color.Black);
            }
            spriteBatch.Draw(texture, position, null, Color.White*MiniGameScene.opacity, 0.0f, new Vector2(0.0f, 0.0f), scale, SpriteEffects.None, 0.04f);
            spriteBatch.Draw(pipline, new Vector2(262, position.Y + texture.Height * scale / 2), null, Color.White*MiniGameScene.opacity, 0.0f, new Vector2(0.0f, 0.0f),0.625f, SpriteEffects.None, 0.08f);

        }
    }
}
