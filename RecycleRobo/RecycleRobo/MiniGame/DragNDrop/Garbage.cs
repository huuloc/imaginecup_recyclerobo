﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using RecycleRobo.Scenes;

namespace RecycleRobo.MGame.DrapNDrop
{
    public class Garbage
    {
        Vector2 effectPosition;
        public bool isEffect = false;
        public string type;
        public Vector2 position;
        public Vector2 orginposition;
        public Texture2D texture, originTexture, textureHover;
        public bool isUsed = false;
        public int timeMove = 0;
        public bool doneEffect = false;
        public float scale = 0.8f;
        public bool isDragging, isShowType = false;
        MouseState currentMouseState, olderMouseState;
        GameManager game;
        public static int energyBall = 0;
        Vector2 bonusPos;
        int bonus;

        public Rectangle baseRect, rect;
        public Color[] textureData;
        public Matrix transform;

        public Garbage(GameManager game, string t, Vector2 position, Texture2D texture, int bonus)
        {
            MiniGame.InitEffect();
            this.game = game;
            this.bonus = bonus;
            this.texture = texture;
            this.originTexture = texture;
            this.textureHover = game.Content.Load<Texture2D>("Minigame/Hide/" + t + "_Data");

            this.type = t.Remove(t.Length - 1, 1);
            this.position = position;
            this.orginposition = position;

            
            SoundEffect.MasterVolume = (float)GlobalClass.volumeSoundFx / 100;
            textureData = new Color[texture.Width * texture.Height];
            this.texture.GetData(textureData);
            baseRect = new Rectangle(0, 0, texture.Width, texture.Height);
        }
        public void Update(GameTime gametime, List<Bins> binsList)
        {
            SoundEffect.MasterVolume = (float)GlobalClass.volumeSoundFx / 100;
            timeMove += gametime.ElapsedGameTime.Milliseconds;
            if (timeMove < 1000)
            {
                this.position.Y -= 8;
            }
            else if (timeMove > 1000 && !doneEffect)
            {
                this.orginposition = position;
                doneEffect = true;
            }
            if(doneEffect)
            {

                currentMouseState = Mouse.GetState();
                Vector2 mousePosition = new Vector2(MouseHelper.MousePosition(currentMouseState).X, MouseHelper.MousePosition(currentMouseState).Y);

                if (currentMouseState.LeftButton == ButtonState.Released)
                {
                    isDragging = false;
                }
                if (currentMouseState.LeftButton == ButtonState.Pressed && olderMouseState.LeftButton == ButtonState.Released)
                {
                    if (checkBoundKey(mousePosition) && !isUsed)
                        isDragging = true;
                    else
                        isDragging = false;
                }
                if (isDragging)
                {
                    Vector2 mouseMove = new Vector2(MouseHelper.MousePosition(olderMouseState).X - MouseHelper.MousePosition(currentMouseState).X, MouseHelper.MousePosition(olderMouseState).Y - MouseHelper.MousePosition(currentMouseState).Y);
                    position -= mouseMove;
                }
                if (checkBoundKey(mousePosition) && currentMouseState.LeftButton == ButtonState.Released)
                {
                    texture = textureHover;
                    this.scale = 0.66f;
                }
                else
                {
                    texture = originTexture;
                    this.scale = 0.62f;
                }

                //Mouse release
                if (currentMouseState.LeftButton == ButtonState.Released && olderMouseState.LeftButton == ButtonState.Pressed)
                {
                    foreach (Bins bins in binsList)
                    {
                        bins.transform = RotatedRectangle.GetMatrixTransfrom(bins.position, Vector2.Zero, 0f, bins.scale);
                        bins.rect = RotatedRectangle.CalculateBoundingRectangle(bins.baseRect, bins.transform);
                        
                        this.transform = RotatedRectangle.GetMatrixTransfrom(this.position, Vector2.Zero, 0f, this.scale);
                        this.rect = RotatedRectangle.CalculateBoundingRectangle(baseRect, this.transform);
                        if (bins.rect.Intersects(this.rect))
                        {
                            if (RotatedRectangle.IntersectPixels(this.transform, this.texture.Width,
                                this.texture.Height, this.textureData,
                                bins.transform, bins.texture.Width,
                                bins.texture.Height, bins.textureData))
                            {
                                if (bins.type.Equals(this.type))
                                {
                                    this.isUsed = true;
                                    Asset.correctSound.Play();
                                    isEffect = true;
                                    effectPosition = position;
                                    bonusPos = bins.position;
                                    energyBall += bonus;
                                }
                                else if (!bins.type.Equals(this.type))
                                {
                                    Asset.wrongSound.Play();
                                    this.isUsed = true;
                                    energyBall -= (int)bonus / 2;


                                }

                            }


                        }

                    }

                    this.position = this.orginposition;
                    isDragging = false;

                }
                olderMouseState = currentMouseState;
                if (isEffect)
                {
                    Vector2 direction = MiniGame.TargetEffectPos - effectPosition;
                    direction.Normalize();
                    effectPosition += direction * (float)gametime.ElapsedGameTime.TotalMilliseconds * 1.5f;

                    if (effectPosition.X >= MiniGame.TargetEffectPos.X && effectPosition.Y >= MiniGame.TargetEffectPos.Y)
                    {
                        isEffect = false;
                    }

                }
            }


        }
        public Bins getLockTarget(Vector2 mousePosition, List<Bins> lockList)
        {
            foreach (Bins lockItem in lockList)
            {
                if (checkBoundLock(mousePosition,lockItem))
                    return lockItem;
            }

            return null ;
        }

        public bool checkBoundKey(Vector2 mousePosition)
        {
            Point mouse = new Point((int)mousePosition.X, (int)mousePosition.Y);

            Rectangle box = new Rectangle((int)position.X, (int)position.Y, Convert.ToInt32(texture.Width*scale), Convert.ToInt32(texture.Height*scale));
            if (box.Contains(mouse))
            {
                return true;
            }
            return false;
        }
        public bool checkBoundLock(Vector2 mousePosition, Bins lockItem)
        {
            Point mouse = new Point((int)mousePosition.X, (int)mousePosition.Y);
            Rectangle box = new Rectangle((int)lockItem.position.X, (int)lockItem.position.Y, Convert.ToInt32(lockItem.texture.Width), Convert.ToInt32(lockItem.texture.Height));
            
            if (box.Contains(mouse))
            {
                return true;
            }
            return false;
        }
        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (isEffect)
            {
                MiniGame.effect.PlayAnimation(MiniGame.energyBall);
                MiniGame.effect.Draw(gameTime, spriteBatch, effectPosition);
                //spriteBatch.DrawString(Asset.mediumFont, "+" + Convert.ToString(bonus), this.bonusPos + new Vector2(60, -30), Color.Black);
            }
            if(!this.isUsed)
                spriteBatch.Draw(texture, position, null, Color.White*MiniGameScene.opacity, 0.0f, Vector2.Zero, scale, SpriteEffects.None, 0.02f);
        }
        
    }
}
