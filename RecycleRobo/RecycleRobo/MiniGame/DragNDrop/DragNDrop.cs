using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using RecycleRobo.Scenes;
using RecycleRobo.MGame.DrapNDrop;

namespace RecycleRobo
{

    public class DragNDrop: MiniGame
    {
        GameManager game;
        List<Bins> binsList;
        List<Garbage> garbageList;
        Background bg;
        LiveTile tile1, tile2;
        Texture2D guideTexture, barTexture;
        public DragNDrop(GameManager game)
        {
            this.game = game;
            tile1 = new LiveTile(game,1);
            tile2 = new LiveTile(game,2);
            tile1.position = new Vector2(0, 0);
            tile2.position = new Vector2(GlobalClass.ScreenWidth - 258, 0);
            guideTexture = game.Content.Load<Texture2D>("Minigame/Guide/dragndrop");
            barTexture = game.Content.Load<Texture2D>("Minigame/energybar");
            bg = new Background(Asset.bgMiniGame, 0.091f);
            binsList = new List<Bins>();
            garbageList = new List<Garbage>();
            Load();
        }
        

        public override void Play(GameTime gameTime)
        {
            tile1.Update(gameTime);
            tile2.Update(gameTime);
            foreach (Bins bin in binsList)
            {
                bin.Update(gameTime);
            }
            foreach (Garbage gb in garbageList)
            {
                gb.Update(gameTime, binsList);
            }
        }
        public override bool isWin(){
            foreach (Garbage gb in garbageList)
                if (!gb.isUsed || gb.isEffect)
                    return false;
            return true;
        }
        private void Load()
        {
            LoadDragAndDrop minigame;
            minigame = new LoadDragAndDrop(game);
            minigame.loadData(garbageList, binsList);
        }
        public override int calculateEnergyBall()
        {
            return Garbage.energyBall;
        }
        public override void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            tile1.Draw(spriteBatch);
            tile2.Draw(spriteBatch);
            bg.Draw(spriteBatch);
            spriteBatch.Draw(guideTexture, new Vector2(0, GlobalClass.ScreenHeight - guideTexture.Height * 0.625f), null, Color.White * MiniGameScene.opacity, 0f, new Vector2(0.0f, 0.0f), 0.625f, SpriteEffects.None, 0.09f);
            spriteBatch.Draw(barTexture, new Vector2(GlobalClass.ScreenWidth - barTexture.Width * 0.625f, GlobalClass.ScreenHeight - barTexture.Height * 0.625f), null, Color.White * MiniGameScene.opacity, 0f, new Vector2(0.0f, 0.0f), 0.625f, SpriteEffects.None, 0.09f);
            spriteBatch.Draw(Asset.bgMiniGameForce, new Vector2(262, 0), null, Color.White * MiniGameScene.opacity, 0f, new Vector2(0.0f, 0.0f), 0.625f, SpriteEffects.None, 0.09f);
            spriteBatch.DrawString(Asset.fontMinigame, Convert.ToString(Garbage.energyBall), new Vector2(GlobalClass.ScreenWidth - 150, GlobalClass.ScreenHeight - 80), Color.Black, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.03f);

            foreach (Bins bin in binsList)
            {
                bin.Draw(spriteBatch);
            }
            foreach (Garbage gb in garbageList)
            {
                gb.Draw(gameTime,spriteBatch);
            }
        }
    }
}
