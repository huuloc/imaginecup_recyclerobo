﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using RecycleRobo.Scenes;
namespace RecycleRobo.MGame.Circle
{
    public class Garbage
    {
        public Vector2 position;
        public Texture2D texture, textureHover, originTexture;
        public Color[] textureData;
        public float rotate = 0f;
        public float scale = 0.38f;
        public float layer = 0.06f;
        public Rectangle rect;
        Rectangle baseRect;
        public Vector2 target = new Vector2(GlobalClass.ScreenWidth/2, GlobalClass.ScreenHeight/2);
        int speed;
        public bool isUpdate = true;
        public Matrix garbageTransform;
        public string type;
        public int bonus;
        public Color color;

        Random rd = new Random();
        public Garbage(GameManager game, string t, Vector2 position, int speed, int bonus)
        {
            originTexture = game.Content.Load<Texture2D>("Minigame/Garbage/" + t);
            texture = originTexture;
            textureHover = game.Content.Load<Texture2D>("Minigame/Hide/" + t + "_Data");
            this.type = t.Remove(t.Length - 1, 1);
            textureData = new Color[texture.Width * texture.Height];
            texture.GetData(textureData);
            this.speed = speed;
            this.position = position;
            baseRect = new Rectangle(0, 0, texture.Width, texture.Height);
            layer += (float)rd.Next(20, 80) / 10000;
            this.bonus = bonus;
            rotate = rd.Next(100, 900) / 1000;
            color = Color.White;

        }
        public void Update(GameTime gametime, List<Bins> listBins)
        {
            MouseState mouse = Mouse.GetState();

            Vector2 direction = target - position;
            direction.Normalize();
            if (Math.Abs(position.X - target.X) < 10 && Math.Abs(position.Y - target.Y) < 10)
            {
                position.X = target.X;
                isUpdate = false;
            }
            else
            {
                position += direction * (float)gametime.ElapsedGameTime.TotalMilliseconds * speed/50;

            }
            //garbageTransform =
                   // Matrix.CreateTranslation(new Vector3(position, 0.0f));
            if (isUpdate)
            {
               garbageTransform = RotatedRectangle.GetMatrixTransfrom(this.position, Vector2.Zero, rotate, scale);
                rect = RotatedRectangle.CalculateBoundingRectangle(baseRect, garbageTransform);
            }
            if (rect.Contains(MouseHelper.MousePosition(mouse)))
            {
                texture = textureHover;
            }
            else
                texture = originTexture;
           
        }
        public void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            if (isUpdate)
            {
                spriteBatch.Draw(texture, position, null, Color.White * MiniGameScene.opacity, rotate, Vector2.Zero, scale, SpriteEffects.None, layer);

            }
        }

        
    }
}
