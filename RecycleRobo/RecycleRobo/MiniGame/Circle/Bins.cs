﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using RecycleRobo.Scenes;
namespace RecycleRobo.MGame.Circle
{
    public class Bins
    {
        public Vector2 position;
        public float rotate;
        public Vector2 center, effectPosition;
        public Texture2D texture;
        public Color[] textureData;
        float layer = 0.08f;
        public float scale = 0.3f;
        public static int energyBall = 0;
        float baseCircle = MathHelper.Pi * 36 / 180;
        Random rd = new Random();
        bool isShow = false;
        public Rectangle rect;
        public Rectangle baseRect;
        public Matrix binsTransform;
        MouseState curMouse, preMouse;
        public string type;
        public bool isClockWise = true, isEffect = false;
        public Bins(GameManager game, string type, Vector2 position, float rotate)
        {
            MiniGame.InitEffect();
            MiniGame.effect.Scale = 0.45f;

            texture = game.Content.Load<Texture2D>("Minigame/Circle/" + type);
            this.type = type;
            textureData = new Color[texture.Width*texture.Height];
            texture.GetData(textureData);
            
            this.position = position;
            this.rotate = rotate;
            Init();
            
            baseRect = new Rectangle(0, 0, texture.Width, texture.Height);
        }
        public void Init()
        {
            center = new Vector2(texture.Width/ 2, texture.Height);
        }
        public void Update(GameTime gametime, List<Garbage> garbageList, List<Bins> binsList)
        {
            SoundEffect.MasterVolume = (float)GlobalClass.volumeSoundFx / 100;

            foreach (Garbage gb in garbageList)
            {
                //gbList.Add(new Garbage(game, "compost0", new Vector2(600, -500), 7, 3));
                if (gb.isUpdate)
                {
                    /// Console.WriteLine(gb.type);

                    this.binsTransform = RotatedRectangle.GetMatrixTransfrom(this.position, this.center, this.rotate, this.scale);
                    this.rect = RotatedRectangle.CalculateBoundingRectangle(this.baseRect, this.binsTransform);
                    if (this.rect.Intersects(gb.rect))
                    {
                        if (RotatedRectangle.IntersectPixels(gb.garbageTransform, gb.texture.Width,
                        gb.texture.Height, gb.textureData,
                        this.binsTransform, this.texture.Width,
                        this.texture.Height, this.textureData))
                        {
                            gb.isUpdate = false;

                            if (this.type.Equals(gb.type))
                            {
                                effectPosition = gb.position;
                                isEffect = true;
                                energyBall += gb.bonus;
                                Asset.correctSound.Play();
                            }
                            else
                            {
                                energyBall -= (int)gb.bonus/2;
                                Asset.wrongSound.Play();

                            }
                            // Console.WriteLine("Hong Hong Tuyet Tuyet");

                        }
                    }
                }

            }

            preMouse = curMouse;

            if (isEffect)
            {
                Vector2 direction = MiniGame.TargetEffectPos - effectPosition;
                direction.Normalize();
                effectPosition += direction * (float)gametime.ElapsedGameTime.TotalMilliseconds * 1.3f;

                if (effectPosition.X >= MiniGame.TargetEffectPos.X && effectPosition.Y >= MiniGame.TargetEffectPos.Y)
                {
                    isEffect = false;
                }

            }
        }
        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            //Rectangle aPositionAdjusted = new Rectangle(rectangleRotated.X + (rectangleRotated.Width / 2), rectangleRotated.Y + (rectangleRotated.Height), rectangleRotated.Width, rectangleRotated.Height);
            //spriteBatch.Draw(texture, position, new Rectangle(0, 0, 2, 6), Color.White, rectangleRotated.Rotation, new Vector2(2 / 2, 6 / 2), SpriteEffects.None, 0);
            if (isEffect)
            {
                MiniGame.effect.PlayAnimation(MiniGame.energyBall);
                MiniGame.effect.Draw(gameTime, spriteBatch, effectPosition);
                //spriteBatch.DrawString(Asset.mediumFont, "+" + Convert.ToString(bonus), this.bonusPos + new Vector2(60, -30), Color.Black);
            }
            spriteBatch.Draw(texture, position, null, Color.White * MiniGameScene.opacity, rotate, center, scale, SpriteEffects.None, layer);
            if (isShow)
            {
               //spriteBatch.DrawString(Asset.mediumFont, Convert.ToString(rotate), position + new Vector2(rd.Next(30),rd.Next(50)), Color.DimGray);
            }

        }
    }
}
