using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using RecycleRobo.Scenes;
using LoadData;


namespace RecycleRobo
{

    public class WaveManager
    {
        public GameManager game;
        public List<List<Monster[]>> waveMonster = new List<List<Monster[]>>();
        public WavePlay wave;
        public int timeToNextWave = 0;
        public List<Item> sewages;
        public bool isEnd;

        private float timeCount = 0.0f;

        public WaveManager(GameManager game, List<List<Monster[]>> waveMonster)
        {
            this.waveMonster = waveMonster;
            this.sewages = new List<Item>();

            wave = new WavePlay(this.game, waveMonster[0]);
            waveMonster.RemoveAt(0);
            this.game = game;
            isEnd = false;
        }
        public void Update(GameTime gameTime,Player[] player, SkillManager skillmanager)
        {
            //Console.WriteLine("XXCCC" + this.waveMonster.Count);
            if (wave.isOver() && !isWin())
            {
                timeToNextWave += gameTime.ElapsedGameTime.Milliseconds;
                if (timeToNextWave >= 2000)
                {
                    wave = new WavePlay(this.game, waveMonster[0]);
                    waveMonster.RemoveAt(0);
                    timeToNextWave = 0;
                }
            }

            if (IsVictory() && !isEnd)
            {
                timeCount += gameTime.ElapsedGameTime.Milliseconds;
                if (timeCount >= 2500)
                {
                    timeCount = 0.0f;
                    //Console.WriteLine("THANG ROI");
                    foreach (Item sewage in sewages.ToArray())
                    {
                        sewages.Remove(sewage);
                    }

                    skillmanager.clearAll();
                    isEnd = true;
                    GameManager.winForm.Cursor = GameManager.cursorDefault;
                    VictoryScene.player = player;
                    game.StartMiniGame();
                }
            }

            if (isLosing(player) && !isEnd)
            {
                timeCount += gameTime.ElapsedGameTime.Milliseconds;
                if (timeCount >= 2500)
                {
                    timeCount = 0.0f;
                    //Console.WriteLine("THUA ROI");
                    foreach (Item sewage in sewages.ToArray())
                    {
                        sewages.Remove(sewage);
                    }
                    skillmanager.clearAll();
                    isEnd = true;
                    GameManager.winForm.Cursor = GameManager.cursorDefault;
                    game.StartGameOverScene();

                }
            }

            wave.Update(gameTime, player, sewages);
        }
        public bool isWin()
        {
            if (this.waveMonster.Count == 0)
                return true;
            return false;
        }

        public bool IsVictory()
        {
            if (waveMonster.Count <= 0 && wave.listMonster.Count == 0)
            {
                for (int i = 0; i < wave.monsters.Length; i++ )
                {
                    if (wave.monsters[i].isAlive)
                    {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }

        public bool isLosing(Player[] player)
        {
            for (int i = 0; i < player.Length; i++)
            {
                if(player[i] != null)
                {
                    if (player[i].isAlive)
                    {
                        return false;
                    }
                }
                
            }
            return true;
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
                wave.Draw(gameTime, spriteBatch, this.sewages);
        }

    }
}
