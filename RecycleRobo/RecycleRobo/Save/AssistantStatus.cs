﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RecycleRobo
{
    [Serializable]
    public class AssistantStatus
    {
        public bool isBought;
        public bool isUnlocked;
        public int cost;
        public string type;
        public AssistantStatus() { ;}
    }
}
