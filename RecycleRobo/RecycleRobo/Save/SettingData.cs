﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RecycleRobo
{
    [Serializable]
    public class SettingData
    {
        public int volumeMusic;
        public int volumeSoundFx;
        public bool isFullScreen;
        public SettingData() { }
        public SettingData(int value1, int value2, bool isFullScreen)
        {
            this.isFullScreen = isFullScreen;
            this.volumeMusic = value1;
            this.volumeSoundFx = value2;
        }
    }
}
