﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RecycleRobo
{
    [Serializable]
    public class PlayerAtt
    {
        public int exp;
        public int level;
        public PlayerAtt() { ;}
        public PlayerAtt(int level, int exp)
        {
            this.level = level;
            this.exp = exp;
        }
    }
    [Serializable]
    public class NumberOneUpgrade : PlayerAtt
    {
        public NumberOneUpgrade() { ;}
        public NumberOneUpgrade(int level, int exp) : base(level, exp){}
    }
    [Serializable]
    public class NumberTwoUpgrade : PlayerAtt {
        public NumberTwoUpgrade() { ;}
        public NumberTwoUpgrade(int level, int exp) : base(level, exp) { }
    }
    [Serializable]
    public class NumberThreeUpgrade : PlayerAtt
    {
        public NumberThreeUpgrade() { ;}
        public NumberThreeUpgrade(int level, int exp) : base(level, exp) { }

    }
    [Serializable]
    public class NumberFourUpgrade : PlayerAtt
    {
        public NumberFourUpgrade() { ;}
        public NumberFourUpgrade(int level, int exp) : base(level, exp){}
    }
    [Serializable]
    public class NumberFiveUpgrade : PlayerAtt
    {
        public NumberFiveUpgrade() { ;}
        public NumberFiveUpgrade(int level, int exp) : base(level, exp) { }

    }

}
