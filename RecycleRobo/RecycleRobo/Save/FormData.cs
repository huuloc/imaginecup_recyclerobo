﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RecycleRobo
{
    [Serializable]
    public class FormData
    {
        public String typeForm;
        public int[] bonus;
        public FormData() { }
        public FormData(String typeForm, int[] bonus)
        {
            this.typeForm = typeForm;
            this.bonus = bonus;
        }
    }
}
