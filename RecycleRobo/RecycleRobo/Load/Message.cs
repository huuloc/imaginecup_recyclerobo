using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using RecycleRobo.Scenes;

namespace RecycleRobo
{
    public class Message
    {
        GameManager game;
        public Vector2 position;
        Texture2D textBox;
        public Message(GameManager game, Vector2 position, Texture2D textBox)
        {
            this.game = game;
            this.position = position;
            this.textBox = textBox;
        }
        public void Draw(SpriteBatch spritebatch)
        {
            spritebatch.Draw(textBox, position, null, Color.White, 0.0f, Vector2.Zero, 1f, SpriteEffects.None, 0.000000000002f);

        }
    }
}
