
﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Xml.Serialization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;

namespace RecycleRobo
{
    public class LoadData
    {
        StorageDevice storageDevice;
        IAsyncResult asyncResult;
        PlayerIndex playerIndex = PlayerIndex.One;
        StorageContainer storageContainer;
        public T DoLoad<T>(string filename)
        {
            T data = default(T);
            asyncResult = StorageDevice.BeginShowSelector(playerIndex, null, null);
            if (asyncResult.IsCompleted)
            {
                storageDevice = StorageDevice.EndShowSelector(asyncResult);
            }
            asyncResult = storageDevice.BeginOpenContainer("TestNumberOne", null, null);
            if (asyncResult.IsCompleted)
            {
                storageContainer = storageDevice.EndOpenContainer(asyncResult);
            }

            // Read the data from the file
            try
            {
                using (Stream stream = storageContainer.OpenFile(filename, FileMode.Open))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(T));
                    data = (T)serializer.Deserialize(stream);
                    storageContainer.Dispose();

                }

            }
            catch (Exception e)
            {

            }


            return data;

        }

    }
}