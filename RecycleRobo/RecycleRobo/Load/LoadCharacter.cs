﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LoadData;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections;
using RecycleRobo.Scenes;

namespace RecycleRobo
{
    public class LoadCharacter
    {
        private GameManager game;
        private int[] posX;
        private int[] posY;
        public LoadCharacter(GameManager g)
        {
            int width = (int)GlobalClass.ScreenWidth;
            int height = (int)GlobalClass.ScreenHeight;

            posX = new int[] { width + 100, width + 80, width + 120, width + 180, width + 240, width + 280,
                                -150, -100, -140,-180,-240,-220,};
            posY = new int[] { 100, 150, 100, 120, 140, 180, 200, 250, 330, 140, 180, 600, 400, 450, 800, 700, 850 };
            game = g;
        }
        public void Monster(Monster[] monsters, Player[] player, List<MonsterData> data)
        {

            Random rd = new Random();
            int k = 0;
            for (int i = 0; i < data.Count; i++)
            {
                for (int j = 0; j < data[i].amount; j++)
                {
                    int posX = this.posX[rd.Next(0, 11)];
                    int posY = this.posY[rd.Next(0, this.posY.Length - 1)];
                    if (data[i].name == "trash1")
                    {
                        monsters[k++] = new Trash1(game, game.Content, new Vector2(posX, posY), data[i].health, data[i].damage, data[i].speed, player);
                    }
                    else if (data[i].name == "trash2")
                    {
                        monsters[k++] = new Trash2(game, game.Content, new Vector2(posX, posY), data[i].health, data[i].damage, data[i].speed, player);
                    }
                    else if (data[i].name == "trash3")
                    {
                        monsters[k++] = new Trash3(game, game.Content, new Vector2(posX, posY), data[i].health, data[i].damage, data[i].speed, player);
                    }
                    else if (data[i].name == "trash4")
                    {
                        monsters[k++] = new Trash4(game, game.Content, new Vector2(posX, posY), data[i].health, data[i].damage, data[i].speed, player);
                    }
                    else if (data[i].name == "trash5")
                    {
                        monsters[k++] = new Trash5(game, game.Content, new Vector2(posX, posY), data[i].health, data[i].damage, data[i].speed, player);
                    }
                    else if (data[i].name == "trash6")
                    {
                        monsters[k++] = new Trash6(game, game.Content, new Vector2(posX, posY), data[i].health, data[i].damage, data[i].speed, player);
                    }
                    else if (data[i].name == "trash7")
                    {
                        monsters[k++] = new Trash7(game, game.Content, new Vector2(posX, posY), data[i].health, data[i].damage, data[i].speed, player);
                    }
                    else if (data[i].name == "trash8")
                    {
                        monsters[k++] = new Trash8(game, game.Content, new Vector2(posX, posY), data[i].health, data[i].damage, data[i].speed, player);
                    }
                    else if (data[i].name == "trash9")
                    {
                        monsters[k++] = new Trash9(game, game.Content, new Vector2(posX, posY), data[i].health, data[i].damage, data[i].speed, player);
                    }
                    else if (data[i].name == "trash10")
                    {
                        monsters[k++] = new Trash10(game, game.Content, new Vector2(posX, posY), data[i].health, data[i].damage, data[i].speed, player);
                    }
                    else if (data[i].name == "trash11")
                    {
                        monsters[k++] = new Trash11(game, game.Content, new Vector2(posX, posY), data[i].health, data[i].damage, data[i].speed, player);
                    }
                    else if (data[i].name == "boss1")
                    {
                        monsters[k++] = new BossOne(game, game.Content, new Vector2(posX, posY), data[i].health, data[i].damage, data[i].speed, player);
                    }
                    else if (data[i].name == "boss2")
                    {
                        monsters[k++] = new BossTwo(game, game.Content, new Vector2(posX, posY), data[i].health, data[i].damage, data[i].speed, player);
                    }
                }
            }
        }
        public void Player(Player[] player)
        {
            List<RoboToSelect> robotSelected = GlobalClass.RobotSelected;

            for (int i = 0; i < robotSelected.Count; i++)
            {
                Assistant assistant = null;
                if (robotSelected.ElementAt(i).assistantEquiped != null)
                    assistant = robotSelected.ElementAt(i).assistantEquiped.assistant;

                if (robotSelected.ElementAt(i).Name == "NumberOne")
                {
                    player[i] = new NumberOne(game.Content, robotSelected.ElementAt(i).CurrentPos, assistant, game);
                }
                else if (robotSelected.ElementAt(i).Name == "NumberTwo")
                {
                    player[i] = new NumberTwo(game.Content, robotSelected.ElementAt(i).CurrentPos, assistant, game);
                }
                else if (robotSelected.ElementAt(i).Name == "NumberThree")
                {
                    player[i] = new NumberThree(game.Content, robotSelected.ElementAt(i).CurrentPos, assistant, game);
                }
                else if (robotSelected.ElementAt(i).Name == "NumberFour")
                {
                    player[i] = new NumberFour(game.Content, robotSelected.ElementAt(i).CurrentPos, assistant, game);
                }
                else if (robotSelected.ElementAt(i).Name == "NumberFive")
                {
                    player[i] = new NumberFive(game.Content, robotSelected.ElementAt(i).CurrentPos, assistant, game);
                }
                else player[i] = null;
                robotSelected[i].correspondingRobot = player[i];

            }
        }
    }

}