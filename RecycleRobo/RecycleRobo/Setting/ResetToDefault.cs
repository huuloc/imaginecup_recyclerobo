﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RecycleRobo
{
    public class ResetToDefault
    {
        private static Data data = new Data();

        public static void reset()
        {
            resetIntro();
            resetMapInfo();
            resetPlayer();
            resetRoboAvailable();
            resetInfoToUpgrade();
            resetResource();
            resetSetting();
            resetChallenge();
        }

        public static void resetIntro()
        {
            bool[] intro = { true, true, true };
            data.Save(intro, "Introduction/intro.xml");
        }

        public static void resetMapInfo()
        {
            Level level;
            //Map1
            for (int i = 1; i <= 5; i++ )
            {
                level = new Level(false, 0);

                data.Save(level, "Map/Map_1/Level" + i + ".xml");
            }
            //Map2
            for (int i = 1; i <= 2; i++)
            {
                level = new Level(false, 0);
                data.Save(level, "Map/Map_2/Level" + i + ".xml");
            }
        }
        public static void resetChallenge()
        {
            Challenge t = new Challenge();
            data.Save(t, "Challenge\\ChallengeCompleted.xml");
        }

        public static void resetPlayer()
        {
            NumberOneUpgrade num1Up = new NumberOneUpgrade(1,1);
            data.Save(num1Up, "Player/NumberOne.xml");

            NumberTwoUpgrade num2Up = new NumberTwoUpgrade(1, 1);
            data.Save(num2Up, "Player/NumberTwo.xml");

            NumberThreeUpgrade num3Up = new NumberThreeUpgrade(1, 1);
            data.Save(num3Up, "Player/NumberThree.xml");

            NumberFourUpgrade num4Up = new NumberFourUpgrade(1, 1);
            data.Save(num4Up, "Player/NumberFour.xml");
            
            NumberFiveUpgrade num5Up = new NumberFiveUpgrade(1, 1);
            data.Save(num5Up, "Player/NumberFive.xml");
        }
        public static void resetRoboAvailable()
        {
            int[] available = {1,2, 3,4,5};
            RoboAvailable roboAvail = new RoboAvailable(available);
            data.Save(roboAvail, "Player/RoboAvailable.xml");
        }

        public static void resetResource()
        {
            int[] resource = { 0,0,0,0,0};
            Resource resourceDefault = new Resource(resource);
            data.Save(resourceDefault, "Resource/resource.xml");
        }

        public static void resetKitBasePrice()
        {
            // kitbasePrice
            List<int[]> kitbasePrice = new List<int[]>();
            //theo thu tu: plastic, paper, glass, compost, metal

            int[] plasticKit = {0,0,0,0,10};
            kitbasePrice.Add(plasticKit);

            int[] paperKit = {50,0,0,0,0};
            kitbasePrice.Add(paperKit);

            int[] glassKit = {70,50,0,0,0};
            kitbasePrice.Add(glassKit);

            int[] compostKit = {10,20,50,0,0};
            kitbasePrice.Add(compostKit);

            int[] metalKit = {20,20,20,0,0};
            kitbasePrice.Add(metalKit);

            int[] metal_plastic_Kit = { 0, 0, 0, 90, 0 };
            kitbasePrice.Add(metal_plastic_Kit);

            int[] multiMixedKit = { 150, 100, 150, 100, 150 };
            kitbasePrice.Add(multiMixedKit);

            data.Save(kitbasePrice, "RecycleBaseData/kitBasePrice.xml");
        }

        public static void resetBaseAttackUpgradeReq()
        {
            List<float[]> baseAttackUpgradeReq = new List<float[]>();
            float [] num1 = { 20, 7, 10, 0, 8 };
            baseAttackUpgradeReq.Add(num1);
            float[] num2 = { 0, 8, 20, 0, 7 };
            baseAttackUpgradeReq.Add(num2);
            float[] num3 = { 12, 13, 14, 18, 20 };
            baseAttackUpgradeReq.Add(num3);
            float[] num4 = { 0, 7, 0, 25, 4 };
            baseAttackUpgradeReq.Add(num4);
            float[] num5 = { 30, 4, 4, 30, 7 };
            baseAttackUpgradeReq.Add(num5);
            data.Save(baseAttackUpgradeReq, "RecycleBaseData/baseAttackUpgradeRequirement.xml");
        }

        public static void resetBaseHealthUpgradeReq()
        {
            List<float[]> baseHealthUpgradeReq = new List<float[]>();
            float[] num1 = { 18, 11, 7, 0, 0 };
            baseHealthUpgradeReq.Add(num1);
            float[] num2 = { 0, 10, 21, 0, 0 };
            baseHealthUpgradeReq.Add(num2);
            float[] num3 = { 0, 0, 19, 0, 10 };
            baseHealthUpgradeReq.Add(num3);
            float[] num4 = { 0, 0, 22, 20, 10 };
            baseHealthUpgradeReq.Add(num4);
            float[] num5 = { 0, 5, 4, 15, 8 };
            baseHealthUpgradeReq.Add(num5);
            data.Save(baseHealthUpgradeReq, "RecycleBaseData/baseHealthUpgradeRequirement.xml");
        }

        public static void resetOtherStats()
        {
            float[] attackCostIncrease = { 1.55f, 1.5f, 1.6f, 1.6f, 1.5f};
            data.Save(attackCostIncrease, "RecycleBaseData/AttackCostIncrease.xml");

            float[] healthCostIncrease = { 1.55f, 1.6f, 1.5f, 1.55f, 1.5f};
            data.Save(healthCostIncrease, "RecycleBaseData/HealthCostIncrease.xml");

            float[] baseAttackIncrease = { 1.12f, 1.135f, 1.1f, 1.1f, 1.08f};
            data.Save(baseAttackIncrease, "RecycleBaseData/baseAttackIncrease.xml");

            float[] baseHealthIncrease = { 1.1f, 1.08f, 1.135f, 1.1f, 1.135f};
            data.Save(baseHealthIncrease, "RecycleBaseData/baseHealthIncrease.xml");
        }

        public static void resetInfoToUpgrade()
        {
            resetKitBasePrice();
            resetBaseAttackUpgradeReq();
            resetBaseHealthUpgradeReq();
            resetOtherStats();
        }

        public static void resetSetting()
        {
            SettingData settingdata = new SettingData(50, 50, false);
            data.Save(settingdata, "Setting/Setting.xml");
        }
    }
}

