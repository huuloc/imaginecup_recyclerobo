using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace RecycleRobo.Scenes
{

    public class LoadingScene
    {
        private Texture2D Background, BackgroundMist, logo, icon, gameName;
        private SpriteFont textPercentage;
        int time = 0;
        private Animation trash, demo;
        private AnimationPlayer trashPlayer, demoPlayer;
        private Vector2 soulPos, demoPos, originpos;

        private float distance;
        private int count = 0;

        public LoadingScene(ContentManager content)
        {
            Background = content.Load<Texture2D>("Menu\\background");
            BackgroundMist = content.Load<Texture2D>("Menu\\blackMist");

            //loadingTexture = content.Load<Texture2D>("Loading\\loading");
            logo = content.Load<Texture2D>("Loading\\logo team");
            icon = content.Load<Texture2D>("Loading\\logo");
            gameName = content.Load<Texture2D>("Loading\\gamename");
            textPercentage = content.Load<SpriteFont>("largeFont");

            trash = new Animation(content.Load<Texture2D>("Animations/trash/trash2_walk_4x6"), 1000, 791, 4, 6, 0.07f, true);
            trashPlayer.Scale = 0.625f;
            trashPlayer.color = Color.White;
            trashPlayer.OrderLayer = 0.1f;
            trashPlayer.PlayAnimation(trash);

            demo = new Animation(content.Load<Texture2D>("Animations/num1/num1_lvl3_idle_2x6"), 1620, 499, 2, 6, 0.07f, true);
            demoPlayer.Scale = 0.625f;
            demoPlayer.color = Color.White;
            demoPlayer.OrderLayer = 0.1f;
            demoPlayer.SpriteEffect = SpriteEffects.FlipHorizontally;
            demoPlayer.PlayAnimation(demo);

            originpos = soulPos = new Vector2(222, 280);
            demoPos = new Vector2(GlobalClass.ScreenWidth - demo.FrameWidth * demoPlayer.Scale - 200, 256);

            distance = demoPos.X - soulPos.X;

        }


        public void Update(GameTime gameTime)
        {

            time += gameTime.ElapsedGameTime.Milliseconds;
            if (time > 80)
            {
                time = 0;
                if (count >= GlobalClass.percentage) count = GlobalClass.percentage;
                else count++;
                soulPos.X = originpos.X + count * distance / 100;
            }
        }

        public void Draw(GameTime gameTime, SpriteBatch sp)
        {

            demoPlayer.Draw(gameTime, sp, demoPos);
            trashPlayer.Draw(gameTime, sp, soulPos);

            sp.DrawString(textPercentage,count.ToString() + " %",
                new Vector2(GlobalClass.ScreenWidth / 2 - 40, GlobalClass.ScreenHeight / 2 + 65), 
                Color.Green, 0f, Vector2.Zero, 0.8f, SpriteEffects.None, 0.0001f);

            sp.Draw(Background, Vector2.Zero, null, Color.White, 0.0f, Vector2.Zero, 0.625f, SpriteEffects.None, 0.98f);
            sp.Draw(BackgroundMist, Vector2.Zero, null, Color.White, 0.0f, Vector2.Zero, 0.625f, SpriteEffects.None, 0.8f);

            //sp.Draw(logo, new Vector2(GlobalClass.ScreenWidth - 200, GlobalClass.ScreenHeight - 200), null, Color.White, 0f, new Vector2(logo.Width / 2, logo.Height / 2), 0.5f, SpriteEffects.None, 0.01f);
            sp.Draw(gameName, new Vector2(GlobalClass.ScreenWidth / 2, 150), null, Color.White, 0f, new Vector2(gameName.Width / 2, gameName.Height / 2), 1f, SpriteEffects.None, 0.01f);

            sp.Draw(icon, new Vector2(GlobalClass.ScreenWidth - icon.Width*0.7f, GlobalClass.ScreenHeight - icon.Height*0.7f), null, Color.White, 0f, new Vector2(icon.Width / 2, icon.Height / 2), 1f, SpriteEffects.None, 0.01f);

            //sp.Draw(loadingTexture, new Vector2(GlobalClass.ScreenWidth / 2, GlobalClass.ScreenHeight / 2), null, Color.White, rotate, new Vector2(loadingTexture.Width / 2, loadingTexture.Height / 2), 0.6f, SpriteEffects.None, 0.01f);

        }
    }
}
