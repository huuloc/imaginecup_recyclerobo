﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections;
using LoadData;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;
using ProjectMercury.Renderers;
using ProjectMercury;
using RecycleRobo.Scenes.Victory;

namespace RecycleRobo.Scenes
{
    public class GamePlayScene
    {
        private LevelData loadLevel;

        private GameManager game;
        private Background bg, howto;

        public static Player[] player;

        public static WaveManager waveManager;
        private List<List<Monster[]>> waveMonster;

        private float delayTime = 0.0f;
        private Setting settingPopup;
        private Button gotIt, settingButton;
        private bool isHowTo;

        public static SkillManager skillManager;
        Song playSceneSong;
        Texture2D fadeTexture;
        SpriteFont font;

        public static sideEffectManager sideEffect;

        private bool isRetreat = false;

        public Exp experiment;
        public GamePlayScene(GameManager game)
        {
            CyclebotNewFormNotif.effect = game.Content.Load<ParticleEffect>("Effect/newCyclebotEffect");
            CyclebotNewFormNotif.effect.LoadContent(game.Content);
            CyclebotNewFormNotif.effect.Initialise();
            sideEffect = new sideEffectManager(game.Content);

            MediaPlayer.IsRepeating = true;

            MediaPlayer.Play(game.Content.Load<Song>("Sounds/play/_gamePlay_" + Convert.ToString(GlobalClass.curLevelPlaying%3+1) + "_" + Convert.ToString(GlobalClass.curMapPlaying)));
            font = game.Content.Load<SpriteFont>("newFont");
            this.game = game;
            fadeTexture = game.Content.Load<Texture2D>("Minigame\\6");

            if (GlobalClass.curMapPlaying == 1 && GlobalClass.curLevelPlaying == 1)
            {
                howto = new Background(Asset.guidePlayScene, 0.01f);
                isHowTo = true;
            }
            else
                isHowTo = false;

            settingPopup = new Setting(game, new Vector2(820, 156));
            waveMonster = new List<List<Monster[]>>();

            LoadCharacter load = new LoadCharacter(game);
            player = new Player[GlobalClass.RobotSelected.Count];

            load.Player(player);

            loadLevel = GlobalClass.LevelInformation = game.Content.Load<LevelData>("Data/Map/Map" + GlobalClass.curMapPlaying +
                                                "/Level" + GlobalClass.curLevelPlaying);

            //read level data
            foreach (Wave wave in loadLevel.waves)
            {
                List<Monster[]> listMonter = new List<Monster[]>();
                Monster[] monster = new Monster[wave.totalMonster];
                List<MonsterData> data = wave.monsters;
                load.Monster(monster, player, data);
                listMonter.Add(monster);
                foreach (SubWave sub in wave.subWave)
                {
                    Monster[] subMonster = new Monster[sub.totalMonster];
                    List<MonsterData> subData = sub.monsters;
                    load.Monster(subMonster, player, subData);
                    listMonter.Add(subMonster);
                }
                waveMonster.Add(listMonter);
            }


            bg = new Background(game.Content, "Maps/Backgrounds/" + loadLevel.background, 0.99f);

            waveManager = new WaveManager(game, waveMonster);

            skillManager = new SkillManager(game.Content, player, waveManager);

            gotIt = new Button(game.Content.Load<Texture2D>("Maps/gotItButton"),
                game.Content.Load<Texture2D>("Maps/gotItButton_Hover"),
                game.Content.Load<Texture2D>("Maps/gotItButton"), 0.8f, 0.0f,
                new Vector2(GlobalClass.ScreenWidth / 2, GlobalClass.ScreenHeight / 2 - 100));
            gotIt.layer = 0.001f;
            gotIt.Clicked += new EventHandler(gotIt_Clicked);
            MiniGameScene.opacity = 0;

            GlobalClass.timePlaying = 0;
            InitConfirmButton();

        }
        public void InitConfirmButton()
        {

            Texture2D optionButton = game.Content.Load<Texture2D>("Menu/optionButton");
            Texture2D optionButtonHover = game.Content.Load<Texture2D>("Menu/optionButton_hover");

            settingButton = new Button(optionButton, optionButtonHover, optionButton, 0.625f, 0.0f,
                 new Vector2(GlobalClass.ScreenWidth - optionButton.Width * 0.625f / 2 - 30, optionButton.Height * 0.625f / 2));
            settingPopup.isPlaySene = true;
            settingButton.layer = 0.098f;
            settingButton.Clicked += new EventHandler(settingButton_Clicked);
        }
        public void settingButton_Clicked(object o, EventArgs e)
        {
            settingPopup.isResume = !settingPopup.isResume;
            GlobalClass.isSetting = !GlobalClass.isSetting;
        }
        private void gotIt_Clicked(object o, EventArgs e)
        {
            this.isHowTo = false;
            GlobalClass.isPause = false;
        }
        public void Update(GameTime gameTime)
        {
            float s = (float)gameTime.ElapsedGameTime.TotalSeconds;
            sideEffect.Update(gameTime, s);
            if (isHowTo)
            {
                gotIt.Update(gameTime);
                GlobalClass.isPause = true;
            }
            MediaPlayer.Volume = (float)GlobalClass.volumeMusic / 100;
            SoundEffect.MasterVolume = (float)GlobalClass.volumeSoundFx / 100;

            settingPopup.Update(gameTime);
            settingButton.Update(gameTime);
            if (!settingPopup.isResume || settingPopup.isRetreat)
            {
                GlobalClass.isPause = true;
            }
            if (!GlobalClass.isPause)
            {
                delayTime += gameTime.ElapsedGameTime.Milliseconds;
                if (delayTime >= 1000)
                {
                    delayTime = 0.0f;
                    GlobalClass.timePlaying++;
                }
                waveManager.Update(gameTime, player, skillManager);
                skillManager.Update(gameTime);
            }


        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (isHowTo)
            {
                gotIt.Draw(spriteBatch);
                howto.Draw(spriteBatch);
            }
            waveManager.Draw(gameTime, spriteBatch);
            bg.Draw(spriteBatch);

            for (int i = 0; i < player.Length; i++)
                if (player[i] != null && player[i].isAlive)
                    player[i].Draw(gameTime, spriteBatch);

            skillManager.Draw(gameTime, spriteBatch);
            if (GlobalClass.isPause)
            {
                spriteBatch.Draw(fadeTexture, new Vector2(GlobalClass.ScreenWidth / 2, GlobalClass.ScreenHeight / 2), null, Color.White, 0.0f, new Vector2(fadeTexture.Width / 2, fadeTexture.Height / 2), 30f, SpriteEffects.None, 0.15f);
            }

            if (!isHowTo)
            {
                settingPopup.Draw(spriteBatch);
                settingButton.Draw(spriteBatch);
            }
        }

        public void RenderParticles()
        {
            foreach (ParticleEffect effect in sideEffect.effects)
            {
                GameManager.particlesRender.RenderEffect(effect, ref GameManager.transform);
            }

            foreach (Player robo in player)
            {
                if (robo.isControl)
                    GameManager.particlesRender.RenderEffect(robo.particleEffect, ref GameManager.transform);
            }

           

            foreach (Monster enemy in waveManager.wave.monsters)
            {
                if (enemy is BossTwo || enemy is BossOne)
                {
                    if (enemy.isAlive)
                        GameManager.particlesRender.RenderEffect(enemy.particleEffect, ref GameManager.transform);
                }
                else if (enemy.isAttackable)
                    GameManager.particlesRender.RenderEffect(enemy.particleEffect, ref GameManager.transform);
            }

        }
        public void RenderVictoryParticle()
        {
            GameManager.particlesRender.RenderEffect(CyclebotNewFormNotif.effect, ref GameManager.transform);
        }
    }
}
