using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Facebook;


namespace RecycleRobo.Scenes
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class TweetBox
    {
        TextBox textbox;
        Texture2D boxTexture;
        Vector2 boxPos;
        GameManager game;
        public bool isEnter = false;
        Button okButton, cancelButton;
        public TweetBox(GameManager game)
        {
            this.game = game;
            Initialize();
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public void Initialize()
        {

            boxTexture = game.Content.Load<Texture2D>("Victory/TweetBox/box");
            boxPos = new Vector2(GlobalClass.ScreenWidth / 2 - boxTexture.Width * 0.625f / 2, GlobalClass.ScreenHeight / 2 - boxTexture.Height * 0.625f / 2);


            Texture2D checkTexture = game.Content.Load<Texture2D>("Setting/official/checkButton");
            Texture2D crossTexture = game.Content.Load<Texture2D>("Setting/official/crossButton");
            Texture2D checkTextureHover = game.Content.Load<Texture2D>("Setting/official/checkButtonHover");
            Texture2D crossTextureHover = game.Content.Load<Texture2D>("Setting/official/crossButtonHover");

            okButton = new Button(checkTexture, checkTextureHover, checkTexture, 0.625f, 0f, boxPos + new Vector2(boxTexture.Width * 0.625f - checkTexture.Width * 0.625f / 2 + 10, 136));

            cancelButton = new Button(crossTexture, crossTextureHover, crossTexture, 0.625f, 0f, boxPos + new Vector2(-checkTexture.Width*0.625f/2 + 10, 136));
            
            okButton.layer = 0.0008f;
            cancelButton.layer = 0.0008f;


            textbox = new TextBox(game.Content.Load<Texture2D>("Victory/TweetBox/textBox"), game.Content.Load<Texture2D>("Victory/TweetBox/caret"), game.Content.Load<SpriteFont>("Victory/boxfont"));
            textbox.Center = boxPos + new Vector2(147,50);
            textbox.TextOffset = new Vector2(66, 3);
            textbox.caret_offset = 34;
            textbox.Width = 220;
            textbox.Highlighted = false;
            textbox.TextColor = Color.Black;
            textbox.OnEnterPressed += TextBoxEnter_Pressed;
            okButton.Clicked += okButton_Clicked;
            cancelButton.Clicked += cancelButton_Clicked;
            GameManager.keyboard_dispatcher.Subscriber = textbox;
            // TODO: Add your initialization code here
        }
        public void TextBoxEnter_Pressed(TextBox sender)
        {
            Twitter.GetAccessToken(textbox.Text);
            isEnter = true;
        }
        public void okButton_Clicked(object o, EventArgs e)
        {
            Twitter.GetAccessToken(textbox.Text);
            isEnter = true;
        }
        public void cancelButton_Clicked(object o, EventArgs e)
        {
            isEnter = true;
        }
        public void Update(GameTime gameTime)
        {
            textbox.Update(gameTime);
            cancelButton.Update(gameTime);
            okButton.Update(gameTime);
        }
        public void Draw(SpriteBatch spriteBatch,GameTime gameTime)
        {
            okButton.Draw(spriteBatch);
            cancelButton.Draw(spriteBatch);
            textbox.Draw(spriteBatch, gameTime);
           // spriteBatch.Draw(boxTexture, boxPos, null, Color.White, 0f, Vector2.Zero, 0.625f, SpriteEffects.None, 0.09f);

        }
    }
}
