﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace RecycleRobo.Scenes.Victory
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    ////
    public class EleComponent
    {
        public string player;
        public string dock;
        public Animation gesture;
        public EleComponent(string player, string dock, Animation gesture)
        {
            this.player = player;
            this.dock = dock;
            this.gesture = gesture;
        }
    }
    public class VectorCompoment
    {
        public Vector2 player;
        public Vector2 dock;
        public Vector2 gesture;
        public VectorCompoment(Vector2 player, Vector2 dock, Vector2 gesture)
        {
            this.player = player;
            this.dock = dock;
            this.gesture = gesture;
        }

    }
    public class ElementNotif : Notification
    {
        Dictionary<Texture2D, EleComponent> compoment;
        Dictionary<Texture2D, VectorCompoment> compomentPos;

        Texture2D elementform;

        AnimationPlayer gesture;
        AnimationPlayer player;
        AnimationPlayer dock;
        
        Vector2 position;
        Vector2 target = new Vector2(250, 120);
        float speed = 1f;
        float rotate = 0.5f;
        float opacity = 0f;

        Vector2 infoPositon;
        Vector2 targetInfoPosition;
        
        float scale = 0.625f;
        Vector2 playerPos;
        Vector2 dockPos;
        Vector2 gesturePos;
        Vector2 targetPlayerPos = new Vector2(310, 320);
        Vector2 targetDockPos = new Vector2(310, 410);
        Vector2 targetGesturePos = new Vector2(330, 350);
        public ElementNotif(Texture2D elementform)
        {
            this.elementform = elementform;

            position = new Vector2(200, -elementform.Height*scale);
            playerPos = new Vector2(250, targetPlayerPos.Y - (120 + elementform.Height * scale));
            gesturePos = new Vector2(200, targetGesturePos.Y - (120 + elementform.Height * scale));
            dockPos = new Vector2(200, targetDockPos.Y - (120 + elementform.Height * scale));

            infoPositon = new Vector2(GlobalClass.ScreenWidth, 0);
            targetInfoPosition = new Vector2(GlobalClass.ScreenWidth - Asset.inforEle.Width * scale, 0);

            gesture.OrderLayer = 0.0002f;
            gesture.Scale = 0.625f;
            gesture.color = Color.White;

            player.OrderLayer = 0.0003f;
            player.Scale = 0.625f;
            player.color = Color.White;

            dock.OrderLayer = 0.0007f;
            dock.Scale = 0.3f;
            dock.color = Color.White;
            InitElementComponent();
            
        }
        private void InitElementComponent(){
            compoment = new Dictionary<Texture2D, EleComponent>();

            compoment.Add(Asset.desc1_2, new EleComponent("NUMBER1_IDLE_1_THORN", "NUM1_AREA_2", Asset.gestureN));
            compoment.Add(Asset.desc1_3, new EleComponent("NUMBER1_IDLE_2_SEAL", "NUM1_AREA_3", Asset.gestureZ));
            compoment.Add(Asset.desc1_4, new EleComponent("NUMBER1_IDLE_3_FIRE", "NUM1_AREA_4", Asset.gestureNone));

            compoment.Add(Asset.desc2_2, new EleComponent("NUMBER2_IDLE_1", "NUM2_AREA_1", Asset.gestureNone));
            compoment.Add(Asset.desc2_3, new EleComponent("NUMBER2_IDLE_2", "NUM2_AREA_1", Asset.gestureNone));
            compoment.Add(Asset.desc2_4, new EleComponent("NUMBER2_IDLE_3_PHOENIX", "NUM2_AREA_4", Asset.gestureTap));

            compoment.Add(Asset.desc3_2, new EleComponent("NUMBER3_IDLE_2_CUTTER", "NUM3_AREA_2", Asset.gestureO));
            compoment.Add(Asset.desc3_3, new EleComponent("NUMBER3_IDLE_2_ICEFANG", "NUM3_AREA_3", Asset.gestureU));
            compoment.Add(Asset.desc3_4, new EleComponent("NUMBER3_IDLE_3", "NUM3_AREA_1", Asset.gestureNone));

            compoment.Add(Asset.desc4_2, new EleComponent("NUMBER4_IDLE_1", "NUM4_AREA_1", Asset.gestureNone));
            compoment.Add(Asset.desc4_3, new EleComponent("NUMBER4_IDLE_2_HERB", "NUM4_AREA_3", Asset.gestureV));
            compoment.Add(Asset.desc4_4, new EleComponent("NUMBER4_IDLE_3", "NUM4_AREA_1", Asset.gestureNone));

            compoment.Add(Asset.desc5_2, new EleComponent("NUMBER5_IDLE_1_THORN", "NUM5_AREA_2", Asset.gestureN));
            compoment.Add(Asset.desc5_3, new EleComponent("NUMBER5_IDLE_2_WAVE", "NUM5_AREA_3", Asset.gestureHorizontal));
            compoment.Add(Asset.desc5_4, new EleComponent("NUMBER5_IDLE_3_STEAM", "NUM5_AREA_4", Asset.gestureVerticle));

            compomentPos = new Dictionary<Texture2D, VectorCompoment>();
            compomentPos.Add(Asset.desc1_2, new VectorCompoment(targetPlayerPos + new Vector2(12,10), targetDockPos + new Vector2(5, -5), targetGesturePos + new Vector2(10, 0)));
            compomentPos.Add(Asset.desc1_3, new VectorCompoment(targetPlayerPos + new Vector2(-5,-10), targetDockPos + new Vector2(-14,-20), targetGesturePos));
            compomentPos.Add(Asset.desc1_4, new VectorCompoment(targetPlayerPos, targetDockPos + new Vector2(-10, -13), targetGesturePos));

            compomentPos.Add(Asset.desc2_2, new VectorCompoment(targetPlayerPos, targetDockPos, targetGesturePos));
            compomentPos.Add(Asset.desc2_3, new VectorCompoment(targetPlayerPos, targetDockPos, targetGesturePos));
            compomentPos.Add(Asset.desc2_4, new VectorCompoment(targetPlayerPos + new Vector2(-20,10), targetDockPos, targetGesturePos));

            compomentPos.Add(Asset.desc3_2, new VectorCompoment(targetPlayerPos + new Vector2(20, -20), targetDockPos + new Vector2(0, -15), targetGesturePos));
            compomentPos.Add(Asset.desc3_3, new VectorCompoment(targetPlayerPos, targetDockPos, targetGesturePos));
            compomentPos.Add(Asset.desc3_4, new VectorCompoment(targetPlayerPos + new Vector2(-20, -20), targetDockPos, targetGesturePos));

            compomentPos.Add(Asset.desc4_2, new VectorCompoment(targetPlayerPos, targetDockPos, targetGesturePos));
            compomentPos.Add(Asset.desc4_3, new VectorCompoment(targetPlayerPos + new Vector2(5,-4), targetDockPos, targetGesturePos + new Vector2(15,0)));
            compomentPos.Add(Asset.desc4_4, new VectorCompoment(targetPlayerPos + new Vector2(-10, -60), targetDockPos + new Vector2(10, 0), targetGesturePos));

            compomentPos.Add(Asset.desc5_2, new VectorCompoment(targetPlayerPos , targetDockPos, targetGesturePos));
            compomentPos.Add(Asset.desc5_3, new VectorCompoment(targetPlayerPos + new Vector2(10,20), targetDockPos + new Vector2(-20,-40), targetGesturePos + new Vector2(20,20)));
            compomentPos.Add(Asset.desc5_4, new VectorCompoment(targetPlayerPos + new Vector2(-30,-22), targetDockPos + new Vector2(-30, -40), targetGesturePos));
        }
        public override void Update(GameTime gameTime)
        {
            MouseState mouse = Mouse.GetState();
         //   Console.WriteLine(mouse);
            Vector2 direction = target - position;
            Vector2 directionInfo = targetInfoPosition - infoPositon;

            Vector2 directionPlayer = compomentPos[elementform].player - playerPos;
            Vector2 directionGesture = compomentPos[elementform].gesture - gesturePos;
            Vector2 directionDock = compomentPos[elementform].dock - dockPos;


            if (rotate > 0f)
            {
                rotate -= 0.05f;
            }
            if (rotate < 0)
            {
                rotate = 0;
            }

            direction.Normalize();
            directionInfo.Normalize();
            directionPlayer.Normalize();
            directionDock.Normalize();
            directionGesture.Normalize();

            if (position.Y < target.Y)
            {
                position += direction * (float)gameTime.ElapsedGameTime.Milliseconds * speed;
            }
            if (playerPos.Y < compomentPos[elementform].player.Y)
            {
                playerPos += directionPlayer * (float)gameTime.ElapsedGameTime.Milliseconds * speed;
            }
            if (gesturePos.Y < compomentPos[elementform].gesture.Y)
            {
                gesturePos += directionGesture * (float)gameTime.ElapsedGameTime.Milliseconds * speed;
            }
            if (dockPos.Y < compomentPos[elementform].dock.Y)
            {
                dockPos += directionDock * (float)gameTime.ElapsedGameTime.Milliseconds * speed;
            }
            if (infoPositon.X > targetInfoPosition.X)
            {
                infoPositon += directionInfo * (float)gameTime.ElapsedGameTime.Milliseconds * speed;
            }
            opacity += 0.08f;
            base.Update(gameTime);
        }
        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            gesture.PlayAnimation(compoment[elementform].gesture);
            gesture.Draw(gameTime, spriteBatch, gesturePos);
            
            player.PlayAnimation(GlobalClass.getAnimationByName(compoment[elementform].player));
            player.Draw(gameTime, spriteBatch, playerPos);

            dock.PlayAnimation(GlobalClass.getAnimationByName(compoment[elementform].dock));
            dock.Draw(gameTime, spriteBatch, dockPos);

            spriteBatch.Draw(elementform, position, new Rectangle(0, 0, 832, 558), Color.White, rotate, Vector2.Zero, scale, SpriteEffects.None, 0.001f);
            spriteBatch.Draw(Asset.inforEle, infoPositon, null, Color.White, 0f, Vector2.Zero, scale, SpriteEffects.None, 0.001f);
            spriteBatch.Draw(Asset.typeEle, new Vector2(0, 0), null, Color.White * opacity, 0f, Vector2.Zero, scale, SpriteEffects.None, 0.001f);
            base.Draw(gameTime, spriteBatch);
        }
    }
}
