﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using ProjectMercury;




namespace RecycleRobo.Scenes.Victory
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class CyclebotNewFormNotif : Notification
    {
        Texture2D newCyclebot;
        Texture2D oldCyclebot;

        Vector2 position;
        Vector2 target = new Vector2(0, 0);
        float speed = 1f;
        float rotate = 0f;
        float oldOpacity = 1f;
        float newOpacity = 0f;
        float opacity = 0f;
        public static ParticleEffect effect;
        Vector2 infoPositon;
        Vector2 targetInfoPosition;
        float scale = 0.625f;
        public CyclebotNewFormNotif(Texture2D newCyclebot, Texture2D oldCyclebot)
        {
            this.newCyclebot = newCyclebot;
            this.oldCyclebot = oldCyclebot;
            position = target;
            infoPositon = new Vector2(GlobalClass.ScreenWidth, 0);
            targetInfoPosition = new Vector2(GlobalClass.ScreenWidth - Asset.inforCycle.Width * scale, 0);
        }
        public override void Update(GameTime gameTime)
        {
            MouseState mouse = Mouse.GetState();
           // Console.WriteLine(mouse);
            Vector2 directionInfo = targetInfoPosition - infoPositon;
            directionInfo.Normalize();

            if (infoPositon.X > targetInfoPosition.X)
            {
                infoPositon += directionInfo * (float)gameTime.ElapsedGameTime.Milliseconds * speed;
            }
            if (oldOpacity > 0)
                oldOpacity -= 0.01f;
            else
                newOpacity += 0.01f;
            opacity += 0.03f;
            effect.Update(1f);
            base.Update(gameTime);
        }
        public override void Draw(GameTime gameTime,SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(newCyclebot, position, null, Color.White * newOpacity, rotate, Vector2.Zero, scale, SpriteEffects.None, 0.001f);
            spriteBatch.Draw(oldCyclebot, position, null, Color.White* oldOpacity, rotate, Vector2.Zero, scale, SpriteEffects.None, 0.001f);
            spriteBatch.Draw(Asset.inforCycle, infoPositon, null, Color.White, 0f, Vector2.Zero, scale, SpriteEffects.None, 0.001f);
            spriteBatch.Draw(Asset.typeCycle, new Vector2(0, 0), null, Color.White * opacity, 0f, Vector2.Zero, scale, SpriteEffects.None, 0.001f);
            
            if (oldOpacity > 0)
            {
                effect.Trigger(new Vector2(400, 330));
                effect.Trigger(new Vector2(330, 330));
                effect.Trigger(new Vector2(500, 380));
                effect.Trigger(new Vector2(500, 280));
                effect.Trigger(new Vector2(450, 220));
                effect.Trigger(new Vector2(450, 450));

            }


            base.Draw(gameTime, spriteBatch);
        }
    }
}