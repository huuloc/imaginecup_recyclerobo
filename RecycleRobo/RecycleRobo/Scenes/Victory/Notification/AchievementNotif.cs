using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace RecycleRobo.Scenes.Victory
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class AchievementNotif:Notification
    {
        Texture2D achievement;
        Vector2 position;
        Vector2 target = new Vector2(300, 50);
        float speed = 1f;
        float rotate = 0.5f;
        float opacity = 0f;
        Vector2 infoPositon;
        Vector2 targetInfoPosition = new Vector2(750, 120);
        Vector2 ballPosition;
        Vector2 targetBallPosition = new Vector2(770, 202);
        float scale = 0.625f;
        int ball;
        public AchievementNotif(Texture2D achive, int ball)
        {
            this.ball = ball;
            achievement = achive;
            position = new Vector2(600, -600);
            infoPositon = new Vector2(GlobalClass.ScreenWidth,120);
            ballPosition = new Vector2(GlobalClass.ScreenWidth + 20, 198);
        }
        public override void Update(GameTime gameTime)
        {
            MouseState mouse = Mouse.GetState();
            //Console.WriteLine(mouse);   
            Vector2 direction = target - position;
            Vector2 directionInfo = targetInfoPosition - infoPositon;
            Vector2 directionBall = targetBallPosition - ballPosition;


            if (rotate > 0f)
            {
                rotate -= 0.03f;
            }
            if (rotate < 0)
            {
                rotate = 0;
            }

            direction.Normalize();
            directionInfo.Normalize();
            directionBall.Normalize();
            if (position.Y < target.Y)
            {
                position += direction * (float)gameTime.ElapsedGameTime.Milliseconds * speed;
            }
            if (infoPositon.X > targetInfoPosition.X)
            {
                infoPositon += directionInfo * (float)gameTime.ElapsedGameTime.Milliseconds * speed;
            }
            if (ballPosition.X > targetBallPosition.X)
            {
                ballPosition += directionBall * (float)gameTime.ElapsedGameTime.Milliseconds * speed;

            }
            opacity += 0.08f;
            base.Update(gameTime);
        }
        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(achievement, position,new Rectangle (0,0,596,765), Color.White, rotate, Vector2.Zero, scale, SpriteEffects.None, 0.001f);
            spriteBatch.Draw(Asset.inforAchie, infoPositon, null, Color.White, 0f, Vector2.Zero, 0.55f, SpriteEffects.None, 0.001f);
            spriteBatch.Draw(Asset.typeAchie, new Vector2(0, 0), null, Color.White * opacity, 0f, Vector2.Zero, scale, SpriteEffects.None, 0.001f);
            spriteBatch.DrawString(Asset.largeFont,Convert.ToString(ball),ballPosition,Color.Black,0f,Vector2.Zero,0.7f,SpriteEffects.None,0.001f);
            base.Draw(gameTime, spriteBatch);
        }
    }
}
