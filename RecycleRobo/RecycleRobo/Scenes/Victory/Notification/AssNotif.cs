using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace RecycleRobo.Scenes.Victory
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class AssNotif : Notification
    {
        Texture2D newAss;
        Vector2 position;
        Vector2 target = new Vector2(300, 50);
        float speed = 1f;
        float rotate = 0.5f;
        float opacity = 0f;

        Vector2 infoPositon;
        Vector2 targetInfoPosition = new Vector2(750, 120);
        float scale = 0.625f;
        public AssNotif(Texture2D newAss)
        {
            this.newAss = newAss;
            position = new Vector2(600, GlobalClass.ScreenHeight);
            infoPositon = new Vector2(GlobalClass.ScreenWidth, 120);
        }
        public override void Update(GameTime gameTime)
        {
            MouseState mouse = Mouse.GetState();
            //Console.WriteLine(mouse);
            Vector2 direction = target - position;
            Vector2 directionInfo = targetInfoPosition - infoPositon;

            if (rotate > 0f)
            {
                rotate -= 0.03f;
            }
            if (rotate < 0)
            {
                rotate = 0;
            }

            direction.Normalize();
            directionInfo.Normalize();

            if (position.Y > target.Y)
            {
                position += direction * (float)gameTime.ElapsedGameTime.Milliseconds * speed;
            }
            if (infoPositon.X > targetInfoPosition.X)
            {
                infoPositon += directionInfo * (float)gameTime.ElapsedGameTime.Milliseconds * speed;
            }
            opacity += 0.08f;
            base.Update(gameTime);
        }
        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(newAss, position, new Rectangle(0, 0, 505, 756), Color.White, rotate, Vector2.Zero, scale, SpriteEffects.None, 0.001f);
            spriteBatch.Draw(Asset.inforAss, infoPositon, null, Color.White, 0f, Vector2.Zero, 0.55f, SpriteEffects.None, 0.001f);
            spriteBatch.Draw(Asset.typeAss, new Vector2(0, 0), null, Color.White * opacity, 0f, Vector2.Zero, scale, SpriteEffects.None, 0.001f);
            base.Draw(gameTime,spriteBatch);
        }
    }
}
