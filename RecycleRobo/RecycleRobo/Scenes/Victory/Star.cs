﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace RecycleRobo.Scenes.Victory
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class Star
    {
        GameManager game;
        Texture2D starTexture;
        public float scale = 0.625f;
        public float layer = 0.004f;
        public float speed = 0.25f;
        Vector2 pos;
        Vector2 target = new Vector2(300, 50);
        public Star(GameManager game, Vector2 pos, Vector2 target, float scale)
        {
            this.game = game;
            this.pos = pos;
            this.target = target;
            this.scale = scale;
            starTexture = game.Content.Load<Texture2D>("Victory/victory/star");
        }
        public void Update(GameTime gameTime)
        {
            Vector2 direction = target - pos;
            direction.Normalize();
            if (pos.Y < target.Y)
            {
                pos += direction * (float)gameTime.ElapsedGameTime.TotalMilliseconds * speed;
            }
        }
        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(starTexture, pos, null, Color.White, 0f, Vector2.Zero, scale, SpriteEffects.None, layer);
        }
    }
}
