using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using RecycleRobo.Scenes;
using RecycleRobo.Scenes.Victory;
using RecycleRobo.Scenes.Victory.RecycleRobo.Scenes.Victory;
using LoadData;
using System.Threading;

namespace RecycleRobo
{
	public class SocialContent
	{
		public string message;
		public string imagePath;
		public SocialContent(string message, string imagePath)
		{
			this.message = message;
			this.imagePath = imagePath;
		}
	}
	public class ResourceAchievement{
        public int cyclebotBattle;
        public int cyclebotUnlock;
        public int energyBall;
        public int cyclebotThirdForm;
        public int mapUnlock;
        public int achieveComplete;
        public ResourceAchievement()
    {
    
    }
		public ResourceAchievement(int cyclebotBattle, int cyclebotUnlock, int energyBall, int cyclebotThirdForm, int mapUnlock, int achieveComplete)
		{
            this.cyclebotBattle = cyclebotBattle;
            this.cyclebotUnlock = cyclebotUnlock;
            this.cyclebotThirdForm = cyclebotThirdForm;
            this.mapUnlock = mapUnlock;
            this.achieveComplete = achieveComplete;
			this.energyBall = energyBall;
		}
		public bool CompareResoure(ResourceAchievement re)
		{
            if (re.cyclebotBattle < this.cyclebotBattle)
				return false;
			if (re.cyclebotUnlock < this.cyclebotUnlock)
				return false;
			if (re.cyclebotThirdForm < this.cyclebotThirdForm)
				return false;
			if (re.energyBall < this.energyBall)
				return false;
			if (re.mapUnlock < this.mapUnlock)
				return false;
			if (re.achieveComplete  < this.achieveComplete)
				return false;
			return true;
		}
	}

	public class VictoryScene
	{
		GameManager game;

		CircleObject circle;
		Button nextButton, tweetButton, faceButton;
		List<Notification> notif;
		List<SocialContent> twitterContent = new List<SocialContent>();
		List<SocialContent> faceContent  = new List<SocialContent>();
		Background newspaper;
		bool isNotif;
		int next = -1;
		int energyBall = 0;
		Vector2 facePos;
		Vector2 targetFacePos;
		Vector2 tweetPos;
		Vector2 targetTweetPos;
		bool isTwitterClicked = false;
		bool isFaceClicked = true;
		bool isTextbox  = false;
		TextBox textbox;
        Dictionary<string, dynamic> mapPlayer;
        List<Star> listStar;
        CalCyclebotExp calCyclebotExp;
        FunFactNotif funFactNotif;
        private LevelData dataLevel;
        RoboAvailable roboAvail;
        TweetBox tweetBox;

        public static Player[] player;

		public VictoryScene(GameManager game)
		{
			this.game = game;
			notif = new List<Notification>();
            calCyclebotExp = new CalCyclebotExp(game);
            funFactNotif = new FunFactNotif(game);
			Initialize();
            EvaluateStar();

            EvaluateAchieveNotif();

            EvaluateNewCyclebotNotif();

            EvaluateNewCyclebotNotif();

            EvaluateAssNotif();

            EvaluateNewFormCyclebotNotif();

            EvaluateElementNotif();

			nextButton.Clicked += new EventHandler(nextButton_Clicked);
			tweetButton.Clicked += new EventHandler(tweetButton_Clicked);
			faceButton.Clicked += new EventHandler(faceButton_Clicked);

            MediaPlayer.Volume = (float)GlobalClass.volumeMusic / 100;
            MediaPlayer.Play(game.Content.Load<Song>("sounds/victory"));
            MediaPlayer.IsRepeating = true;


		}
        public void EvaluateStar()
        {
            LevelData dataLevel = game.Content.Load<LevelData>("Data/Map/Map" + GlobalClass.curMapPlaying + "/Level" + GlobalClass.curLevelPlaying);
            Dictionary<int, int> calStar = new Dictionary<int, int>();
            listStar = new List<Star>();
            int star = 0;
            if (GlobalClass.timePlaying <= dataLevel.threeStar)
            {
                star = 3;
                listStar.Add(new Star(game, new Vector2(330, -80), new Vector2(330, 250), 0.5f));
                listStar.Add(new Star(game, new Vector2(100, -80), new Vector2(100, 250), 0.5f));
                listStar.Add(new Star(game, new Vector2(200, -80), new Vector2(200, 270), 0.7f));
            }
            else if (GlobalClass.timePlaying <= dataLevel.twoStar && GlobalClass.timePlaying > dataLevel.threeStar)
            {
                star = 2;
                listStar.Add(new Star(game, new Vector2(110, -80), new Vector2(110, 260), 0.625f));
                listStar.Add(new Star(game, new Vector2(270, -80), new Vector2(270, 260), 0.625f));
            }
            else
            {
                listStar.Add(new Star(game, new Vector2(250, -80), new Vector2(250, 260), 0.7f));
                star = 1;
            }
            Level data = new Level();
            data.rating = star;
            data.isUnlock = true;
            new Data().Save(data, "Map/Map_" + Convert.ToString(GlobalClass.curMapPlaying) + "/Level" + Convert.ToString(GlobalClass.curLevelPlaying) + ".xml");
            data.rating = -1;
            data.isUnlock = true;
            if (GlobalClass.curMapPlaying == 1 && GlobalClass.curLevelPlaying < 7)
            {
                if (new Data().Load<Level>("Map/Map_" + Convert.ToString(GlobalClass.curMapPlaying) + "/Level" + Convert.ToString(GlobalClass.curLevelPlaying + 1) + ".xml").isUnlock == false)
                {
                    new Data().Save(data, "Map/Map_" + Convert.ToString(GlobalClass.curMapPlaying) + "/Level" + Convert.ToString(GlobalClass.curLevelPlaying + 1) + ".xml");
                }
            }
            else if (GlobalClass.curMapPlaying == 2 && GlobalClass.curLevelPlaying < 8)
            {
                if (new Data().Load<Level>("Map/Map_" + Convert.ToString(GlobalClass.curMapPlaying) + "/Level" + Convert.ToString(GlobalClass.curLevelPlaying + 1) + ".xml").isUnlock == false)
                {
                    new Data().Save(data, "Map/Map_" + Convert.ToString(GlobalClass.curMapPlaying) + "/Level" + Convert.ToString(GlobalClass.curLevelPlaying + 1) + ".xml");
                }
            }


        }
		private void tweetButton_Clicked(object o, EventArgs e)
		{
            if (new Data().Load<Access>("Social/Twitter.xml") == null && FacebookUtility.IsNetworkAvailable())
            {
                if (GlobalClass.isFullScreen)
                {
                    Setting.setNormalScreen();
                    GlobalClass.isFullScreen = false;     
                }

            }
            if (FacebookUtility.IsNetworkAvailable())
			{
				isTwitterClicked = true;
				Thread thread = new Thread(Tweet);
				thread.SetApartmentState(ApartmentState.STA);
				thread.Start();
			}
		}
		private void Tweet()
		{
            if (!Twitter.PostPoto(twitterContent[next].message, twitterContent[next].imagePath) && new Data().Load<Access>("Social/Twitter.xml") == null)
			{
                Twitter.GetPinAuthenticate();

                tweetBox.isEnter = false;
				isTextbox = true;
			}
		}
		private void faceButton_Clicked(object o, EventArgs e)
		{
            try
            {
                dynamic info = FacebookUtility.GetInfo();
            }
            catch (Exception)
            {
                if (GlobalClass.isFullScreen)
                {
                    Setting.setNormalScreen();
                    GlobalClass.isFullScreen = false;
                }
            }
			if (FacebookUtility.IsNetworkAvailable())
			{
				isFaceClicked = true;
				faceButton.texture = Asset.facebookButton_Hover;
				Thread thread = new Thread(Face);
				thread.SetApartmentState(ApartmentState.STA);
				thread.Start();
			}
		}
		private void Face()
		{
			if (!FacebookUtility.PostPhoto(faceContent[next].message, faceContent[next].imagePath))
			{
				if (!FacebookUtility.Login())
				{
					isFaceClicked = false;
				}
			}
		}

		private void EvaluateElementNotif()
		{
            Player[] players = player;
			//Just for Test;
            //Player[] players = new Player[4];
            //players[0] = new NumberFive(game.Content, Vector2.Zero, null, game);
            //players[1] = new NumberTwo(game.Content, Vector2.Zero, null, game);
            //players[2] = new NumberThree(game.Content, Vector2.Zero, null, game);
            //players[3] = new NumberFour(game.Content, Vector2.Zero, null, game);


			Exp exp = new Exp(players);
			exp.Calcualate();

			Dictionary<string, PlayerAtt> playerInfo = Load(players);
			Dictionary<string, PlayerAtt> playerInfoNext = exp.dictPlayer;

			Dictionary<string, int[]> dictNewFormOrSkill = new Dictionary<string, int[]>();
			int[] numberOne = { 6, 10, 15 };
			int[] numberTwo = { 7, 10, 15 };
			int[] numberThree = { 10, 14, 18 };
			int[] numberFour = { 0, 10, 15 };
			int[] numberFive = { 10, 15, 20 };

			dictNewFormOrSkill.Add("NumberOne", numberOne);
			dictNewFormOrSkill.Add("NumberTwo", numberTwo);
			dictNewFormOrSkill.Add("NumberThree", numberThree);
			dictNewFormOrSkill.Add("NumberFour", numberFour);
			dictNewFormOrSkill.Add("NumberFive", numberFive);

			Dictionary<string, Texture2D[]> mapFormTexture = new Dictionary<string, Texture2D[]>();
			//Waite for  Demo Done;
			Texture2D[] tN1 = { Asset.desc1_2, Asset.desc1_3, Asset.desc1_4 };
			Texture2D[] tN2 = { Asset.desc2_2, Asset.desc2_3, Asset.desc2_4 };
			Texture2D[] tN3 = { Asset.desc3_2, Asset.desc3_3, Asset.desc3_4 };
			Texture2D[] tN4 = { Asset.desc4_2, Asset.desc4_3, Asset.desc4_4 };
			Texture2D[] tN5 = { Asset.desc5_2, Asset.desc5_3, Asset.desc5_4 };

			mapFormTexture.Add("NumberOne", tN1);
			mapFormTexture.Add("NumberTwo", tN2);
			mapFormTexture.Add("NumberThree", tN3);
			mapFormTexture.Add("NumberFour", tN4);
			mapFormTexture.Add("NumberFive", tN5);

			foreach (string playerName in playerInfo.Keys)
			{
				for (int i = 0; i < dictNewFormOrSkill[playerName].Length; i++)
				{
					if (dictNewFormOrSkill[playerName][i] <= playerInfoNext[playerName].level && dictNewFormOrSkill[playerName][i] > playerInfo[playerName].level)
					{
                        faceContent.Add(new SocialContent("I just unlock a new element-form! Wow... #GreenHeaven", "Social/Form/" + playerName + "/" + Convert.ToString(i + 2) + ".png"));
                        twitterContent.Add(new SocialContent("I just unlock a new element-form! Wow... #GreenHeaven", "Social/Form/" + playerName + "/" + Convert.ToString(i + 2) + ".png"));
						notif.Add(new ElementNotif(mapFormTexture[playerName][i]));
					}
				}
			}
            exp.Save();
		}
		private void EvaluateAchieveNotif()
		{
            Dictionary<int, int> awardAchievement = new Dictionary<int, int>();

            awardAchievement.Add(1, 20);
            awardAchievement.Add(2, 30);
            awardAchievement.Add(3, 30);
            awardAchievement.Add(4, 50);
            awardAchievement.Add(5, 50);
            awardAchievement.Add(6, 100);
            awardAchievement.Add(7, 250);
            awardAchievement.Add(8, 90);
            awardAchievement.Add(9, 500);
            awardAchievement.Add(10, 500);
            awardAchievement.Add(11, 150);
            awardAchievement.Add(12, 100);
            awardAchievement.Add(13, 500);


            Dictionary<int, Texture2D> mapAchieveTexture = new Dictionary<int, Texture2D>();
            mapAchieveTexture.Add(1, Asset.challenge1completed);
            mapAchieveTexture.Add(2, Asset.challenge2completed);
            mapAchieveTexture.Add(3, Asset.challenge3completed);
            mapAchieveTexture.Add(4, Asset.challenge4completed);
            mapAchieveTexture.Add(5, Asset.challenge5completed);
            mapAchieveTexture.Add(6, Asset.challenge6completed);
            mapAchieveTexture.Add(7, Asset.challenge7completed);
            mapAchieveTexture.Add(8, Asset.challenge8completed);
            mapAchieveTexture.Add(9, Asset.challenge9completed);
            mapAchieveTexture.Add(10, Asset.challenge10completed);
            mapAchieveTexture.Add(11, Asset.challenge11completed);
            mapAchieveTexture.Add(12, Asset.challenge12completed);
            mapAchieveTexture.Add(13, Asset.challenge13completed);

			
			Challenge challenge = new Data().Load<Challenge>("Challenge/ChallengeCompleted.xml");
            List<int> newAchieveCompleted = new List<int>();
            if (challenge.completed != null)
            {
                newAchieveCompleted.InsertRange(0, challenge.completed);
            }

			Dictionary<int, ResourceAchievement> achievemntResource = new Dictionary<int, ResourceAchievement>();
			//int cyclebotBattle, 
            //int cyclebotUnlock, int energyBall, 
            //int cyclebotThirdForm, int mapUnlock, int achieveComplete)

			achievemntResource.Add(1, new ResourceAchievement(2, 0, 0, 0, 0, 0));
            achievemntResource.Add(2, new ResourceAchievement(3, 0, 0, 0, 0, 0));
            achievemntResource.Add(3, new ResourceAchievement(0, 3, 0, 0, 0, 0));
            achievemntResource.Add(4, new ResourceAchievement(0, 5, 0, 0, 0, 0));
            achievemntResource.Add(5, new ResourceAchievement(0, 0, 100, 0, 0, 0));
            achievemntResource.Add(6, new ResourceAchievement(0, 0, 300, 0, 0, 0));
            achievemntResource.Add(7, new ResourceAchievement(0, 0, 700, 0, 0, 0));
            achievemntResource.Add(8, new ResourceAchievement(0, 0, 0, 3, 0, 0));
            achievemntResource.Add(9, new ResourceAchievement(0, 0, 0, 5, 0, 0));
            achievemntResource.Add(10, new ResourceAchievement(0, 0, 1000, 0, 0, 0));
            achievemntResource.Add(11, new ResourceAchievement(0, 0, 0, 0, 3, 0));
            achievemntResource.Add(12, new ResourceAchievement(0, 0, 0, 0, 0, 6));
            achievemntResource.Add(13, new ResourceAchievement(0, 0, 0, 0, 0, 12));






            UnifiedRes resBall = new UnifiedRes();
            resBall = new Data().Load<UnifiedRes>("Resource/UnifiedRes.xml");
            resBall.available += calculateBall();

            ResourceAchievement resourceFromBattle = new ResourceAchievement();
            
            if (challenge.completed != null)
            {
                resourceFromBattle.achieveComplete = challenge.completed.Length;
            }
            else
            {
                resourceFromBattle.achieveComplete = 0;
            }
            resourceFromBattle.cyclebotUnlock = calculateCycleBotUnlock();
            resourceFromBattle.energyBall =  resBall.available;
            resourceFromBattle.cyclebotThirdForm = calculateCyclebotThridForm();
            resourceFromBattle.cyclebotBattle = player.Length;
            // Never Complete this achievement because map3 is Never unlock; :))
            resourceFromBattle.mapUnlock = 1;
			for (int i = 1; i <= achievemntResource.Count; i++)
			{
                if (challenge.completed == null && achievemntResource[i].CompareResoure(resourceFromBattle))
                {
                    resourceFromBattle.achieveComplete += 1;
                    resBall.available += awardAchievement[i];
                    resourceFromBattle.energyBall = resBall.available;
                    newAchieveCompleted.Add(i);
                    faceContent.Add(new SocialContent("I just get achievement! Do you know some information in here? :D #GreenHeaven", "Social/Achievement/" + Convert.ToString(i) + "Done.png"));
                    twitterContent.Add(new SocialContent("I just get achievement! Do you know some information in here? :D #GreenHeaven", "Social/Achievement/" + Convert.ToString(i) + "Done.png"));
                    notif.Add(new AchievementNotif(mapAchieveTexture[i], awardAchievement[i]));
                }
                else if (achievemntResource[i].CompareResoure(resourceFromBattle) && !challenge.completed.Contains(i))
                {
                    resourceFromBattle.achieveComplete += 1;
                    resBall.available += awardAchievement[i];
                    resourceFromBattle.energyBall = resBall.available;
                    newAchieveCompleted.Add(i);
                    faceContent.Add(new SocialContent("I just get achievement! Do you know some information in here? :D #GreenHeaven", "Social/Achievement/" + Convert.ToString(i) + "Done.png"));
                    twitterContent.Add(new SocialContent("I just get achievement! Do you know some information in here? :D #GreenHeaven", "Social/Achievement/" + Convert.ToString(i) + "Done.png"));
                    notif.Add(new AchievementNotif(mapAchieveTexture[i], awardAchievement[i]));
                }
			}

            if (newAchieveCompleted.Count != 0)
            {
                Challenge newComplete = new Challenge();
                newComplete.completed = newAchieveCompleted.ToArray();
                new Data().Save(newComplete, "Challenge/ChallengeCompleted.xml");
            }

            new Data().Save(resBall, "Resource/UnifiedRes.xml");


		}
        private int calculateCyclebotThridForm()
        {
            int count = 0;
            Dictionary<string, int> dictEvolutionInfo = new Dictionary<string, int>();



            Dictionary<int, string> numberAndName = new Dictionary<int, string>();
            numberAndName.Add(1,"NumberOne");
            numberAndName.Add(2,"NumberTwo");
            numberAndName.Add(3,"NumberThree");
            numberAndName.Add(4,"NumberFour");
            numberAndName.Add(5,"NumberFive");

			dictEvolutionInfo.Add("NumberOne",12);
			dictEvolutionInfo.Add("NumberTwo", 15);
			dictEvolutionInfo.Add("NumberThree", 18);
			dictEvolutionInfo.Add("NumberFour", 15);
			dictEvolutionInfo.Add("NumberFive", 20);

            for (int i = 0; i < roboAvail.available.Length; i++)
            {
                string name = numberAndName[roboAvail.available[i]];
                if (mapPlayer[name].level >= dictEvolutionInfo[name])
                {
                    count += 1;
                }

            }
            return count;

        }
		private int calculateBall(){
            return MiniGameScene.ballCollected;    
		}
		private int calculateCycleBotUnlock(){
			int nCyclebot = roboAvail.available.Length;
			if (roboAvail.available.Contains(dataLevel.roboUnlock) || dataLevel.roboUnlock == 0)
			{
				return nCyclebot;
			}
			return nCyclebot + 1;
		}
		private void EvaluateAssNotif()
		{
			Dictionary<string, Texture2D> mapAssTexture = new Dictionary<string, Texture2D>();

			mapAssTexture.Add("defentant",Asset.defentantBuy);
			mapAssTexture.Add("gigadroid",Asset.gigadroidBuy);
			mapAssTexture.Add("steelKnight",Asset.knightBuy);
			mapAssTexture.Add("megadroid",Asset.megadroidBuy);
			mapAssTexture.Add("thunderbolt1",Asset.thunderBuy);
			mapAssTexture.Add("thunderbolt2",Asset.thunderbolt2Buy);
			mapAssTexture.Add("engine1",Asset.engine);
			mapAssTexture.Add("healer1",Asset.healer1);
			mapAssTexture.Add("healer2",Asset.healer2);

			List<AssistantStatus> assData = new Data().Load<List<AssistantStatus>>("Assistant/AssistantStatus.xml");
			for (int i = 0; i < assData.Count; i++)
			{
                if (assData[i].type.Equals(dataLevel.assistantUnlock) && !assData[i].isUnlocked)
				{
					assData[i].isUnlocked = true;
                    faceContent.Add(new SocialContent("I just unlock a new assistant! Amazing... :D #GreenHeaven", "Social/Ass/" + assData[i].type + ".png"));
                    twitterContent.Add(new SocialContent("I just unlock a new assistant! Amazing... :D #GreenHeaven", "Social/Ass/" + assData[i].type + ".png"));

					notif.Add(new AssNotif(mapAssTexture[assData[i].type]));

				}
			}
			new Data().Save(assData, "Assistant/AssistantStatus.xml");

		}
		private void EvaluateNewCyclebotNotif()
		{
			Dictionary<int, Texture2D> mapCyclebot = new Dictionary<int, Texture2D>();
			mapCyclebot.Add(2,Asset.ray1);
			mapCyclebot.Add(3,Asset.crusher1);
			mapCyclebot.Add(4,Asset.soul1);
			mapCyclebot.Add(5,Asset.george1);
			Dictionary<int, string> mapNameCyclebot = new Dictionary<int, string>();
			mapNameCyclebot.Add(2, "NumberTwo1");
			mapNameCyclebot.Add(3, "NumberThree1");
			mapNameCyclebot.Add(4, "NumberFour1");
			mapNameCyclebot.Add(5, "NumberFive1");
			
			//Just for test
			//LevelData dataLevel = game.Content.Load<LevelData>("Data/Map/Map" + 1 + "/Level" + 2);
			
			List<int> newCyclebotAvail = new List<int>();
			newCyclebotAvail.InsertRange(0, roboAvail.available);
			if (dataLevel.roboUnlock != 0 && !roboAvail.available.Contains(dataLevel.roboUnlock))
			{
				newCyclebotAvail.Add(dataLevel.roboUnlock);
				roboAvail.available = newCyclebotAvail.ToArray();

				faceContent.Add(new SocialContent("I just get a new Cyclebot #GreenHeaven", "Social/Player/" + mapNameCyclebot[dataLevel.roboUnlock] + ".png"));
                twitterContent.Add(new SocialContent("I just get a new Cyclebot #GreenHeaven", "Social/Player/" + mapNameCyclebot[dataLevel.roboUnlock] + ".png"));

				notif.Add(new CyclebotNotif(mapCyclebot[dataLevel.roboUnlock]));
                new Data().Save(roboAvail, "Player/RoboAvailable.xml");
			}

		}
		private void EvaluateNewFormCyclebotNotif(){
            Player[] players = player;
			//Just for Test;
            //Player[] players = new Player[4];
            //players[0] = new NumberFive(game.Content, Vector2.Zero, null, game);
            //players[1] = new NumberTwo(game.Content, Vector2.Zero, null, game);
            //players[2] = new NumberThree(game.Content, Vector2.Zero, null, game);
            //players[3] = new NumberFour(game.Content, Vector2.Zero, null, game);


			Exp exp = new Exp(players);
			exp.Calcualate();

			Dictionary<string, PlayerAtt> playerInfo = Load(players);
			Dictionary<string,PlayerAtt> playerInfoNext = exp.dictPlayer;
			

			Dictionary<string, int[]> dictEvolutionInfo = new Dictionary<string, int[]>();

			//Level to up form;
			int[] numberOne = { 5, 9, 12 };
			int[] numberTwo = { 5, 10, 15};
			int[] numberThree = { 7, 12, 18 };
			int[] numberFour = { 7, 10, 15 };
			int[] numberFive = { 10, 15, 20 };

			dictEvolutionInfo.Add("NumberOne",numberOne);
			dictEvolutionInfo.Add("NumberTwo", numberTwo);
			dictEvolutionInfo.Add("NumberThree", numberThree);
			dictEvolutionInfo.Add("NumberFour", numberFour);
			dictEvolutionInfo.Add("NumberFive", numberFive);

			Dictionary<string, Texture2D[]> mapEvolutionTexture = new Dictionary<string,Texture2D[]>();
			//Wait for Demo Done
            Texture2D[] tN1 = { Asset.demo1, Asset.demo2, Asset.demo3 };
			Texture2D[] tN2 = {Asset.ray1,Asset.ray2,Asset.ray3};
			Texture2D[] tN3 = {Asset.crusher1,Asset.crusher2,Asset.crusher3};
			Texture2D[] tN4 = {Asset.soul1,Asset.soul2,Asset.soul3};
			Texture2D[] tN5 = {Asset.george1,Asset.george2,Asset.george3};

			mapEvolutionTexture.Add("NumberOne", tN1);
			mapEvolutionTexture.Add("NumberTwo", tN2);
			mapEvolutionTexture.Add("NumberThree", tN3);
			mapEvolutionTexture.Add("NumberFour", tN4);
			mapEvolutionTexture.Add("NumberFive", tN5);

			foreach (string playerName in playerInfo.Keys)
			{
				for (int i = 0; i < dictEvolutionInfo[playerName].Length; i++)
				{
					if (dictEvolutionInfo[playerName][i] <= playerInfoNext[playerName].level && dictEvolutionInfo[playerName][i] > playerInfo[playerName].level)
					{
                        faceContent.Add(new SocialContent("I just make a new evolution! It's Awesome #GreenHeaven", "Social/Player/" + playerName + Convert.ToString(i + 1) + ".png"));
                        twitterContent.Add(new SocialContent("I just make a new evolution! It's Awesome #GreenHeaven", "Social/Player/" + playerName + Convert.ToString(i + 1) + ".png"));

						notif.Add(new CyclebotNewFormNotif(mapEvolutionTexture[playerName][i], mapEvolutionTexture[playerName][i-1]));
					}
				}	
			}
		}
		private Dictionary<string, PlayerAtt> Load(Player[] players)
		{
			
			Dictionary<string, PlayerAtt> dictPlayers = new Dictionary<string, PlayerAtt>();


			foreach (Player player in players)
			{
				//Console.WriteLine(player.GetType().Name.Replace("Upgrade", String.Empty));
				if (player.isAlive)
					dictPlayers.Add(player.GetType().Name.Replace("Upgrade", String.Empty), mapPlayer[player.GetType().Name.Replace("Upgrade", String.Empty)]);
			}
			return dictPlayers;
		}
		private void nextButton_Clicked(object o, EventArgs e)
		{
			if (notif.Count == 0 || next + 1 == notif.Count)
			{
                bool[] introData = new Data().Load<bool[]>("Introduction/intro.xml");
                if (GlobalClass.curLevelPlaying == 7 && GlobalClass.curMapPlaying == 1 && !introData[1])
                {
                    GlobalClass.curMapPlaying = 2;
                    GlobalClass.curLevelPlaying = 1;
                    introData[1] = true;
                    new Data().Save(introData, "Introduction/intro.xml");
                    
                    Level level = new Level();
                    level.isUnlock = true;
                    level.rating = 0;
                    new Data().Save(level, "Map/Map_2/Level1.xml");
                    game.StartIntroScene();
                }
				else
                    game.StartMap();
			}
			faceButton.texture = Asset.facebookButton;
			isTextbox = false;
			isTwitterClicked = false;
			isFaceClicked = false;
			isNotif = true;
			next++;
		}
		public void Initialize()
		{
            dataLevel = game.Content.Load<LevelData>("Data/Map/Map" + GlobalClass.curMapPlaying + "/Level" + GlobalClass.curLevelPlaying);
            roboAvail = new Data().Load<RoboAvailable>("Player/RoboAvailable.xml");
            tweetBox = new TweetBox(game);
			newspaper = new Background(Asset.victoryBg, 0.0095f);
			newspaper.Position = new Vector2(0, -GlobalClass.ScreenHeight);
			circle = new CircleObject(Asset.circle, new Vector2(GlobalClass.ScreenWidth, GlobalClass.ScreenHeight), new Vector2(Asset.circle.Width / 2, Asset.circle.Height / 2), true);
			circle.layer = 0.0032f;
			nextButton = new Button(Asset.nextButton, Asset.nextButtonHover, Asset.nextButton, 0.625f, 0f, new Vector2(GlobalClass.ScreenWidth - Asset.nextButton.Width * 0.625f / 2, GlobalClass.ScreenHeight - 50));
			facePos = new Vector2(GlobalClass.ScreenWidth + Asset.facebookButton.Width * 0.625f / 2, GlobalClass.ScreenHeight - 125);
			tweetPos = new Vector2(GlobalClass.ScreenWidth +  Asset.twitterButton.Width * 0.625f / 2, GlobalClass.ScreenHeight - 200);
			targetFacePos = new Vector2(GlobalClass.ScreenWidth - Asset.facebookButton.Width * 0.625f / 2, GlobalClass.ScreenHeight - 125);
			targetTweetPos = new Vector2(GlobalClass.ScreenWidth - Asset.twitterButton.Width * 0.625f / 2, GlobalClass.ScreenHeight - 200);
			faceButton = new Button(Asset.facebookButton, Asset.facebookButton_Hover, Asset.facebookButton, 0.625f, 0f, facePos);
			tweetButton = new Button(Asset.twitterButton, Asset.twitterButton_Hover, Asset.twitterButton, 0.625f, 0f,tweetPos );
            nextButton.layer = 0.003f;
            faceButton.layer = 0.003f;
            tweetButton.layer = 0.003f;
            Data loadData = new Data();
            mapPlayer = new Dictionary<string, dynamic>();

            mapPlayer.Add("NumberOne", loadData.Load<NumberOneUpgrade>("Player\\NumberOne.xml"));
            mapPlayer.Add("NumberTwo", loadData.Load<NumberTwoUpgrade>("Player\\NumberTwo.xml"));
            mapPlayer.Add("NumberThree", loadData.Load<NumberThreeUpgrade>("Player\\NumberThree.xml"));
            mapPlayer.Add("NumberFour", loadData.Load<NumberFourUpgrade>("Player\\NumberFour.xml"));
            mapPlayer.Add("NumberFive", loadData.Load<NumberFiveUpgrade>("Player\\NumberFive.xml"));
        }

		public void Update(GameTime gameTime)
		{
			if (isNotif)
			{
				Vector2 directionTweet = targetTweetPos - tweetButton.position;
				Vector2 directionFace = targetFacePos - faceButton.position;

				directionTweet.Normalize();
				directionFace.Normalize();
				if (tweetButton.position.X > targetTweetPos.X + 6)
				{
					tweetButton.position += directionTweet * (float)gameTime.ElapsedGameTime.TotalMilliseconds * 0.35f;
				}
				if (faceButton.position.X > targetFacePos.X + 6)
				{
					faceButton.position += directionFace * (float)gameTime.ElapsedGameTime.TotalMilliseconds * 0.35f;
				}
				notif[next].Update(gameTime);
			}
			nextButton.Update(gameTime);
			if (!isFaceClicked)
				faceButton.Update(gameTime);
			if(!isTwitterClicked && !isTextbox)
				tweetButton.Update(gameTime);
			circle.update(gameTime);
			Vector2 direction = new Vector2(0, 0) - newspaper.Position;
			direction.Normalize();
            if (newspaper.Position.Y < 0)
            {
                newspaper.Position += direction * (float)gameTime.ElapsedGameTime.TotalMilliseconds * 0.5f;
            }
            else if (newspaper.Position.Y > 0)
                newspaper.Position.Y = 0;

            foreach (Star start in listStar)
            {
                start.Update(gameTime);
            }
            calCyclebotExp.Update(gameTime);
            funFactNotif.Update(gameTime);
            if (isTextbox)
            {
                isTwitterClicked = false;
                tweetBox.Update(gameTime);
                if (tweetBox.isEnter)
                {
                    isTextbox = false;
                }
            }
		}
		public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
		{
			if (isNotif)
			{
				faceButton.Draw(spriteBatch);
				tweetButton.Draw(spriteBatch);
				notif[next].Draw(gameTime, spriteBatch);
			}
            nextButton.Draw(spriteBatch);
			if (!isNotif)
				spriteBatch.Draw(Asset.quangsang, new Vector2(GlobalClass.ScreenWidth - Asset.quangsang.Width * 0.625f + 80, GlobalClass.ScreenHeight - Asset.quangsang.Height * 0.625f + 50), null, Color.White, 0f, Vector2.Zero, 0.625f, SpriteEffects.None, 0.0033f);

			
            foreach (Star start in listStar)
            {
                start.Draw(gameTime, spriteBatch);
                
            }
			circle.draw(spriteBatch);
			newspaper.Draw(spriteBatch);
            calCyclebotExp.Draw(gameTime, spriteBatch);
            funFactNotif.Draw(spriteBatch);
            if (isTextbox)
            {
                tweetBox.Draw(spriteBatch, gameTime);
            }
		}
	}
}
