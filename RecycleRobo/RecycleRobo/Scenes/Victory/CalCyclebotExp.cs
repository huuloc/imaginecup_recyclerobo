﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace RecycleRobo.Scenes.Victory
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class CalCyclebotExp
    {
        GameManager game;
        List<CyclebotExp> listCyclebot;
        public CalCyclebotExp(GameManager game)
        {
            this.game = game;
            listCyclebot = new List<CyclebotExp>();
            Calcualate();
        }
        public void Update(GameTime gameTime)
        {
            foreach (CyclebotExp cyclebot in listCyclebot)
            {
                cyclebot.Update(gameTime);
            }
        }
        public void Calcualate()
        {
            //Player[] players = new Player[3];
            //players[0] = new NumberFive(game.Content, Vector2.Zero, null, game);
            //players[1] = new NumberTwo(game.Content, Vector2.Zero, null, game);
            //players[2] = new NumberThree(game.Content, Vector2.Zero, null, game);
            Player[] players = VictoryScene.player;
            Exp exp = new Exp(players);
            exp.Calcualate();

            Dictionary<string, PlayerAtt> playerInfo = Load(players);
            Dictionary<string, PlayerAtt> playerInfoNext = exp.dictPlayer;


            Dictionary<string, int[]> dictEvolutionInfo = new Dictionary<string, int[]>();

            //Level to up form;
            int[] numberOne = { 5, 9, 12 };
            int[] numberTwo = { 5, 10, 15 };
            int[] numberThree = { 7, 12, 18 };
            int[] numberFour = { 7, 10, 15 };
            int[] numberFive = { 10, 15, 20 };

            dictEvolutionInfo.Add("NumberOne", numberOne);
            dictEvolutionInfo.Add("NumberTwo", numberTwo);
            dictEvolutionInfo.Add("NumberThree", numberThree);
            dictEvolutionInfo.Add("NumberFour", numberFour);
            dictEvolutionInfo.Add("NumberFive", numberFive);

            Dictionary<string, Texture2D[]> mapEvolutionTexture = new Dictionary<string, Texture2D[]>();
            //Wait for Demo Done
            Texture2D[] tN1 = { Asset.barDemo_1, Asset.barDemo_2, Asset.barDemo_3 };
            Texture2D[] tN2 = { Asset.barRay_1, Asset.barRay_2, Asset.barRay_3 };
            Texture2D[] tN3 = { Asset.barCrusher_1, Asset.barCrusher_2, Asset.barCrusher_3 };
            Texture2D[] tN4 = { Asset.barSoul_1, Asset.barSoul_2, Asset.barSoul_3 };
            Texture2D[] tN5 = { Asset.barGeorge_1, Asset.barGeorge_2, Asset.barGeorge_3};


            int margin = 20;
            int basePos = 30;
            int nextPos = Convert.ToInt16(Asset.barDemo_1.Height * 0.625f) + margin;

            mapEvolutionTexture.Add("NumberOne", tN1);
            mapEvolutionTexture.Add("NumberTwo", tN2);
            mapEvolutionTexture.Add("NumberThree", tN3);
            mapEvolutionTexture.Add("NumberFour", tN4);
            mapEvolutionTexture.Add("NumberFive", tN5);

            Dictionary<string, int> formPlayer = new Dictionary<string, int>();
            formPlayer.Add("NumberOne", 0);
            formPlayer.Add("NumberTwo", 0);
            formPlayer.Add("NumberThree", 0);
            formPlayer.Add("NumberFour", 0);
            formPlayer.Add("NumberFive", 0);

            foreach (string playerName in playerInfo.Keys)
            {
                for (int i = 0; i < dictEvolutionInfo[playerName].Length; i++)
                {
                    if (playerInfo[playerName].level >= dictEvolutionInfo[playerName][i])
                    {
                        formPlayer[playerName] = i;
                    }
                }
                //Console.WriteLine(playerName + ":" + formPlayer[playerName]);
            }

            Dictionary<string, int> expWidth = new Dictionary<string, int>();
            expWidth.Add("NumberOne", 0);
            expWidth.Add("NumberTwo", 0);
            expWidth.Add("NumberThree", 0);
            expWidth.Add("NumberFour", 0);
            expWidth.Add("NumberFive", 0);

            Dictionary<string, int[]> expLevel = new Dictionary<string, int[]>();
            expLevel.Add("NumberOne", Exp.numberOne);
            expLevel.Add("NumberTwo", Exp.numberTwo);
            expLevel.Add("NumberThree", Exp.numberThree);
            expLevel.Add("NumberFour", Exp.numberFour);
            expLevel.Add("NumberFive", Exp.numberFive);

            foreach (string playerName in playerInfo.Keys)
            {
                //if (playerInfoNext[playerName].level > playerInfo[playerName].level)
                //{
                //    Console.WriteLine(playerName);
                //    Console.WriteLine("NextEXP" + playerInfoNext[playerName].exp);
                //    Console.WriteLine("Level" + playerInfoNext[playerName].level);
                //    Console.WriteLine("expLevel" + expLevel[playerName][playerInfoNext[playerName].level - 1]);
                //}
                //Console.WriteLine(playerName);
                //Console.WriteLine("OldEXP: " + playerInfo[playerName].exp);
                //Console.WriteLine("NextEXP: " + playerInfoNext[playerName].exp);
                //Console.WriteLine("NextLevel: " + playerInfoNext[playerName].level);
                //Console.WriteLine("expLevel: " + expLevel[playerName][playerInfoNext[playerName].level - 1]);
                if (playerInfoNext[playerName].exp > expLevel[playerName][19])
                {
                    expWidth[playerName] = 0;
                }
                else
                    expWidth[playerName] = playerInfoNext[playerName].exp;
                //Console.WriteLine("expWidth: " + expWidth[playerName]);

            }

            int next = 0;
            foreach (string playerName in playerInfo.Keys)
            {
                int form = formPlayer[playerName];
                int expLevelNext = 0;
                if (playerInfoNext[playerName].level < 20)
                {
                    expLevelNext = expLevel[playerName][playerInfoNext[playerName].level];
                    //expLevelNext = expLevel[playerName][playerInfoNext[playerName].level] - expLevel[playerName][playerInfoNext[playerName].level - 1];
                    //Console.WriteLine("expLevelNext: " + expLevelNext);
                }
                listCyclebot.Add(new CyclebotExp(game, mapEvolutionTexture[playerName][form], new Vector2(630, basePos + nextPos * next++),
                                                    expWidth[playerName], expLevelNext, playerInfoNext[playerName].level));
            }
        }

        private Dictionary<string, PlayerAtt> Load(Player[] players)
        {
            Data loadData = new Data();
            Dictionary<string, PlayerAtt> dictPlayers = new Dictionary<string, PlayerAtt>();
            Dictionary<string, dynamic> mapPlayer = new Dictionary<string, dynamic>();

            mapPlayer.Add("NumberOne", loadData.Load<NumberOneUpgrade>("Player\\NumberOne.xml"));
            mapPlayer.Add("NumberTwo", loadData.Load<NumberTwoUpgrade>("Player\\NumberTwo.xml"));
            mapPlayer.Add("NumberThree", loadData.Load<NumberThreeUpgrade>("Player\\NumberThree.xml"));
            mapPlayer.Add("NumberFour", loadData.Load<NumberFourUpgrade>("Player\\NumberFour.xml"));
            mapPlayer.Add("NumberFive", loadData.Load<NumberFiveUpgrade>("Player\\NumberFive.xml"));

            foreach (Player player in players)
            {
                if (player.isAlive)
                    dictPlayers.Add(player.GetType().Name.Replace("Upgrade", String.Empty), mapPlayer[player.GetType().Name.Replace("Upgrade", String.Empty)]);
            }
            return dictPlayers;
        }
        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            foreach (CyclebotExp cyclebot in listCyclebot)
            {
                cyclebot.Draw(gameTime, spriteBatch);
            }
        }
    }
}
