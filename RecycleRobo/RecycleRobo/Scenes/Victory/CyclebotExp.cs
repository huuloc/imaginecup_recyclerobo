﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace RecycleRobo.Scenes.Victory
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class CyclebotExp
    {
        GameManager game;
        Texture2D numTexture;
        Texture2D expBarTexture;
        public float scale = 0.625f;
        public float layer = 0.0045f;
        public float speed = 0.25f;
        Vector2 pos;
        Vector2 target = new Vector2(300, 50);
        Vector2 expPos;
        Vector2 expTarGetPos;
        Vector2 basePos = new Vector2(110, 39);
        int expWidthInRect;
        int level;
        SpriteFont font;
        public CyclebotExp(GameManager game, Texture2D numTexture,Vector2 target, int expWidth, int expLevelNext, int level)
        {
            this.game = game;
            this.pos = new Vector2(target.X,-100);
            this.expPos = pos + basePos;
            this.expTarGetPos = target + basePos;
            this.target = target;
            this.numTexture = numTexture;
            this.level = level;
            expBarTexture = game.Content.Load<Texture2D>("Victory/victory/barExp");
            if (expWidth != 0 && expLevelNext != 0)
            {
                float unitExp = expBarTexture.Width * scale / expLevelNext;
                expWidthInRect = Convert.ToInt16(unitExp * expWidth);
            }
            else
                expWidthInRect = 0;

            font = game.Content.Load<SpriteFont>("Victory/font");
        }
        public void Update(GameTime gameTime)
        {
            Vector2 direction = target - pos;
            direction.Normalize();
            if (pos.Y < target.Y)
            {
                pos += direction * (float)gameTime.ElapsedGameTime.TotalMilliseconds * speed;
                expPos += direction * (float)gameTime.ElapsedGameTime.TotalMilliseconds * speed;
            }
        }
        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(numTexture, pos, null, Color.White, 0f, Vector2.Zero, scale, SpriteEffects.None, layer);
            spriteBatch.Draw(expBarTexture, new Rectangle((int)expPos.X, (int)expPos.Y, expWidthInRect, Convert.ToInt16(expBarTexture.Height * scale)), null, Color.White, 0f, Vector2.Zero, SpriteEffects.None, 0.004f);
            //spriteBatch.DrawString(font, "lvl." + Convert.ToString(level), expPos + new Vector2(10, 10), Color.DimGray);
            if(level < 20)
                spriteBatch.DrawString(font, "lvl." + Convert.ToString(level), expPos + new Vector2(20, 3), Color.DimGray, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.0039f);
            else
                spriteBatch.DrawString(font, "Wow Max lvl", expPos + new Vector2(20, 3), Color.DimGray, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.0039f);


        }
    }
}
