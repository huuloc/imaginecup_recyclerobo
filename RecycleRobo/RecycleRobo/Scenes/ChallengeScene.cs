using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using LoadData;
using System.Collections;

namespace RecycleRobo.Scenes
{
    public class ChallengeScene
    {
        
        private Button backButton;
        private List<Texture2D> AllItem;

        private static float scale = 0.625f;
        public Vector2[] Pos;
        private MouseState cur;
        private MouseState pre;

        private GameManager game;

        public static int[] addResource = {20,30, 30, 50, 50, 100, 250, 90, 500, 500, 150, 100, 500};
        public int[] completed;

        public ChallengeScene(GameManager game) 
        {
            this.game = game;
            loadChallenge();
        }

        public void loadChallenge()
        {
            Data loadData = new Data();
            Challenge challenge = loadData.Load<Challenge>("Challenge/ChallengeCompleted.xml");
            if (challenge.completed != null)
                completed = challenge.completed;
            else
                completed = new int[0];
            AllItem = new List<Texture2D>();
            AllItem.Add(Asset.challenge1);
            AllItem.Add(Asset.challenge2);
            AllItem.Add(Asset.challenge3);
            AllItem.Add(Asset.challenge4);
            AllItem.Add(Asset.challenge5);
            AllItem.Add(Asset.challenge6);
            AllItem.Add(Asset.challenge7);
            AllItem.Add(Asset.challenge8);
            AllItem.Add(Asset.challenge9);
            AllItem.Add(Asset.challenge10);
            AllItem.Add(Asset.challenge11);
            AllItem.Add(Asset.challenge12);
            AllItem.Add(Asset.challenge13);

            List<Texture2D> completedTexture = new List<Texture2D>();
            completedTexture.Add(Asset.challenge1completed);
            completedTexture.Add(Asset.challenge2completed);
            completedTexture.Add(Asset.challenge3completed);
            completedTexture.Add(Asset.challenge4completed);
            completedTexture.Add(Asset.challenge5completed);
            completedTexture.Add(Asset.challenge6completed);
            completedTexture.Add(Asset.challenge7completed);
            completedTexture.Add(Asset.challenge8completed);
            completedTexture.Add(Asset.challenge9completed);
            completedTexture.Add(Asset.challenge10completed);
            completedTexture.Add(Asset.challenge11completed);
            completedTexture.Add(Asset.challenge12completed);
            completedTexture.Add(Asset.challenge13completed);

            Pos = new Vector2[AllItem.Count];
            Pos[0] = new Vector2(20,100);
            for (int i = 0; i < Pos.Length; i++) 
            {
                Pos[i].Y = 100;
            }
            for (int i = 1; i < Pos.Count(); i++)
            {
                Pos[i].X = Pos[i - 1].X +596 * scale + 20;
            }

            for (int i = 0; i < AllItem.Count; i++) 
            {
                if (completed.Contains(i+1)) 
                {
                    AllItem[i] = completedTexture[i];
                }
            }

            Vector2 backButtonPos;
            backButtonPos = new Vector2(GlobalClass.ScreenWidth - Asset.ChallengeBacknormal.Width * scale / 2, Asset.ChallengeBacknormal.Height * scale / 2);
            backButton = new Button(Asset.ChallengeBacknormal, Asset.ChallengeBackHover, Asset.ChallengeBacknormal, scale, 0.0f, backButtonPos);
            backButton.Clicked += new EventHandler(backButton_Clicked);
        }

        public void backButton_Clicked(object o, EventArgs e)
        {
            //Console.WriteLine("click");
            if (!GlobalClass.isInMap)
                game.StartMenuScene();
            else
                game.StartMap();
            
        }

        public void Update(GameTime gameTime) 
        {
            cur = Mouse.GetState();
            if (cur.LeftButton == ButtonState.Pressed && pre.LeftButton == ButtonState.Pressed)
            {
                Vector2 Target = new Vector2((float)MouseHelper.MousePosition(cur).X, (float)MouseHelper.MousePosition(cur).Y);
                Vector2 Position = new Vector2((float)MouseHelper.MousePosition(pre).X, (float)MouseHelper.MousePosition(pre).Y);
                Vector2 direction = Target - Position;

                if (direction.Length() > 40)
                {
                    direction.Normalize();
                    direction = direction * 40;
                }
                for (int i = 0; i < Pos.Count(); i++) 
                {
                    Pos[i].X += direction.X;
                }
                if (Pos[0].X > 20 && direction.X > 0) 
                {
                    for (int i = 0; i < Pos.Count(); i++)
                    {
                        Pos[i].X -= direction.X;
                    }
                }
                else if (Pos[AllItem.Count - 1].X + 596 * scale < GlobalClass.ScreenWidth - 20 && direction.X < 0) 
                {
                    for (int i = 0; i < Pos.Count(); i++)
                    {
                        Pos[i].X -= direction.X;
                    }
                }
                  
            }
            backButton.Update(gameTime);
            pre = cur;
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch) 
        {
            spriteBatch.Draw(Asset.challengeBackground, Vector2.Zero, null, Color.White, 0.0f, Vector2.Zero, 0.625f, SpriteEffects.None, 0.98f);
            spriteBatch.End();
            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend,
                      null, null, null, null, Resolution.getTransformationMatrix()); 
            
            for (int i = 0; i < AllItem.Count; i++) 
            {
                spriteBatch.Draw(AllItem[i], new Vector2(Pos[i].X, Pos[i].Y), new Rectangle(0,0,596,765), Color.White, 0.0f, Vector2.Zero, scale, SpriteEffects.None, 0.5f);
            }
            spriteBatch.Draw(Asset.challengeTaskBar, Vector2.Zero, null, Color.White, 0.0f, Vector2.Zero, scale, SpriteEffects.None, 0.5f);
            backButton.Draw(spriteBatch);
        }
    }
}
