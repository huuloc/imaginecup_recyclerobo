using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace RecycleRobo.Scenes
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class HowToPlay
    {
        GameManager game;
        List<Texture2D> howtoTexture;
        Vector2[] Pos;
        public Rectangle clipRect, originRect;
        Rectangle[] BoundingBox;
        public float scale = 0.4f;
        Texture2D fadeTexture;
        MouseState cur, pre;
        int boxSelect = 0;
        public Texture2D texture;
        private static RasterizerState _rasterizerState = new RasterizerState() { ScissorTestEnable = true };


        public HowToPlay(GameManager game)
        {
            this.game = game;

            Initialize();

        }
        public void Initialize()
        {
            fadeTexture = game.Content.Load<Texture2D>("HowToPlay\\fade");

            texture = game.Content.Load<Texture2D>("HowToPlay\\tutorialBar");
            howtoTexture = new List<Texture2D>();
            howtoTexture.Add(game.Content.Load<Texture2D>("HowToPlay\\buildTeam"));
            howtoTexture.Add(game.Content.Load<Texture2D>("HowToPlay\\Minigame"));
            howtoTexture.Add(game.Content.Load<Texture2D>("HowToPlay\\gamePlay"));
            howtoTexture.Add(game.Content.Load<Texture2D>("HowToPlay\\skill"));
            howtoTexture.Add(game.Content.Load<Texture2D>("HowToPlay\\Factory"));
            howtoTexture.Add(game.Content.Load<Texture2D>("HowToPlay\\Factory2"));
            howtoTexture.Add(game.Content.Load<Texture2D>("HowToPlay\\challenge"));
            if (GlobalClass.fullWidthDisplay != 1366)
            {
                clipRect = RotatedRectangle.CalculateBoundingRectangle(new Rectangle(86, 170, 560, 286), GameManager.transform);
                originRect = clipRect;
            }
            else
            {
                clipRect = new Rectangle(86, 170, 560, 286);
                originRect = clipRect;

            }
            if (GlobalClass.isFullScreen)
            {
                clipRect = RotatedRectangle.CalculateBoundingRectangle(originRect, GameManager.transform);
            }

            Pos = new Vector2[howtoTexture.Count];
            BoundingBox = new Rectangle[howtoTexture.Count];
            Pos[0] = new Vector2(clipRect.X, clipRect.Y);
            for (int i = 0; i < howtoTexture.Count; i++ )
            {
                if (i >= 1) Pos[i] = new Vector2(Pos[i - 1].X + howtoTexture[i].Width * scale + 10, clipRect.Y);
            }
            for (int i = 0; i < howtoTexture.Count; i++)
            {

                Pos[i] = new Vector2((Pos[i]).X + 10, (Pos[i]).Y);
                BoundingBox[i] = new Rectangle((int)Pos[i].X, (int)clipRect.Y,
                (int)(howtoTexture[i].Width * scale),
                (int)(howtoTexture[i].Height * scale));
            }
        }
        public void Update(GameTime gametime)
        {
            cur = Mouse.GetState();
            for (int i = 0; i < howtoTexture.Count; i++)
            {
                if ((BoundingBox[i]).Contains(MouseHelper.MousePosition(cur)))
                {
                    boxSelect = i;
                }
            }
            if (cur.LeftButton == ButtonState.Pressed && clipRect.Contains(MouseHelper.MousePosition(cur)))
            {
                Vector2 Target = new Vector2(MouseHelper.MousePosition(cur).X, MouseHelper.MousePosition(cur).Y);
                Vector2 Position = new Vector2(MouseHelper.MousePosition(pre).X, MouseHelper.MousePosition(pre).Y);
                Vector2 direction = Target - Position;

                if (direction.Length() > 60)
                {
                    direction.Normalize();
                    direction = direction * 60;
                }
                if (Pos[0].X > clipRect.X + 10 && direction.X > 0) return;
                if (Pos[Pos.Count() - 1].X + howtoTexture[0].Width*scale < GlobalClass.ScreenWidth
                    && direction.X < 0) return;

                for (int i = 0; i < howtoTexture.Count; i++)
                {

                    Pos[i] = new Vector2((Pos[i]).X + (direction.X), (Pos[i]).Y);
                    BoundingBox[i] = new Rectangle((int)Pos[i].X, (int)clipRect.Y,
                    (int)(howtoTexture[i].Width * scale),
                    (int)(howtoTexture[i].Height * scale));
                    //BoundingBox[i] = new Rectangle((int)Pos[i].X, (int)clipRect.Y,
                    //(int)(GlobalClass.ScreenWidth),
                    //(int)(GlobalClass.ScreenHeight));
                }

            }
            pre = cur;
        }
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, new Vector2(50, 100), null, Color.White, 0f, Vector2.Zero, 0.625f, SpriteEffects.None, 0.06f);
            spriteBatch.DrawString(game.Content.Load<SpriteFont>("HowToPlay\\font"), Convert.ToString(boxSelect+1) + "/" + Convert.ToString(howtoTexture.Count - 1), new Vector2(100, 135), Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.059f);
            spriteBatch.End();
            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend,
                                null, null, _rasterizerState, null,GameManager.transform);
            spriteBatch.GraphicsDevice.ScissorRectangle = clipRect;

            for (int i = 0; i < howtoTexture.Count; i++)
            {
                    spriteBatch.Draw((howtoTexture[i])
                        , (Rectangle)BoundingBox[i]
                        , null, Color.White, 0.0f, new Vector2(0, 0), SpriteEffects.None, 0.059f);
            }
            spriteBatch.End();
            spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, null, null, null, null, Resolution.getTransformationMatrix());
        }

    }
}