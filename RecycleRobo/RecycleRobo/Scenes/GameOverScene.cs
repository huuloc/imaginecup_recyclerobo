﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using RecycleRobo.Scenes;
using RecycleRobo.Scenes.Victory;
using RecycleRobo.Scenes.Victory.RecycleRobo.Scenes.Victory;
using LoadData;
using System.Threading;

namespace RecycleRobo
{
    public class GameOverScene
    {
        GameManager game;

        CircleObject circle;
        Button nextButton;

        Background newspaper;
        FunFactNotif funFactNotif;

        public GameOverScene(GameManager game)
        {
            this.game = game;
            funFactNotif = new FunFactNotif(game);
            funFactNotif.layer = 0.004f;
            Initialize();
            nextButton.Clicked += new EventHandler(nextButton_Clicked);

            MediaPlayer.Volume = GlobalClass.volumeMusic;
            MediaPlayer.Play(game.Content.Load<Song>("sounds/lose"));
            MediaPlayer.IsRepeating = true;


        }
        public void nextButton_Clicked(object o, EventArgs e)
        {
            game.StartMap();
        }
        public void Initialize()
        {
            newspaper = new Background(Asset.loseBg, 0.005f);
            newspaper.Position = new Vector2(0, -GlobalClass.ScreenHeight);
            circle = new CircleObject(Asset.circle, new Vector2(GlobalClass.ScreenWidth, GlobalClass.ScreenHeight), new Vector2(Asset.circle.Width / 2, Asset.circle.Height / 2), true);
            circle.layer = 0.0032f;
            nextButton = new Button(Asset.nextButton, Asset.nextButtonHover, Asset.nextButton, 0.625f, 0f, new Vector2(GlobalClass.ScreenWidth - Asset.nextButton.Width * 0.625f / 2, GlobalClass.ScreenHeight - 50));
            nextButton.layer = 0.003f;
        }

        public void Update(GameTime gameTime)
        {
            nextButton.Update(gameTime);
            circle.update(gameTime);
            Vector2 direction = new Vector2(0, 0) - newspaper.Position;
            direction.Normalize();
            if (newspaper.Position.Y < 0)
            {
                newspaper.Position += direction * (float)gameTime.ElapsedGameTime.TotalMilliseconds * 0.5f;
            }
            else if (newspaper.Position.Y > 0)
                newspaper.Position.Y = 0;
            funFactNotif.Update(gameTime);

        }
        public void Draw(SpriteBatch spriteBatch)
        {
            newspaper.Draw(spriteBatch);
            spriteBatch.Draw(Asset.quangsang, new Vector2(GlobalClass.ScreenWidth - Asset.quangsang.Width * 0.625f + 80, GlobalClass.ScreenHeight - Asset.quangsang.Height * 0.625f + 50), null, Color.White, 0f, Vector2.Zero, 0.625f, SpriteEffects.None, 0.0035f);

            nextButton.Draw(spriteBatch);
            funFactNotif.Draw(spriteBatch);

            circle.draw(spriteBatch);
            newspaper.Draw(spriteBatch);

        }


    }
}
