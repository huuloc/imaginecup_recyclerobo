using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using RecycleRobo.Scenes;

namespace RecycleRobo.Scenes
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class MiniGameScene
    {
        MiniGame miniGame;
        GameManager game;
        Random rd = new Random();
        int timeNext = 0;
        public static int ballCollected = 0;
        public static float opacity = 0f;
        public MiniGameScene(GameManager game)
        {

            this.game = game;
            int[] mgOne = {1,2};
            int[] mgTwo = {5,7};
            int[] mgThree = {3,4,6,8};  
            if (mgOne.Contains(GlobalClass.curLevelPlaying))
            {
                miniGame = new DragNDrop(game);
            }
            else if (mgTwo.Contains(GlobalClass.curLevelPlaying))
            {
                miniGame = new Catcher(game);
            }
            else if (mgThree.Contains(GlobalClass.curLevelPlaying))
            {
                miniGame = new Circle(game);
            }
            MediaPlayer.Volume = (float)GlobalClass.volumeMusic/ 100;
            MediaPlayer.Play(game.Content.Load<Song>("Minigame/sound"));
            MediaPlayer.IsRepeating = true;


        }

        public void Update(GameTime gameTime)
        {

            opacity += 0.005f;
            if (opacity >= 1f)
            {
                miniGame.Play(gameTime);
            }
            if (miniGame.isWin())
                game.StartVictoryScene();
            ballCollected = miniGame.calculateEnergyBall();
        }
        public void Draw(SpriteBatch sprteBatch, GameTime gameTime)
        {
            miniGame.Draw(sprteBatch, gameTime);
        }
    }
}
