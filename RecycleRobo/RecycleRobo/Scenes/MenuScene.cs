﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using System.Dynamic;
using System.Threading;



namespace RecycleRobo.Scenes
{
    public class Button2
    {
        public String name;
        public Texture2D button;
        public Texture2D textImage;
        public Vector2 originPos, position;
        public Vector2 bottomPos;
        private float scale = 0.625f;
        private bool isShowTextImage = true;
        private float timeDelay = 0f;

        private MouseState curState, prevState;
        public bool clicked = false;
        private SoundEffect soundButton;
        private float delayToDown = 0;

        public Button2(String name, Texture2D button, Texture2D textImage, Vector2 position)
        {
            this.name = name;
            this.button = button;
            this.textImage = textImage;
            this.originPos = this.position = position;
            this.bottomPos = new Vector2(originPos.X, 580);
            soundButton = Asset.soundButton;
        }
        public bool isHover(Point mousePoint)
        {
            if (getRectangle().Contains(mousePoint)) return true;
            return false;
        }
        public Rectangle getRectangle()
        {
            return new Rectangle((int)(position.X - button.Width * scale / 2), (int)(position.Y - button.Height * scale / 2), (int)(button.Width * scale), (int)(button.Height * scale));
        }
        public void Draw(SpriteBatch spriteBatch)
        {
            if (isShowTextImage)
                spriteBatch.Draw(textImage, position + new Vector2(0, button.Height / 2 * scale + 20), null,
                    Color.White, 0f, new Vector2(textImage.Width / 2, textImage.Height / 2), scale, SpriteEffects.None, 0.82f);

            spriteBatch.Draw(button, position, null, Color.White, 0f, new Vector2(button.Width / 2, button.Height / 2),
                scale, SpriteEffects.None, 0.82f);
        }

        public void Update(GameTime gameTime)
        {
            curState = Mouse.GetState();
            Point mousePoint = new Point(MouseHelper.MousePosition(curState).X, MouseHelper.MousePosition(curState).Y);

            timeDelay += gameTime.ElapsedGameTime.Milliseconds;
            if (timeDelay > 700 && !isHover(mousePoint))
            {
                if (position.Y >= bottomPos.Y) position.Y = bottomPos.Y;
                else
                {
                    delayToDown += gameTime.ElapsedGameTime.Milliseconds;
                    if (delayToDown >= 400)
                    {
                        isShowTextImage = false;
                        position.Y += 3.5f;
                    }
                }
            }

            if (curState.LeftButton == ButtonState.Released)
            {
                clicked = false;
                if (isHover(mousePoint))
                {
                    delayToDown = 0;
                    if (position.Y <= originPos.Y)
                    {
                        position.Y = originPos.Y;
                        isShowTextImage = true;
                    }
                    else position.Y -= 3.5f;
                }
            }

            if (curState.LeftButton == ButtonState.Released && prevState.LeftButton == ButtonState.Pressed)
            {
                if (isHover(mousePoint))
                {
                    clicked = true;
                    soundButton.Play();
                }
            }

            prevState = curState;
        }
    }
    public class MenuScene
    {
        private GameManager game;
        public Button settingButton;
        public Setting settingPopup;

        //left side
        private Texture2D[] leftSide = new Texture2D[2];
        //right side
        private Texture2D[] rightSide = new Texture2D[3];
        private int i = 0, j = 1;

        public float timeDelayOpacity = 0f, opacity = 0f;
        public bool increaseOpacity = true;

        private List<Button2> buttons = new List<Button2>();

        private CircleObject circleObject;

        public static bool  isOptionClicked = false;

        public MenuScene(GameManager game)
        {
            this.game = game;
            GlobalClass.isInMap = false;
            //Test
            //Thread thread = new Thread(() => { FacebookUtility.Logout(); });
            //thread.SetApartmentState(ApartmentState.STA);
            //thread.Start();
            MediaPlayer.IsRepeating = true;
            MediaPlayer.Play(Asset.bgSound1);
            initButtons();
            
            circleObject = new CircleObject(Asset.menuCircle, new Vector2(GlobalClass.ScreenWidth / 2 - 20, 290), new Vector2(410, 475), false);
            leftSide[0] = Asset.ray; leftSide[1] = Asset.soul;
            rightSide[0] = Asset.demo; rightSide[1] = Asset.crusher; rightSide[2] = Asset.george;

            settingPopup = new Setting(game, new Vector2(820,156));
            //settingPopup.optionButton.position.X = GlobalClass.ScreenWidth;
            Texture2D optionButton = Asset.optionButtonMenu;
            Texture2D optionButtonHover = Asset.optionButtonHoverMenu;

            settingButton = new Button(optionButton, optionButtonHover, optionButton, 0.625f, 0.0f,
                 new Vector2(GlobalClass.ScreenWidth - optionButton.Width * 0.625f / 2 - 30, optionButton.Height * 0.625f / 2));
            settingButton.Clicked += new EventHandler(settingButton_Clicked);

        }
        public void settingButton_Clicked(object o, EventArgs e)
        {
            settingPopup.isResume = !settingPopup.isResume;
        }
        public void initButtons()
        {
            Button2 button = null;
            button = new Button2("shop", Asset.shopButton, Asset.shopText, new Vector2(GlobalClass.ScreenWidth / 2 - 400, 520));
            buttons.Add(button);
            button = new Button2("play", Asset.startButton, Asset.startText, new Vector2(GlobalClass.ScreenWidth / 2, 520));
            buttons.Add(button);
            button = new Button2("achievement", Asset.achievementButton, Asset.achievementText, new Vector2(GlobalClass.ScreenWidth / 2 + 400, 520));
            buttons.Add(button);
        }
        public void Update(GameTime gameTime)
        {
            //tweetBox.Update(gameTime);
            updateOpacity(gameTime);
            circleObject.update(gameTime);
            if (settingPopup.isResume)
            {
                foreach (Button2 button in buttons)
                {
                    button.Update(gameTime);
                    if (button.clicked)
                        pressButton(button);
                }
            }


            MediaPlayer.Volume = (float)GlobalClass.volumeMusic / 100;
            SoundEffect.MasterVolume = (float)GlobalClass.volumeSoundFx / 100;

            if (isOptionClicked)
            {
            }
            settingPopup.Update(gameTime);

            settingButton.Update(gameTime);
        }

        public void updateOpacity(GameTime gameTime)
        {
            timeDelayOpacity += gameTime.ElapsedGameTime.Milliseconds;
            if (timeDelayOpacity > 50)
            {
                timeDelayOpacity = 0;
                if (increaseOpacity) opacity += 0.04f;
                else opacity -= 0.04f;

                if (opacity >= 1)
                {
                    j++; if (j > 2) j = 0;
                    increaseOpacity = false;
                    timeDelayOpacity = -2500;
                }
                if (opacity <= 0)
                {
                    i++; if (i > 2) i = 0;
                    increaseOpacity = true;
                    timeDelayOpacity = -2500;
                }
            }
        }
        public void pressButton(Button2 button)
        {
            if (button.name == "play") game.WorldMap();
            else if (button.name == "shop") game.RecycleStart();
            else game.StartChallenge();
        }
        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Asset.menuBackground, Vector2.Zero, null, Color.White, 0f, Vector2.Zero, 0.625f, SpriteEffects.None, 1f);
            spriteBatch.Draw(Asset.blackMist, Vector2.Zero, null, Color.White, 0f, Vector2.Zero, 0.625f, SpriteEffects.None, 0.92f);

            circleObject.draw(spriteBatch);

            spriteBatch.Draw(leftSide[0], Vector2.Zero, null, Color.White * opacity, 0f, Vector2.Zero, 0.625f, SpriteEffects.None, 0.94f);
            spriteBatch.Draw(leftSide[1], Vector2.Zero, null, Color.White * (1 - opacity), 0f, Vector2.Zero, 0.625f, SpriteEffects.None, 0.92f);
            spriteBatch.Draw(rightSide[i], new Vector2(GlobalClass.ScreenWidth - rightSide[i].Width * 0.625f, 0), null,
                Color.White * opacity, 0f, Vector2.Zero, 0.625f, SpriteEffects.None, 0.94f);
            spriteBatch.Draw(rightSide[j], new Vector2(GlobalClass.ScreenWidth - rightSide[j].Width * 0.625f, 0), null,
                Color.White * (1 - opacity), 0f, Vector2.Zero, 0.625f, SpriteEffects.None, 0.92f);

            spriteBatch.Draw(Asset.titleGame, new Vector2(GlobalClass.ScreenWidth / 2, 270), null, Color.White, 0f,
                new Vector2(Asset.titleGame.Width / 2, Asset.titleGame.Height / 2), 0.6f, SpriteEffects.None, 0.88f);

            foreach (Button2 button in buttons)
            {
                button.Draw(spriteBatch);
            }
            //tweetBox.Draw(spriteBatch, gameTime);
            settingButton.Draw(spriteBatch);
            settingPopup.Draw(spriteBatch);
        }

    }
}