using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using RecycleRobo.Scenes;



namespace RecycleRobo
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class WorldMapScene
    {
        private Texture2D texture, comming, warning;
        private Texture2D[] nameVillageTexture;
        private GameManager game;
        Vector2 backButtonPos;

        private MouseState curState;
        private MouseState preState;
        private Vector2[] nameVillageTexturePos;
        public int stateId = 0;
        Button backButton, buttonMap1, buttonMap2, buttonMap3;

        public bool isShowWarning = false;
        public bool isShowComming = false;
        int timeShowComming = 0;
        int timeShowWarning = 0;
        float scale = 0.625f;
        bool[] isShow;
        public WorldMapScene(GameManager game)
        {
            this.game = game;
            isShow = new bool[3];
            Initialize();

        }

        public void Initialize()
        {
            stateId = getStateId();
            nameVillageTexture = new Texture2D[3];


            if (stateId == 1)
            {
                nameVillageTexture[0] = game.Content.Load<Texture2D>("WorldMap/greenHeavenVillage");
                nameVillageTexture[1] = game.Content.Load<Texture2D>("WorldMap/blueLock");
                nameVillageTexture[2] = game.Content.Load<Texture2D>("WorldMap/redLock");

            }else if (stateId == 2)
            {
                nameVillageTexture[0] = game.Content.Load<Texture2D>("WorldMap/greenHeavenVillage");
                nameVillageTexture[1] = game.Content.Load<Texture2D>("WorldMap/blueSwampVillage");
                nameVillageTexture[2] = game.Content.Load<Texture2D>("WorldMap/redLock");
            }
            else if (stateId == 3)
            {
                nameVillageTexture[0] = game.Content.Load<Texture2D>("WorldMap/greenHeavenVillage");
                nameVillageTexture[1] = game.Content.Load<Texture2D>("WorldMap/blueSwampVillage");
                nameVillageTexture[2] = game.Content.Load<Texture2D>("WorldMap/redVolcanoVillage");
            }

            nameVillageTexturePos = new Vector2[3];
            nameVillageTexturePos[0] = new Vector2(80, 30);
            nameVillageTexturePos[1] = new Vector2(30, GlobalClass.ScreenHeight - nameVillageTexture[1].Height*scale - 30);
            nameVillageTexturePos[2] = new Vector2(GlobalClass.ScreenWidth - nameVillageTexture[1].Width * scale - 30, GlobalClass.ScreenHeight - nameVillageTexture[1].Height * scale - 80);



            comming = game.Content.Load<Texture2D>("WorldMap/comingSoon");
            warning = game.Content.Load<Texture2D>("WorldMap/warning");

            Dictionary<int, Texture2D> stateNMap = new Dictionary<int, Texture2D>();
            stateNMap.Add(1,Asset.world1);
            stateNMap.Add(2,Asset.world2);
            stateNMap.Add(3,Asset.world3);

            texture = stateNMap[stateId];
            

            backButtonPos = new Vector2(GlobalClass.ScreenWidth + Asset.backButtonRight.Width * 0.625f / 6,Asset.backButtonRight.Height * 0.625f / 2 + 20);

            backButton = new Button(Asset.backButtonRight, Asset.backButtonRight, Asset.backButtonRight, 0.625f, 0.0f, backButtonPos);



            Texture2D b1 = game.Content.Load<Texture2D>("WorldMap/buttonMap1");
            Texture2D b2 = game.Content.Load<Texture2D>("WorldMap/buttonMap2");
            Texture2D b3 = game.Content.Load<Texture2D>("WorldMap/buttonMap3");

            Vector2 buttonMap1Pos = new Vector2(220 + b1.Width*scale/2, 50 + b1.Height*scale/2);
            Vector2 buttonMap2Pos = new Vector2(170 + b2.Width * scale / 2, 320 + b2.Height * scale / 2);
            Vector2 buttonMap3Pos = new Vector2(730 + b3.Width * scale / 2, 220 + b3.Height * scale / 2);

            buttonMap1 = new Button(b1,b1,b1, scale, 0.0f, buttonMap1Pos);
            buttonMap2 = new Button(b2, b2, b2, scale, 0.0f, buttonMap2Pos);
            buttonMap3 = new Button(b3, b3, b3, scale, 0.0f, buttonMap3Pos);

            backButton.Clicked += new EventHandler(backButton_Clicked);
            backButton.Hover += new EventHandler(backButton_Hover);
            backButton.NotHover += new EventHandler(backButton_NotHover);
            
            
            buttonMap1.Clicked += new EventHandler(buttonMap1_Clicked);
            buttonMap2.Clicked += new EventHandler(buttonMap2_Clicked);
            buttonMap3.Clicked += new EventHandler(buttonMap3_Clicked);
        }

        public void backButton_Hover(object O, EventArgs e)
        {
            Button t = (Button)O;
            if (t.position.X > GlobalClass.ScreenWidth + Asset.backButtonRight.Width * 0.625f / 6 - 2 * Asset.backButtonRight.Width * 0.625f / 3 + 8)
            {
                t.position.X -= 4;
            }
        }
        public void backButton_NotHover(object O, EventArgs e)
        {
            Button t = (Button)O;
            if (t.position.X < GlobalClass.ScreenWidth + Asset.backButtonRight.Width * 0.625f / 6)
            {
                t.position.X += 4;
            }
        }


        public void buttonMap1_Clicked(object o, EventArgs e)
        {
            GlobalClass.curMapPlaying = 1;
            bool[] introData = new Data().Load<bool[]>("Introduction/intro.xml");
            if (introData[0] == false)
            {
                introData[0] = true;
                new Data().Save(introData,"Introduction/intro.xml");
                game.StartIntroScene();
            }
            else
                game.StartMap();
        }
        public void buttonMap2_Clicked(object o, EventArgs e)
        {
            if (stateId == 2 || stateId == 3)
            {
                GlobalClass.curMapPlaying = 2;
                game.StartMap();
            }else if (stateId == 1)
            {
                isShowWarning = true;
            }

        }
        public void buttonMap3_Clicked(object o, EventArgs e)
        {
            if (stateId == 3)
            {
                isShowComming = true;
            }
            else if (stateId == 1 || stateId == 2)
            {
                isShowWarning = true;
            }
        }
        public void backButton_Clicked(object o, EventArgs e)
        {
                game.StartMenuScene();
        }
        private int getStateId()
        {
            int id = 1;
            Level level = new Data().Load<Level>("Map\\Map_1\\Level7.xml");
            Level level2 = new Data().Load<Level>("Map\\Map_2\\Level8.xml");
            Console.WriteLine(level.isUnlock);
            if (level.isUnlock == true && level.rating != -1)
            {
                id = 2;
            }
            else if (level2.isUnlock == true && level2.rating != -1)
            {
                id = 3;
            }
            return id;
        }
        public void Update(GameTime gameTime)
        {
            backButton.Update(gameTime);
            buttonMap1.Update(gameTime);
            buttonMap2.Update(gameTime);
            buttonMap3.Update(gameTime);

            curState = Mouse.GetState();
            if (isShowComming && timeShowComming < 60)
            {
                timeShowComming += gameTime.ElapsedGameTime.Milliseconds/16;
            }
            else
            {
                isShowComming = false;
                timeShowComming = 0;
            }
            if (isShowWarning && timeShowWarning < 60)
            {
                timeShowWarning += gameTime.ElapsedGameTime.Milliseconds / 16;
            }
            else
            {
                isShowWarning = false;
                timeShowWarning = 0;
            }
            preState = curState;
        }
        public void Draw(SpriteBatch spritebatch)
        {
            backButton.Draw(spritebatch);
            for (int i = 0; i < nameVillageTexture.Length; i++)
            {
                spritebatch.Draw(nameVillageTexture[i], nameVillageTexturePos[i], null, Color.White, 0.0f, Vector2.Zero, scale, SpriteEffects.None, 0.005f);
            }
            if (isShowComming)
            {
                spritebatch.Draw(comming, new Vector2(GlobalClass.ScreenWidth/2 - comming.Width*0.45f/2, GlobalClass.ScreenHeight/2 - comming.Height*0.45f/2), null, Color.White, 0.0f, Vector2.Zero, 0.45f, SpriteEffects.None, 0.005f);
            }
            if (isShowWarning)
            {
                spritebatch.Draw(warning, new Vector2(GlobalClass.ScreenWidth / 2 - warning.Width * 0.45f / 2, GlobalClass.ScreenHeight / 2 - warning.Height * 0.45f / 2), null, Color.White, 0.0f, Vector2.Zero, 0.45f, SpriteEffects.None, 0.005f);
            }
            spritebatch.Draw(texture, new Vector2(0,0), new Rectangle(0,0,1920, 1080), Color.White, 0.0f, Vector2.Zero, 0.625f, SpriteEffects.None, 1f);

        }

    }
}
