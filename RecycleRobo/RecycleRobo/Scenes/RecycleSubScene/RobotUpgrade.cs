﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using LoadData;
using System.Collections;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;

namespace RecycleRobo.Scenes
{
    class RobotUpgrade
    {

        private GameManager game;

        private Vector2[] Pos;
        private Texture2D[] IconBackground;
        private Rectangle[] BoundingBox;
        private Button aUpgrade;
        private Button hUpgrade;
        private Texture2D attackUpgrade;
        private Texture2D attackUpgrade2;
        private Texture2D healthUpgrade;
        private Texture2D hUpgradeHover;
        private Texture2D aUpgradeHover;
        private Texture2D pUpgradeHover;

        public static List<Texture2D> allRobot;
        public int[] AttackUpgradeLevel;
        public int[] HealthUpgradeLevel;
        public SpriteFont font;

        public static float scale = 1.0f;
        private MouseState cur;
        private MouseState pre;

        private int textBoxID;
        private bool inside;
        private bool disable;
        private Rectangle borderRectangle;
        private Rectangle clippingRec;
        private RecycleScene re;

        private SoundEffect effectCorrect;
        private SoundEffect effectFailure;

        private float[] baseHealth ;
        private float[] baseAtk ;
        private float[] curHealth;
        private float[] curAtk;
        private float MAXAttack;
        private float MAXHealth;
        private int[] available;
        private static RasterizerState _rasterizerState = new RasterizerState() { ScissorTestEnable = true };


        public Texture2D healthbar;

        public RobotUpgrade(int[] AttackUpgradeLevel, int[] HealthUpgradeLevel, Rectangle clippingRec,GameManager game
            , RecycleScene re, float[] baseHealth, float[] baseAtk) 
        {
            this.re = re;
            this.game = game;
            this.AttackUpgradeLevel = AttackUpgradeLevel;
            this.HealthUpgradeLevel = HealthUpgradeLevel;
            this.clippingRec = clippingRec;
            this.baseAtk = baseAtk;
            this.baseHealth = baseHealth;
            loadRobot();
            borderRectangle = new Rectangle((int)clippingRec.X, (int)GlobalClass.ScreenHeight - 300,
                (int)((GlobalClass.ScreenWidth - clippingRec.X) - 10), 300 );
            createUpgradeButton();
            font = game.Content.Load<SpriteFont>("newFont");
            textBoxID = -1;
            inside = false;
            disable = true;
            healthbar = game.Content.Load<Texture2D>("HealthBar");
            effectCorrect = game.Content.Load<SoundEffect>("Minigame\\correct");
            effectFailure = game.Content.Load<SoundEffect>("Minigame\\wrong");
            curHealth = new float[5];
            curAtk = new float[5];
            for (int i = 0; i < curHealth.Length; i++)
                curHealth[i] = baseHealth[i] * (float)Math.Pow((RecycleScene.baseHealthIncrease)[i], HealthUpgradeLevel[i]);
            for (int i = 0; i < curHealth.Length; i++)
                curAtk[i] = baseAtk[i] * (float)Math.Pow((RecycleScene.baseAttackIncrease)[i], AttackUpgradeLevel[i]);
            MAXAttack = 1200;
            MAXHealth = 15000;
            MaxUpdate();
            loadAvailableRobot();
        }

        public void MaxUpdate() 
        {
            for (int i = 0; i < curHealth.Length; i++) 
            {
                if (curHealth[i] > MAXHealth) MAXHealth = curHealth[i];
                if (curAtk[i] > MAXAttack) MAXAttack = curAtk[i];
            }
            if (curAtk[3] * 2 > MAXAttack)
                MAXAttack = curAtk[3] * 2;
        }

        public void loadAvailableRobot() 
        {
            Data loadData = new Data();
            available= loadData.Load<RoboAvailable>("Player/RoboAvailable.xml").available;
            for (int i = 0; i < available.Length; i++) 
            {
                available[i]--;
            }
        }

        public void createUpgradeButton()
        {
            if (disable == true) return;
            Rectangle border = new Rectangle(borderRectangle.X + 20, borderRectangle.Y, borderRectangle.Width - 40, borderRectangle.Height);
            Vector2 center = new Vector2(border.X + border.Width / 2, border.Y + border.Height);
            hUpgradeHover = game.Content.Load<Texture2D>("Recycle/upgradeHealthButtonHover");
            aUpgradeHover = game.Content.Load<Texture2D>("Recycle/upgradeAttackButtonHover");
            pUpgradeHover = game.Content.Load<Texture2D>("Recycle/upgradePowerButtonHover");
            
            healthUpgrade = game.Content.Load<Texture2D>("Recycle/upgradeHealthButton");
            hUpgrade = new Button(healthUpgrade, hUpgradeHover, healthUpgrade, 0.5f, 0.0f, center - new Vector2((healthUpgrade.Width/4) + 10, + (int)(healthUpgrade.Height/4) + 10));
            attackUpgrade = game.Content.Load<Texture2D>("Recycle/upgradeAttackButton");
            attackUpgrade2 = game.Content.Load<Texture2D>("Recycle/upgradePowerButton");
            
            aUpgrade = new Button(attackUpgrade, aUpgradeHover, attackUpgrade, 0.5f, 0.0f, center + new Vector2(attackUpgrade.Width/4 + 10, - (int)(attackUpgrade.Height/4) - 10));
            hUpgrade.Clicked += new EventHandler(hUpgrade_Clicked);
            aUpgrade.Clicked += new EventHandler(aUpgrade_Clicked);
        }

        private void aUpgrade_Clicked(object sender, EventArgs e)
        {
            if (disable)
            {
                return;
            }
            if (!CheckAttackRequirement(textBoxID)) {
                effectFailure.Play();
                return;
            }
            AttackUpgradeLevel[textBoxID]++;
            curAtk[textBoxID] = baseAtk[textBoxID] * (float)Math.Pow((RecycleScene.baseAttackIncrease)[textBoxID],AttackUpgradeLevel[textBoxID]);
            MaxUpdate();
            for (int i = 0; i < re.resource.Count(); i++) 
            {
                re.resource[i] -= (int)((RecycleScene.baseAttackUpgradeRequirement)[textBoxID][i] *
                    Math.Pow((RecycleScene.AttackCostIncrease)[textBoxID],AttackUpgradeLevel[textBoxID]));
            }
            this.re.save();
            effectCorrect.Play();
        }

        private void hUpgrade_Clicked(object sender, EventArgs e)
        {
            if (disable)
            {
                return;
            }
            if (!CheckHealthRequirement(textBoxID))
            {
                effectFailure.Play();
                return;
            }
            HealthUpgradeLevel[textBoxID]++;
            curHealth[textBoxID] = baseHealth[textBoxID] * (float)Math.Pow((RecycleScene.baseHealthIncrease)[textBoxID], HealthUpgradeLevel[textBoxID]);
            MaxUpdate();
            for (int i = 0; i < re.resource.Count(); i++)
            {
                re.resource[i] -= (int)((RecycleScene.baseHealthUpgradeRequirement)[textBoxID][i] * 
                    Math.Pow((RecycleScene.HealthCostIncrease)[textBoxID], HealthUpgradeLevel[textBoxID]));
            }
            this.re.save();
            effectCorrect.Play();
        }

        public float scaleText(Vector2 size, Rectangle border, String text, SpriteFont font) 
        {
            float result = 1.0f;
            while (size.X * result >= border.Width - 10) 
            {
                result = result * .9f;
            }
            return result;
        }

        public void DrawTextBox(SpriteBatch spriteBatch, int ID)
        {
            if (!inside) return;
            if (textBoxID == -1) return;
            Texture2D boxTexture = Asset.infoBackground;
            spriteBatch.Draw(boxTexture, borderRectangle, null, Color.White, 0.0f, new Vector2(0, 0), SpriteEffects.None, 0.98f);
            Rectangle border = new Rectangle(borderRectangle.X + 20, borderRectangle.Y, borderRectangle.Width - 40, borderRectangle.Height);
            List<String> textToWrite = getText(ID);
            for (int i = 0; i < textToWrite.Count; i++)
            {
                String text = textToWrite[i];
                Vector2 size = font.MeasureString(text);
                float scale = scaleText(size,border,text,font);
                size = size * scale;
                if (i == 0)
                    spriteBatch.DrawString(font, text, 
                            new Vector2(border.X + border.Width / 2 - size.X / 2, border.Y + 10 + i * 35)
                            , Color.White, 0.0f, new Vector2(), scale, SpriteEffects.None, 0.9f);
                else if (i == 1)
                    spriteBatch.DrawString(font, text, 
                            new Vector2(border.X + border.Width / 2 - size.X / 2, border.Y + 10 + i * 35)
                            , Color.Purple, 0.0f, new Vector2(), scale, SpriteEffects.None, 0.9f);
                else if (i == 2)
                {
                    if (CheckAttackRequirement(ID))
                        spriteBatch.DrawString(font, text, 
                            new Vector2(border.X + border.Width / 2 - size.X / 2, border.Y + 10 + i * 35)
                            , Color.Green, 0.0f, new Vector2(), scale, SpriteEffects.None, 0.9f);
                    else
                        spriteBatch.DrawString(font, text, 
                            new Vector2(border.X + border.Width / 2 - size.X / 2, border.Y + 10 + i * 35)
                            , Color.Red, 0.0f, new Vector2(), scale, SpriteEffects.None, 0.9f);
                }
                else if (i == 3)
                {
                    if (CheckHealthRequirement(ID))
                        spriteBatch.DrawString(font, text, 
                            new Vector2(border.X + border.Width / 2 - size.X / 2, border.Y + 10 + i * 35)
                            , Color.Green, 0.0f, new Vector2(), scale, SpriteEffects.None, 0.9f);
                    else
                        spriteBatch.DrawString(font, text, 
                            new Vector2(border.X + border.Width / 2 - size.X / 2, border.Y + 10 + i * 35)
                            , Color.Red, 0.0f, new Vector2(), scale, SpriteEffects.None, 0.9f);
                }
                else 
                {
                    spriteBatch.DrawString(font, text, 
                            new Vector2(border.X + border.Width / 2 - size.X / 2, border.Y + 10 + i * 35)
                            , Color.OrangeRed, 0.0f, new Vector2(), scale, SpriteEffects.None, 0.9f);
                }
            }
            if (!disable)
            {
                aUpgrade.Draw(spriteBatch);
                hUpgrade.Draw(spriteBatch);

                DrawHealth(ID, new Vector2(hUpgrade.getBound().X + hUpgrade.getBound().Width, hUpgrade.getBound().Y)
                        , new Vector2(aUpgrade.getBound().X, aUpgrade.getBound().Y)
                        , spriteBatch);
            }
        }

        private void DrawHealth(int i, Vector2 Position, Vector2 Position2, SpriteBatch spriteBatch)
        {
            if (i == -1) return;
            Rectangle health = new Rectangle((int)Position.X - (int)(healthbar.Width * curHealth[i] / MAXHealth * 4)
                , (int)Position.Y - 30
                , (int)(healthbar.Width * curHealth[i] / MAXHealth * 4), 15);
            Rectangle mana;
            if(i != 3)
                mana = new Rectangle((int)Position2.X, (int)Position2.Y - 30
                    , (int)(healthbar.Width * curAtk[i] / MAXAttack * 4), 15);
            else
                mana = new Rectangle((int)Position2.X, (int)Position2.Y - 30
                    , (int)(healthbar.Width * curAtk[i]*2 / MAXAttack * 4), 15);
            spriteBatch.DrawString(font, ((int)curHealth[i]).ToString()
                , new Vector2(health.X - font.MeasureString(((int)curHealth[i]).ToString()).X-10, health.Y), Color.White);
            spriteBatch.DrawString(font, ((int)curAtk[i]).ToString(), new Vector2(mana.X + mana.Width + 10, mana.Y)
                , Color.White);
            spriteBatch.Draw(healthbar, health, null, Color.Red, 0f, Vector2.Zero, SpriteEffects.None, 0.8f);
            spriteBatch.Draw(healthbar, mana, null, Color.Blue, 0f, Vector2.Zero, SpriteEffects.None, 0.8f);
        }

        public void loadRobot() 
        {
            allRobot = new List<Texture2D>();
            allRobot.Add(Asset.numberOne);
            allRobot.Add(Asset.numberTwo);
            allRobot.Add(Asset.numberThree);
            allRobot.Add(Asset.numberFour);
            allRobot.Add(Asset.numberFive);


            Pos = new Vector2[allRobot.Count()];
            Pos[0] = new Vector2(clippingRec.X, clippingRec.Y);
            IconBackground = new Texture2D[allRobot.Count()];
            BoundingBox = new Rectangle[allRobot.Count];
            for (int i = 0; i < allRobot.Count; i++)
            {
                IconBackground[i] = Asset.ownedBackground;
                if (i >= 1) Pos[i] = new Vector2(Pos[i - 1].X + IconBackground[i].Width * scale + 20, clippingRec.Y);
                BoundingBox[i] = new Rectangle((int)Pos[i].X, (int)clippingRec.Y,
                    (int)(IconBackground[i].Width * scale),
                    (int)(IconBackground[i].Height * scale));
            }
        }

        public List<String> getText(int ID)
        {
            String[] typesOfRes = {"plastic","paper","glass","compost","metal" };
            List<String> result = new List<string>();
            if (!available.Contains(ID))
            {
                result.Add("You have not unlocked this robot");
                return result;
            }
            if (ID == 0)
                result.Add("Sam's first robot \"DEMO\", well-rounded and very effective");
            else if (ID == 1)
                result.Add("RAY - The fearless eagle of Green Heaven with powerful range attack");
            else if (ID == 2)
                result.Add("CRUSHER - The power of Mother Russia in your hand");
            else if (ID == 3)
                result.Add("SOUL - It's a deer and it's a healer, Sam calls it a deerler");
            else if (ID == 4)
                result.Add("LONESOME GEORGE - George is awesome, annnnnnnnnnnnnnnd ForeverAlone");
            if (ID != 3)
            {
                String tmp = "Health is at ";
                if (HealthUpgradeLevel[textBoxID] == 0)
                    tmp += "base level ";
                else
                    tmp += "level " + HealthUpgradeLevel[textBoxID] + " ";
                tmp += "and attack is at ";
                if (AttackUpgradeLevel[textBoxID] == 0)
                    tmp += "base level ";
                else
                    tmp += "level " + AttackUpgradeLevel[textBoxID];
                String AttacktextTowrite = "";
                String HealthtextTowrite = "";
                result.Add(tmp);
                for (int i = 0; i < typesOfRes.Length; i++) 
                {
                    HealthtextTowrite += (int)((RecycleScene.baseHealthUpgradeRequirement)[ID][i] * Math.Pow((RecycleScene.HealthCostIncrease)[ID], HealthUpgradeLevel[ID] + 1)) + " " + typesOfRes[i];
                    AttacktextTowrite += (int)((RecycleScene.baseAttackUpgradeRequirement)[ID][i] * Math.Pow((RecycleScene.AttackCostIncrease)[ID], AttackUpgradeLevel[ID] + 1)) + " " + typesOfRes[i];
                    if (i != typesOfRes.Length - 1) 
                    {
                        HealthtextTowrite += ",";
                        AttacktextTowrite += ",";
                    }
                }
                result.Add("Requirement(atack): " + AttacktextTowrite);
                result.Add("Requirement(health): " + HealthtextTowrite);
            }
            else{
                String tmp = "Health is at ";
                if (HealthUpgradeLevel[textBoxID] == 0)
                    tmp += "base level ";
                else
                    tmp += "level " + HealthUpgradeLevel[textBoxID] + " ";
                tmp += "and heal power is at ";
                if (AttackUpgradeLevel[textBoxID] == 0)
                    tmp += "base level ";
                else
                    tmp += "level " + AttackUpgradeLevel[textBoxID];
                result.Add(tmp);
                String AttacktextTowrite = "";
                String HealthtextTowrite = "";
                for (int i = 0; i < typesOfRes.Length; i++)
                {
                    HealthtextTowrite += (int)((RecycleScene.baseHealthUpgradeRequirement)[ID][i] * Math.Pow((RecycleScene.HealthCostIncrease)[ID], HealthUpgradeLevel[ID] + 1)) + " " + typesOfRes[i];
                    AttacktextTowrite += (int)((RecycleScene.baseAttackUpgradeRequirement)[ID][i] * Math.Pow((RecycleScene.AttackCostIncrease)[ID], AttackUpgradeLevel[ID] + 1)) + " " + typesOfRes[i];
                    if (i != typesOfRes.Length - 1)
                    {
                        HealthtextTowrite += ",";
                        AttacktextTowrite += ",";
                    }
                }
                result.Add("Requirement(power): " + AttacktextTowrite);
                result.Add("Requirement(health): " + HealthtextTowrite);
            }
            if (ID == 0) 
            {
                result.Add("Nobody likes carrying recycle bins except DEMO, he's a garbage truck");
            }
            else if (ID == 1) 
            {
                result.Add("Have you seen an eagle carrying garbage? NOPE");
            }
            else if (ID == 2) 
            {
                result.Add("Too badass to carry garbage.");
            }
            else if (ID == 3) 
            {
                result.Add("SOUL cannot carry recycle kits. Too elegant may be ?");
            }
            else if (ID == 4) 
            {
                result.Add("GEORGE thinks recycle bins make him sexier. Too bad for him, and good for us");
            }
            return result;
        }

        public bool CheckAttackRequirement(int ID) 
        {
            for (int i = 0; i < (re.resource).Count(); i++) 
            {
                if ((re.resource[i] < (int) ((RecycleScene.baseAttackUpgradeRequirement)[ID][i] * Math.Pow((RecycleScene.AttackCostIncrease)[ID], AttackUpgradeLevel[ID]+1))))
                {
                    return false;
                }
            }
            return true;
        }

        public bool CheckHealthRequirement(int ID)
        {
            for (int i = 0; i < (re.resource).Count(); i++)
            {
                if ((re.resource[i] < (int)((RecycleScene.baseHealthUpgradeRequirement)[ID][i] * Math.Pow((RecycleScene.HealthCostIncrease)[ID], HealthUpgradeLevel[ID]+1))))
                {
                    return false;
                }
            }
            return true;
        }

        public void Update(GameTime gametime)
        {
            cur = Mouse.GetState();
            if (!clippingRec.Contains(new Point((int)MouseHelper.MousePosition(cur).X, (int)MouseHelper.MousePosition(cur).Y)))
            {
                inside = false;
                return;
            }
            else
            {
                inside = true;
            }

            if (!available.Contains(textBoxID))
            {
                disable = true;
            }
            else 
            {
                disable = false;
            }

            if (cur.LeftButton == ButtonState.Pressed && pre.LeftButton == ButtonState.Released)
            {
                for (int i = 0; i < allRobot.Count; i++)
                {
                    if ((BoundingBox[i]).Contains(new Point(MouseHelper.MousePosition(cur).X, MouseHelper.MousePosition(cur).Y)))
                    {
                        if (i == 3)
                        {
                            aUpgrade.setTexture(attackUpgrade2, pUpgradeHover);
                        }
                        else 
                        {
                            aUpgrade.setTexture(attackUpgrade, aUpgradeHover);
                        }
                        textBoxID = i;
                        IconBackground[i] = game.Content.Load<Texture2D>("Recycle/selectedState");
                    }
                    else if ((!aUpgrade.getBound().Contains((new Point(MouseHelper.MousePosition(cur).X, MouseHelper.MousePosition(cur).Y)))) &&
                        (!hUpgrade.getBound().Contains((new Point(MouseHelper.MousePosition(cur).X, MouseHelper.MousePosition(cur).Y)))))
                    {
                        IconBackground[i] = game.Content.Load<Texture2D>("Recycle/ownedBackground");
                    }
                }
            }
            if (cur.LeftButton == ButtonState.Pressed && pre.LeftButton == ButtonState.Pressed)
            {
                Vector2 Target = new Vector2(MouseHelper.MousePosition(cur).X, MouseHelper.MousePosition(cur).Y);
                Vector2 Position = new Vector2(MouseHelper.MousePosition(pre).X, MouseHelper.MousePosition(pre).Y);
                Vector2 direction = Target - Position;

                if (direction.Length() > 40)
                {
                    direction.Normalize();
                    direction = direction * 40;
                }
                if (Pos[0].X > clippingRec.X + 10 && direction.X > 0) return;
                if (Pos[Pos.Count() - 1].X + IconBackground[0].Width < GlobalClass.ScreenWidth
                    && direction.X < 0) return;

                for (int i = 0; i < allRobot.Count; i++)
                {
                    Pos[i] = new Vector2((Pos[i]).X + (direction.X), (Pos[i]).Y);
                    BoundingBox[i] = (new Rectangle((int)((Pos[i]).X), (int)((Pos[i]).Y),
                    (IconBackground[i]).Width, (IconBackground[i]).Height));
                }
            }
            pre = cur;
            aUpgrade.Update(gametime);
            hUpgrade.Update(gametime);
            re.AttackUpgradelevel = this.AttackUpgradeLevel;
            re.HealthUpgradelevel = this.HealthUpgradeLevel;
        }
        public void DrawRobot(GameTime gametime, SpriteBatch spriteBatch)
        {
            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend,
                      null, null, _rasterizerState, null, Resolution.getTransformationMatrix());
            spriteBatch.GraphicsDevice.ScissorRectangle = clippingRec;
            for (int i = 0; i < allRobot.Count; i++)
            {
                int pos1 = (int)(Pos[i]).X + (IconBackground[i]).Width / 2 - (allRobot[i]).Width / 2;

                int pos2 = (int)(Pos[i]).Y + (IconBackground[i]).Height / 2 - (allRobot[i]).Height / 2;

                int width = (allRobot[i]).Width;

                int height = (allRobot[i]).Height;
                if (available.Contains(i))
                {
                    spriteBatch.Draw((IconBackground[i])
                        , (Rectangle)BoundingBox[i]
                        , null, Color.White, 0.0f, new Vector2(0, 0), SpriteEffects.None, 0.98f);
                    spriteBatch.Draw((allRobot[i]), new Rectangle(pos1, pos2, width, height),
                    null, Color.White, 0.0f, new Vector2(0, 0), SpriteEffects.None, 0.98f);
                }
                else 
                {
                    spriteBatch.Draw((IconBackground[i])
                        , (Rectangle)BoundingBox[i]
                        , null, Color.White*0.4f, 0.0f, new Vector2(0, 0), SpriteEffects.None, 0.98f);
                    spriteBatch.Draw((allRobot[i]), new Rectangle(pos1, pos2, width, height),
                    null, Color.White*0.4f, 0.0f, new Vector2(0, 0), SpriteEffects.None, 0.98f);
                }
            }
            DrawTextBox(spriteBatch, textBoxID);
            spriteBatch.End();
        }
    }
}
