﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using LoadData;
using System.Collections;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;
namespace RecycleRobo.Scenes
{
    public class CyclebotView
    {
        private MouseState cur, prev;
        private RecycleScene recycle;

        private Texture2D levelLayer1;
        private List<Texture2D> levelLayer2;
  
        private List<Texture2D> robotName;
        private List<Texture2D> robotList;
        private List<Texture2D> robotIcon;

        private AnimationPlayer animationPlayer;
        private AnimationPlayer formPlayer;
        private AnimationPlayer areaPlayer;
        private Animation animation;
        private static Animation[,] animationList = { { Asset.gestureNULL, Asset.gestureN, Asset.gestureZ, Asset.gestureHorizontal },
                                                    { Asset.gestureNULL, Asset.gestureNULL, Asset.gestureNULL, Asset.gestureTap },
                                                    { Asset.gestureNULL, Asset.gestureO, Asset.gestureU, Asset.gestureNULL },
                                                    { Asset.gestureNULL, Asset.gestureNULL, Asset.gestureV, Asset.gestureNULL },
                                                    { Asset.gestureNULL, Asset.gestureN, Asset.gestureHorizontal, Asset.gestureVerticle },};
        private static Animation[,,] robotIdles = { //robotNum - evoNum - formNum
                                                    { 
                                                        {GlobalClass.getAnimationByName("NUMBER1_IDLE_1"),
                                                        GlobalClass.getAnimationByName("NUMBER1_IDLE_1_THORN"),
                                                        GlobalClass.getAnimationByName("NUMBER1_IDLE_1"),
                                                        GlobalClass.getAnimationByName("NUMBER1_IDLE_1"),
                                                        },
                                                        {GlobalClass.getAnimationByName("NUMBER1_IDLE_2"),
                                                        GlobalClass.getAnimationByName("NUMBER1_IDLE_2_THORN"),
                                                        GlobalClass.getAnimationByName("NUMBER1_IDLE_2_SEAL"),
                                                        GlobalClass.getAnimationByName("NUMBER1_IDLE_2"),
                                                        },
                                                        {GlobalClass.getAnimationByName("NUMBER1_IDLE_3"),
                                                        GlobalClass.getAnimationByName("NUMBER1_IDLE_3_THORN"),
                                                        GlobalClass.getAnimationByName("NUMBER1_IDLE_3_SEAL"),
                                                        GlobalClass.getAnimationByName("NUMBER1_IDLE_3_FIRE"),
                                                        }
                                                    },
                                                    { 
                                                        {GlobalClass.getAnimationByName("NUMBER2_IDLE_1"),
                                                        GlobalClass.getAnimationByName("NUMBER2_IDLE_1"),
                                                        GlobalClass.getAnimationByName("NUMBER2_IDLE_1"),
                                                        GlobalClass.getAnimationByName("NUMBER2_IDLE_1"),
                                                        },
                                                        {GlobalClass.getAnimationByName("NUMBER2_IDLE_2"),
                                                        GlobalClass.getAnimationByName("NUMBER2_IDLE_2"),
                                                        GlobalClass.getAnimationByName("NUMBER2_IDLE_2"),
                                                        GlobalClass.getAnimationByName("NUMBER2_IDLE_2"),
                                                        },
                                                        {GlobalClass.getAnimationByName("NUMBER2_IDLE_3"),
                                                        GlobalClass.getAnimationByName("NUMBER2_IDLE_3"),
                                                        GlobalClass.getAnimationByName("NUMBER2_IDLE_3"),
                                                        GlobalClass.getAnimationByName("NUMBER2_IDLE_3_PHOENIX"),
                                                        },
                                                    },
                                                    {
                                                        {GlobalClass.getAnimationByName("NUMBER3_IDLE_1"),
                                                        GlobalClass.getAnimationByName("NUMBER3_IDLE_1"),
                                                        GlobalClass.getAnimationByName("NUMBER3_IDLE_1"),
                                                        GlobalClass.getAnimationByName("NUMBER3_IDLE_1")
                                                        },
                                                        {GlobalClass.getAnimationByName("NUMBER3_IDLE_2"),
                                                        GlobalClass.getAnimationByName("NUMBER3_IDLE_2_CUTTER"),
                                                        GlobalClass.getAnimationByName("NUMBER3_IDLE_2_ICEFANG"),
                                                        GlobalClass.getAnimationByName("NUMBER3_IDLE_2")
                                                        },
                                                        {GlobalClass.getAnimationByName("NUMBER3_IDLE_3"),
                                                        GlobalClass.getAnimationByName("NUMBER3_IDLE_3_CUTTER"),
                                                        GlobalClass.getAnimationByName("NUMBER3_IDLE_3_ICEFANG"),
                                                        GlobalClass.getAnimationByName("NUMBER3_IDLE_3")
                                                        }
                                                    },
                                                    {
                                                        {GlobalClass.getAnimationByName("NUMBER4_IDLE_1"),
                                                        GlobalClass.getAnimationByName("NUMBER4_IDLE_1"),
                                                        GlobalClass.getAnimationByName("NUMBER4_IDLE_1"),
                                                        GlobalClass.getAnimationByName("NUMBER4_IDLE_1"),
                                                        },
                                                        {GlobalClass.getAnimationByName("NUMBER4_IDLE_2"),
                                                        GlobalClass.getAnimationByName("NUMBER4_IDLE_2"),
                                                        GlobalClass.getAnimationByName("NUMBER4_IDLE_2_HERB"),
                                                        GlobalClass.getAnimationByName("NUMBER4_IDLE_2"),
                                                        },
                                                        {GlobalClass.getAnimationByName("NUMBER4_IDLE_3"),
                                                        GlobalClass.getAnimationByName("NUMBER4_IDLE_3"),
                                                        GlobalClass.getAnimationByName("NUMBER4_IDLE_3_HERB"),
                                                        GlobalClass.getAnimationByName("NUMBER4_IDLE_3"),
                                                        },
                                                    },
                                                    {
                                                        {GlobalClass.getAnimationByName("NUMBER5_IDLE_1"),
                                                        GlobalClass.getAnimationByName("NUMBER5_IDLE_1_THORN"),
                                                        GlobalClass.getAnimationByName("NUMBER5_IDLE_1"),
                                                        GlobalClass.getAnimationByName("NUMBER5_IDLE_1"),
                                                        },
                                                        {GlobalClass.getAnimationByName("NUMBER5_IDLE_2"),
                                                        GlobalClass.getAnimationByName("NUMBER5_IDLE_2_THORN"),
                                                        GlobalClass.getAnimationByName("NUMBER5_IDLE_2_WAVE"),
                                                        GlobalClass.getAnimationByName("NUMBER5_IDLE_2"),
                                                        },
                                                        {GlobalClass.getAnimationByName("NUMBER5_IDLE_3"),
                                                        GlobalClass.getAnimationByName("NUMBER5_IDLE_3_THORN"),
                                                        GlobalClass.getAnimationByName("NUMBER5_IDLE_3_WAVE"),
                                                        GlobalClass.getAnimationByName("NUMBER5_IDLE_3_STEAM"),
                                                        },
                                                    },
                                                 };
        private static int[] originAnimation = { 0, 2, 2, 1, 1};
        private static Animation[,] areaForm = { 
                                                    { GlobalClass.getAnimationByName("NUM1_AREA_1"),
                                                    GlobalClass.getAnimationByName("NUM1_AREA_2"),
                                                    GlobalClass.getAnimationByName("NUM1_AREA_3"),
                                                    GlobalClass.getAnimationByName("NUM1_AREA_4"),
                                                    },
                                                    { GlobalClass.getAnimationByName("NUM2_AREA_1"),
                                                    GlobalClass.getAnimationByName("NUM2_AREA_1"),
                                                    GlobalClass.getAnimationByName("NUM2_AREA_1"),
                                                    GlobalClass.getAnimationByName("NUM2_AREA_4"),
                                                    },
                                                    { GlobalClass.getAnimationByName("NUM3_AREA_1"),
                                                    GlobalClass.getAnimationByName("NUM3_AREA_2"),
                                                    GlobalClass.getAnimationByName("NUM3_AREA_3"),
                                                    GlobalClass.getAnimationByName("NUM3_AREA_3"),
                                                    },
                                                    { GlobalClass.getAnimationByName("NUM4_AREA_1"),
                                                    GlobalClass.getAnimationByName("NUM4_AREA_1"),
                                                    GlobalClass.getAnimationByName("NUM4_AREA_3"),
                                                    GlobalClass.getAnimationByName("NUM4_AREA_3"),
                                                    },
                                                    { GlobalClass.getAnimationByName("NUM5_AREA_1"),
                                                    GlobalClass.getAnimationByName("NUM5_AREA_2"),
                                                    GlobalClass.getAnimationByName("NUM5_AREA_3"),
                                                    GlobalClass.getAnimationByName("NUM5_AREA_4"),
                                                    },
                                               };

        private static Vector2[,] gestureOffset = {{new Vector2(40, 200),new Vector2(40, 200),new Vector2(40, 200),new Vector2(40, 200)},
                                                  {new Vector2(40, 180), new Vector2(40, 180), new Vector2(40, 180), new Vector2(40, 180)},
                                                  {new Vector2(40, 180),new Vector2(40, 180),new Vector2(40, 180),new Vector2(40, 180)},
                                                  {new Vector2(40, 200),new Vector2(40, 200),new Vector2(40, 200),new Vector2(40, 200)},
                                                  {new Vector2(60, 180),new Vector2(40, 200),new Vector2(50, 200),new Vector2(60, 180)}};

        private static Vector2[,] areaOffset = { 
                                               {new Vector2(0,0), new Vector2(33,243),new Vector2(25,232),new Vector2(30,240)},
                                               {new Vector2(0,0),new Vector2(0,0),new Vector2(0,0),new Vector2(52,230)},
                                               {new Vector2(0,0),new Vector2(40,235),new Vector2(30,235),new Vector2(0,0)},
                                               {new Vector2(0,0),new Vector2(0,0),new Vector2(30,245),new Vector2(0,0)},
                                               {new Vector2(0,0),new Vector2(25,225),new Vector2(30,220),new Vector2(25,220)},
                                               };
        private static Vector2[,] formOffset = { 
                                               {new Vector2(0,0), new Vector2(40,200),new Vector2(45,190),new Vector2(60,190)},
                                               {new Vector2(0,0),new Vector2(0,0),new Vector2(0,0),new Vector2(30,197)},
                                               {new Vector2(0,0),new Vector2(45,175),new Vector2(27,167),new Vector2(0,0)},
                                               {new Vector2(0,0),new Vector2(0,0),new Vector2(35,180),new Vector2(0,0)},
                                               {new Vector2(0,0),new Vector2(30,210),new Vector2(40,200),new Vector2(40,200)},
                                               };

        private static Vector2 mainOffset = new Vector2(40, 200);

        private static Texture2D[,] descriptionBox = { { Asset.desc1_1, Asset.desc1_2, Asset.desc1_3, Asset.desc1_4 },
                                                     { Asset.desc2_1, Asset.desc2_2, Asset.desc2_3, Asset.desc2_4 },
                                                     { Asset.desc3_1, Asset.desc3_2, Asset.desc3_3, Asset.desc3_4 },
                                                     { Asset.desc4_1, Asset.desc4_2, Asset.desc4_3, Asset.desc4_4 },
                                                     { Asset.desc5_1, Asset.desc5_2, Asset.desc5_3, Asset.desc5_4 },};
        private static Vector2 descriptionBoxPos = new Vector2(100,100);

        private Texture2D textBox;
        private Vector2 textBoxPlace;

        private int robotNumber;

        public static int[,] transitionLevel = { {0,0,0,0,0}, { 9, 10, 12, 10, 15 }, { 12, 15, 18, 15, 20 } };
        public static int[,] formTransitionLevel = { {0,0,0,0,0}, { 6,7,10,0,10 }, { 10,10,14,10,15 }, { 15,15,18,18,20} };

        public static Texture2D[] levelLayer2List = { Asset.level1_1, Asset.level2_1, Asset.level3_1, Asset.level4_1 };

        private static float scale = 0.525f;
        private static Vector2 robotIconPlaces = new Vector2(300,100);
        private static Rectangle[] places = { new Rectangle(0, 130, 240, 80),
                                            new Rectangle(0, 218, 240, 80),
                                            new Rectangle(0, 306, 240, 80),
                                            new Rectangle(0, 393, 240, 80),
                                            new Rectangle(0, 480, 240, 80),};

        private static Vector2[] AnimationTriggerPoints = {new Vector2(873,547),
                                                          new Vector2(920,401),
                                                          new Vector2(1020,285),
                                                          new Vector2(1174,237),};
        private List<int> formLevel;
        private List<int> evolutionLevel;
        private int descriptionBoxTriggerID;

        private static Vector2 AttackTextPos = new Vector2(645,588);
        private static Vector2 AttackTextPos2 = new Vector2(639,645);
        private static Vector2 DeffenceTextPos = new Vector2(739, 588);
        private static Vector2 DeffenceTextPos2 = new Vector2(732, 642);
        private static Vector2 levelTextPos = new Vector2(1080, 540);
        private static Vector2 descriptionTextPos = new Vector2(380, 496);

        private int[] def;
        private int[] atk;
        private int[] def2;
        private int[] atk2;

        private static String[] descriptionText = { "Sam's first robot \"DEMO\", well-rounded and very effective",
                                                  "RAY - The fearless eagle of Green Heaven with time travel ability",
                                                  "CRUSHER - The power of Mother Russia",
                                                  "SOUL - It's a deer and it's a healer, Sam calls it a deerler",
                                                  "LONESOME GEORGE - George is awesome, annnnnnnnnnnnnnnd ForeverAlone"};

        public CyclebotView(RecycleScene re) 
        {
            robotNumber = 0;
            formLevel = new List<int>();
            evolutionLevel = new List<int>();
            descriptionBoxTriggerID = -1;
            robotName = new List<Texture2D>();
            robotList = new List<Texture2D>();

            robotName.Add(Asset.cyclebotList1);
            robotName.Add(Asset.cyclebotList2);
            robotName.Add(Asset.cyclebotList3);
            robotName.Add(Asset.cyclebotList4);
            robotName.Add(Asset.cyclebotList5);

            robotList.Add(Asset.selected_1);
            robotList.Add(Asset.selected_2);
            robotList.Add(Asset.selected_3);
            robotList.Add(Asset.selected_4);
            robotList.Add(Asset.selected_5);

            List<List<Texture2D>> robotFullIcon = new List<List<Texture2D>>();

            robotIcon = new List<Texture2D>();
            robotIcon.Add(Asset.num1_lv1);
            robotIcon.Add(Asset.num2_lv1);
            robotIcon.Add(Asset.num3_lv1);
            robotIcon.Add(Asset.num4_lv1);
            robotIcon.Add(Asset.num5_lv1);
            robotFullIcon.Add(robotIcon);

            robotIcon = new List<Texture2D>();
            robotIcon.Add(Asset.num1_lv2);
            robotIcon.Add(Asset.num2_lv2);
            robotIcon.Add(Asset.num3_lv2);
            robotIcon.Add(Asset.num4_lv2);
            robotIcon.Add(Asset.num5_lv2);
            robotFullIcon.Add(robotIcon);

            robotIcon = new List<Texture2D>();
            robotIcon.Add(Asset.num1_lv3);
            robotIcon.Add(Asset.num2_lv3);
            robotIcon.Add(Asset.num3_lv3);
            robotIcon.Add(Asset.num4_lv3);
            robotIcon.Add(Asset.num5_lv3);
            robotFullIcon.Add(robotIcon);
            
            textBox = Asset.descriptionBox;
            textBoxPlace = new Vector2(150, GlobalClass.ScreenHeight - textBox.Height * scale);

            this.recycle = re;
            levelLayer1 = Asset.levelDisplay;
            checkForm(robotFullIcon);

            animation = Asset.gestureNULL;
            getLevel();
        }

        public void getLevel() 
        {
            atk = new int[5];
            def = new int[5];
            atk2 = new int[5];
            def2 = new int[5];
            for (int i = 0; i < robotName.Count; i++) 
            {
                def[i] = (int)(recycle.baseHealth[i] * Math.Pow(recycle.baseHealthIncrease[i], RecycleScene.robotLevel[i])) / 100;
                atk[i] = (int)(recycle.baseDamage[i] * Math.Pow(recycle.baseAttackIncrease[i], RecycleScene.robotLevel[i])) /2;
                def2[i] = (int)(recycle.baseHealth[i] * Math.Pow(recycle.baseHealthIncrease[i], RecycleScene.robotLevel[i] + 1)) / 100;
                atk2[i] = (int)(recycle.baseDamage[i] * Math.Pow(recycle.baseAttackIncrease[i], RecycleScene.robotLevel[i] + 1)) / 2;
            }
        }

        public static string WrapText(SpriteFont spriteFont, string text, float maxLine)
        {
            string[] words = text.Split(' ');
            StringBuilder sb = new StringBuilder();
            float lineLength = 0;
            float space = spriteFont.MeasureString(" ").X;

            foreach (string word in words)
            {
                Vector2 lineSize = spriteFont.MeasureString(word);
                if (lineLength + lineSize.X < maxLine)
                {
                    sb.Append(word + " ");
                    lineLength += lineSize.X + space;
                }
                else
                {
                    sb.Append("\n" + word + " ");
                    lineLength = lineSize.X + space;
                }
            }
            return sb.ToString();
        }

        public void checkForm(List<List<Texture2D>> robotFullIcon)
        {
            robotIcon = new List<Texture2D>();
            levelLayer2 = new List<Texture2D>();
            for (int i = 0; i < RecycleScene.robotLevel.Length; i++) 
            {
                for (int j = transitionLevel.GetLength(0)-1 ; j >= 0; j--) 
                {
                    if (RecycleScene.robotLevel[i] >= transitionLevel[j, i]) 
                    {
                        robotIcon.Add(robotFullIcon[j][i]);
                        evolutionLevel.Add(j);
                        break;
                    }
                }
                for (int j = formTransitionLevel.GetLength(0) - 1; j >= 0; j--)
                {
                    if (RecycleScene.robotLevel[i] >= formTransitionLevel[j, i])
                    {
                        levelLayer2.Add(levelLayer2List[j]);
                        formLevel.Add(j);
                        break;
                    }
                }
            }
        }

        public void Update(GameTime gametime) 
        {
            cur = Mouse.GetState();
            Point mousePoint = MouseHelper.MousePosition(cur);
            Vector2 currentPos = new Vector2(mousePoint.X,mousePoint.Y);
            bool check = true;
            
            for (int i = 0; i <= formLevel[robotNumber]; i++) 
            {
                if (Vector2.Distance(AnimationTriggerPoints[i], currentPos) < 35) 
                {
                    descriptionBoxTriggerID = i;
                    //Console.WriteLine(descriptionBoxTriggerID);
                    check = false;
                    break;
                }
            }
            if (check) descriptionBoxTriggerID = -1;
            if (cur.LeftButton == ButtonState.Pressed)
            {
                for (int i = 0; i < places.Length; i++)
                {
                    if (places[i].Contains(mousePoint))
                    {
                        if (recycle.robotAvailable.available.Contains(i+1))
                        {
                            robotNumber = i;
                            goto next;
                        }
                    }
                }
            }
            next:
            prev = cur;
        }
        public void Draw(GameTime gametime, SpriteBatch spriteBatch) 
        {
            Vector2 layer1position = new Vector2(GlobalClass.ScreenWidth - levelLayer1.Width * scale
                , GlobalClass.ScreenHeight - levelLayer1.Height * scale);
            Vector2 layer2position = new Vector2(GlobalClass.ScreenWidth - levelLayer2[robotNumber].Width * scale
                , GlobalClass.ScreenHeight - levelLayer2[robotNumber].Height * scale);


            spriteBatch.Draw(robotList[robotNumber], Vector2.Zero, null, Color.White, 0, Vector2.Zero, 0.625f, SpriteEffects.None, 0.35f);
            spriteBatch.Draw(Asset.unknown, Vector2.Zero, null, Color.White, 0, Vector2.Zero, 0.625f, SpriteEffects.None, 0.55f);
            for (int i = 0; i < robotName.Count; i++) 
            {
                if(recycle.robotAvailable.available.Contains(i+1))
                    spriteBatch.Draw(robotName[i],Vector2.Zero, null, Color.White, 0, Vector2.Zero, 0.625f, SpriteEffects.None, 0.45f);
            }

            spriteBatch.Draw(levelLayer1, layer1position, null, Color.White, 0, Vector2.Zero, scale, SpriteEffects.None, 0.5f);
            spriteBatch.Draw(levelLayer2[robotNumber], layer2position, null, Color.White, 0, Vector2.Zero, scale, SpriteEffects.None, 0.25f);
     
            spriteBatch.Draw(robotIcon[robotNumber], robotIconPlaces, null, Color.White, 0, Vector2.Zero, 0.5f, SpriteEffects.None, 0.75f);
            spriteBatch.Draw(textBox, textBoxPlace, null, Color.White, 0, Vector2.Zero, scale, SpriteEffects.None, 0.25f);
            
            spriteBatch.DrawString(Asset.smallFont, def2[robotNumber].ToString(), DeffenceTextPos2, Color.White);
            spriteBatch.DrawString(Asset.smallFont, atk2[robotNumber].ToString(), AttackTextPos2, Color.White);
            spriteBatch.DrawString(Asset.mediumFont, def[robotNumber].ToString(), DeffenceTextPos, Color.White);
            spriteBatch.DrawString(Asset.mediumFont, atk[robotNumber].ToString(), AttackTextPos, Color.White);
            spriteBatch.DrawString(Asset.mediumFont, 
                WrapText(Asset.mediumFont,descriptionText[robotNumber], 400), descriptionTextPos, Color.White);
            spriteBatch.DrawString(Asset.largeFont, RecycleScene.robotLevel[robotNumber].ToString(), levelTextPos, Color.White);

            if (descriptionBoxTriggerID >= 0)
            {
                Point descriptionPos = MouseHelper.MousePosition(cur);
                Vector2 descPos = new Vector2(descriptionPos.X - 832 * scale, descriptionPos.Y - 558 * scale);
                if (descriptionBoxTriggerID == 0)
                    descPos.Y -= 50;
                else if (descriptionBoxTriggerID == 3)
                    descPos.Y += 130;
                else
                    descPos.Y += 50;
                animation = animationList[robotNumber, descriptionBoxTriggerID];
                if (!Animation.ReferenceEquals(animation, Asset.gestureNULL)) 
                {
                    animationPlayer.PlayAnimation(animation);
                    animationPlayer.Scale = 0.655f;
                    animationPlayer.OrderLayer = 0.0001f;
                    animationPlayer.color = Color.White;

                    formPlayer.PlayAnimation(robotIdles[robotNumber,evolutionLevel[robotNumber],descriptionBoxTriggerID]);
                    formPlayer.Scale = 0.4f / robotIdles[robotNumber, evolutionLevel[robotNumber],descriptionBoxTriggerID].FrameHeight * 
                        robotIdles[robotNumber, originAnimation[robotNumber], descriptionBoxTriggerID].FrameHeight;
                    formPlayer.OrderLayer = 0.001f;
                    formPlayer.color = Color.White;

                    areaPlayer.PlayAnimation(areaForm[robotNumber,descriptionBoxTriggerID]);
                    areaPlayer.Scale = 0.2f;
                    areaPlayer.OrderLayer = 0.01f;
                    areaPlayer.color = Color.White;

                    animationPlayer.Draw(gametime, spriteBatch, descPos + gestureOffset[robotNumber,descriptionBoxTriggerID]);
                    
                    if (robotNumber == 4 && evolutionLevel[robotNumber] == 2)
                    {
                        if (descriptionBoxTriggerID == 2)
                            areaPlayer.Scale -= 0.02f;
                        formPlayer.Scale += 0.03f;
                        formPlayer.Draw(gametime, spriteBatch, descPos + formOffset[robotNumber, descriptionBoxTriggerID] + new Vector2(0,-15));
                    }
                    else if (robotNumber == 4 && evolutionLevel[robotNumber] == 0) 
                    {
                        formPlayer.Draw(gametime, spriteBatch, descPos + formOffset[robotNumber, descriptionBoxTriggerID] + new Vector2(-7, -5));
                    }
                    else if (robotNumber == 2 && evolutionLevel[robotNumber] == 0) 
                    {
                        formPlayer.Draw(gametime, spriteBatch, descPos + formOffset[robotNumber, descriptionBoxTriggerID] + new Vector2(5, 0));
                    }
                    else if (robotNumber == 0 && evolutionLevel[robotNumber] == 2 && descriptionBoxTriggerID == 3) 
                    {
                        formPlayer.Draw(gametime, spriteBatch, descPos + formOffset[robotNumber, descriptionBoxTriggerID] + new Vector2(-15, 10));
                    }
                    else if (robotNumber == 0 && evolutionLevel[robotNumber] == 1) 
                    {
                        formPlayer.Draw(gametime, spriteBatch, descPos + formOffset[robotNumber, descriptionBoxTriggerID] + new Vector2(-7, 7));
                    }
                    else
                    {
                        formPlayer.Draw(gametime, spriteBatch, descPos + formOffset[robotNumber, descriptionBoxTriggerID]);
                    }
                    areaPlayer.Draw(gametime, spriteBatch, descPos + areaOffset[robotNumber, descriptionBoxTriggerID]);
                }
                // 832 - 558 la width va height cua anh goc
                spriteBatch.Draw(descriptionBox[robotNumber, descriptionBoxTriggerID], descPos, new Rectangle(0,0,832,558), Color.White, 0, Vector2.Zero, scale, SpriteEffects.None, 0.1f);
                
            }
        }
    }
}