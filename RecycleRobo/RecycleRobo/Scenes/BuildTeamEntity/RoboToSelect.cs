﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace RecycleRobo
{
    public class RoboToSelect
    {
        public String Name;
        public float scale;
        public float scaleHover;
        public float staticScale;
        public int level;
        public Texture2D Texture;
        public Vector2 center;
        public Texture2D empty;
        public Vector2 emptyCenter;
        public Vector2 CurrentPos;
        public Vector2 OriginalPos;
        public bool IsChoose;
        public Slot slot;
        public Animation idleAnimation;
        public AnimationPlayer animationPlayer;
        public AssistantToSelect assistantEquiped;
        public Player correspondingRobot;

        public RoboToSelect(Texture2D texture, String name, Vector2 originPos, Animation idleAnimation, int level)
        {
            Texture = texture;
            Name = name;
            OriginalPos = originPos;
            CurrentPos = originPos;
            IsChoose = false;
            slot = null;
            assistantEquiped = null;
            this.correspondingRobot = null;
            staticScale = scale = 0.625f;
            scaleHover = 0.7f;
            center = new Vector2(texture.Width / 2, texture.Height / 2);
            empty = Asset.emptyPosRobo;
            emptyCenter = new Vector2(empty.Width / 2, empty.Height / 2);
            this.idleAnimation = idleAnimation;
            animationPlayer.Scale = staticScale;
            animationPlayer.color = Color.White;
            animationPlayer.OrderLayer = 0.96f;
            this.level = level;
        }

        public RoboToSelect(RoboToSelect clone)
        {
            this.Texture = clone.Texture;
            this.Name = clone.Name;
            this.OriginalPos = clone.OriginalPos;
            this.CurrentPos = clone.CurrentPos;
            this.IsChoose = false;
            this.slot = clone.slot;
            assistantEquiped = clone.assistantEquiped;
            staticScale = scale = 0.625f;
            scaleHover = 0.7f;
            this.center = clone.center;
            empty = clone.empty;
            emptyCenter = clone.emptyCenter;
            this.idleAnimation = clone.idleAnimation;
            this.animationPlayer = clone.animationPlayer;
            animationPlayer.Scale = 0.625f;
            animationPlayer.color = Color.White;
            animationPlayer.OrderLayer = 0.96f;
            level = clone.level;
        }

        public bool isHover(Point mousePoint)
        {
            if (getRectangle().Contains(mousePoint) && !IsChoose) return true;
            return false;
        }
        public Rectangle getRectangle()
        {
            return new Rectangle((int)(CurrentPos.X - Texture.Width*staticScale/2), (int)(CurrentPos.Y-  Texture.Height*staticScale/2),
                (int)(Texture.Width * staticScale), (int)(Texture.Height * staticScale));
        }

        public void drawIdleAnimation(GameTime gameTime, SpriteBatch spriteBatch, Vector2 pos)
        {
            animationPlayer.PlayAnimation(idleAnimation);
            animationPlayer.Draw(gameTime, spriteBatch, pos);
        }
    }

}
