using System;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using LoadData;
using System.Collections;

namespace RecycleRobo.Scenes
{
    public class RecycleScene
    {
        public int resoure;
        private static float scale = 0.625f;
        private Button backButton;
        private GameManager game;
        private AssistantView assistantView;
        private CyclebotView cyclebotView;
        private Texture2D taskbar;
        private bool assistantOrCyclebot;
        private MouseState cur, pre;

        public RoboAvailable robotAvailable;
        public AssistantStatus[] assistantStatus;
        private bool isSaving;
        private Data loadData;
        public float[] baseHealth;
        public float[] baseDamage;
        public float[] baseAttackIncrease;
        public float[] baseHealthIncrease;
        public static int[] robotLevel;

        private static Rectangle recycleBotRec = new Rectangle(690, 30, 155, 40);
        private static Rectangle AssistantRec = new Rectangle(460, 30, 155, 40);

        public RecycleScene(GameManager game)
        {
            this.game = game;
            load();
            assistantView = new AssistantView(this);
            cyclebotView = null;
            Vector2 backButtonPos;
            backButtonPos = new Vector2(GlobalClass.ScreenWidth - Asset.ChallengeBacknormal.Width * scale / 2, Asset.ChallengeBacknormal.Height * scale / 2);
            backButton = new Button(Asset.ChallengeBacknormal, Asset.ChallengeBackHover, Asset.ChallengeBacknormal, scale, 0.0f, backButtonPos);
            backButton.Clicked += new EventHandler(backButton_Clicked);
            taskbar = Asset.taskBarAssistant;
            assistantOrCyclebot = true;
            isSaving = false;
        }

        public void backButton_Clicked(object o, EventArgs e)
        {
           // Console.WriteLine("click");
            if (!GlobalClass.isInMap)
                game.StartMenuScene();
            else
                game.StartMap();

        }

        public void save() 
        {
            if (isSaving) return;
            isSaving = true;
            loadData = new Data();
            UnifiedRes res = new UnifiedRes(resoure);
            loadData.Save(res,"Resource/UnifiedRes.xml");
            isSaving = false;
            loadData.Save(assistantStatus,"Assistant/AssistantStatus.xml");
        }

        public void load() 
        {

            baseHealth = new float[5];
            baseDamage = new float[5];
            baseAttackIncrease = new float[5];
            baseHealthIncrease = new float[5];
            robotLevel = new int[5];

            loadData = new Data();

            NumberOneBase dataBase1 = game.Content.Load<NumberOneBase>("Data\\Player\\NumberOne");
            NumberTwoBase dataBase2 = game.Content.Load<NumberTwoBase>("Data\\Player\\NumberTwo");
            NumberThreeBase dataBase3 = game.Content.Load<NumberThreeBase>("Data\\Player\\NumberThree");
            NumberFourBase dataBase4 = game.Content.Load<NumberFourBase>("Data\\Player\\NumberFour");
            NumberFiveBase dataBase5 = game.Content.Load<NumberFiveBase>("Data\\Player\\NumberFive");

            NumberOneUpgrade att1 = loadData.Load<NumberOneUpgrade>("Player/NumberOne.xml");
            NumberTwoUpgrade att2 = loadData.Load<NumberTwoUpgrade>("Player/NumberTwo.xml");
            NumberThreeUpgrade att3 = loadData.Load<NumberThreeUpgrade>("Player/NumberThree.xml");
            NumberFourUpgrade att4 = loadData.Load<NumberFourUpgrade>("Player/NumberFour.xml");
            NumberFiveUpgrade att5 = loadData.Load<NumberFiveUpgrade>("Player/NumberFive.xml");
            UnifiedRes res = loadData.Load<UnifiedRes>("Resource/UnifiedRes.xml");

            resoure = res.available;
            assistantStatus = loadData.Load<AssistantStatus[]>("Assistant/AssistantStatus.xml");
            robotAvailable = loadData.Load<RoboAvailable>("Player/RoboAvailable.xml");

            baseHealth[0] = dataBase1.health;
            baseHealth[1] = dataBase2.health;
            baseHealth[2] = dataBase3.health;
            baseHealth[3] = dataBase4.health;
            baseHealth[4] = dataBase5.health;

            baseDamage[0] = dataBase1.damage;
            baseDamage[1] = dataBase2.damage;
            baseDamage[2] = dataBase3.damage;
            baseDamage[3] = dataBase4.healAmount;
            baseDamage[4] = dataBase5.damage;

            baseHealthIncrease[0] = dataBase1.coefficientHealth;
            baseHealthIncrease[1] = dataBase2.coefficientHealth;
            baseHealthIncrease[2] = dataBase3.coefficientHealth;
            baseHealthIncrease[3] = dataBase4.coefficientHealth;
            baseHealthIncrease[4] = dataBase5.coefficientHealth;

            baseAttackIncrease[0] = dataBase1.coefficientDamage;
            baseAttackIncrease[1] = dataBase2.coefficientDamage;
            baseAttackIncrease[2] = dataBase3.coefficientDamage;
            baseAttackIncrease[3] = dataBase4.coefficientHealAmount;
            baseAttackIncrease[4] = dataBase5.coefficientDamage;

            robotLevel[0] = att1.level;
            robotLevel[1] = att2.level;
            robotLevel[2] = att3.level;
            robotLevel[3] = att4.level;
            robotLevel[4] = att5.level;
        }

        public void Update(GameTime gameTime)
        {
            cur = Mouse.GetState();
            Point mousePoint = new Point(-20,-20);
            if (cur.LeftButton == ButtonState.Pressed)
            {
                mousePoint = new Point(MouseHelper.MousePosition(cur).X, MouseHelper.MousePosition(cur).Y);
            }
            if (recycleBotRec.Contains(mousePoint))
            {
                taskbar = Asset.taskBarCyclebot;
                assistantOrCyclebot = !assistantOrCyclebot;
                cyclebotView = new CyclebotView(this);
                assistantView = null;
            }
            else if (AssistantRec.Contains(mousePoint)) 
            {
                taskbar = Asset.taskBarAssistant;
                assistantOrCyclebot = !assistantOrCyclebot;
                assistantView = new AssistantView(this);
                cyclebotView = null;
            }
            if(assistantView != null) assistantView.Update(gameTime);
            if(cyclebotView != null) cyclebotView.Update(gameTime);
            backButton.Update(gameTime);
            pre = cur;
        }
        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (assistantView != null)
                assistantView.Draw(gameTime, spriteBatch);
            if(cyclebotView != null)
                cyclebotView.Draw(gameTime, spriteBatch);
            spriteBatch.Draw(Asset.challengeBackground, Vector2.Zero, null, Color.White, 0f, Vector2.Zero, scale ,SpriteEffects.None, 0.999f);
            spriteBatch.Draw(taskbar, Vector2.Zero, null, Color.White, 0f, Vector2.Zero, scale, SpriteEffects.None, 0.7f);
            backButton.Draw(spriteBatch);
        }
    }
}


