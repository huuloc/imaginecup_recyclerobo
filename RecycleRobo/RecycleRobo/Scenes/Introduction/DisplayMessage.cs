using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace RecycleRobo.Scenes
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class DisplayMessage
    {
        private string message;
        private TimeSpan displayTime;
        private int currentIndex;
        public Vector2 position;
        private string drawnMessage;
        private Color drawColor;
        private SoundEffect type;
        private int timePlay = 0;
        public DisplayMessage(GameManager game, string message, TimeSpan displayTime, int currentIndex, Vector2 position, Color drawColor)
        {
            this.message = message;
            this.displayTime = displayTime;
            this.currentIndex = currentIndex;
            this.position = position;
            this.drawnMessage = string.Empty;
            this.drawColor = drawColor;
            type = game.Content.Load<SoundEffect>("sounds\\keyboardpress");
        }
        public void Update(GameTime gameTime)
        {
            timePlay += gameTime.ElapsedGameTime.Milliseconds/16;
            if (this.currentIndex < this.message.Length && timePlay%10 == 9)
            {
                type.Play();

            }
        }
        public void Draw(SpriteBatch spriteBatch, SpriteFont font)
        {
            if (this.currentIndex < this.message.Length && timePlay % 2 == 0)
            {

                this.drawnMessage += this.message[this.currentIndex++].ToString();

            }
            spriteBatch.DrawString(font, this.drawnMessage, this.position, this.drawColor);
        }
    }
}
