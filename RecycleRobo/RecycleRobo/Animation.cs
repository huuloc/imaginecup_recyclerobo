﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.GamerServices;

namespace RecycleRobo
{
    public class Animation
    {
        Texture2D texture;
        public Texture2D Texture
        {
            get { return texture; }
        }

        float frametime;
        public float FrameTime
        {
            get { return frametime; }
        }

        int framecount;
        public int FrameCount
        {
            get { return framecount; }
        }

        int frameWidth;
        public int FrameWidth
        {
            get { return frameWidth; }
        }

        int frameHeight;
        public int FrameHeight
        {
            get { return frameHeight; }
        }

        public int frameWidthDXT;
        public int frameHeightDXT;

        bool isLooping;
        public bool IsLooping
        {
            get { return isLooping; }
        }

        Rectangle[] listFrame;
        public Rectangle[] ListFrame
        {
            get { return listFrame; }
        }

        public Animation(Texture2D pTexture, int rows, int cols, float pFrametime, bool pIsLooping)
        {
            this.texture = pTexture;
            this.frametime = pFrametime;
            this.isLooping = pIsLooping;
            this.framecount = rows * cols;

            listFrame = new Rectangle[framecount];

            frameWidthDXT = frameWidth = texture.Width / cols;
            frameHeightDXT = frameHeight = texture.Height / rows;

            int x = 0; int y = 0;
            for (int i = 0; i < framecount; i++ )
            {
                if (x % cols == 0 && x!=0)
                {
                    x = 0;
                    y++;
                }
                listFrame[i] = new Rectangle(x * frameWidth, y * frameHeight, frameWidth, frameHeight);
                x++;
            }
        }


        public Animation(Texture2D pTexture, int width, int height, int rows, int cols, float pFrametime, bool pIsLooping)
        {
            this.texture = pTexture;
            this.frametime = pFrametime;
            this.isLooping = pIsLooping;
            this.framecount = rows * cols;

            listFrame = new Rectangle[framecount];

            frameWidth =  frameWidthDXT = width / cols;
            frameHeight = frameHeightDXT = height / rows;

            int x = 0; int y = 0;
            for (int i = 0; i < framecount; i++)
            {
                if (x % cols == 0 && x != 0)
                {
                    x = 0;
                    y++;
                }
                listFrame[i] = new Rectangle(x * frameWidth, y * frameHeight, frameWidth, frameHeight);
                x++;
            }
        }
    }
}
