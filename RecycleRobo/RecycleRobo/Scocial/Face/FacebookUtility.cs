﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Facebook;
using System.Windows.Forms;
using System.Dynamic;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices;
using System.Net;
using System.IO;

namespace RecycleRobo
{
    static public class MessageBox
    {
        static public DialogResult Show(string message)
        {
            return Show(message, string.Empty, MessageBoxButtons.OK);
        }

        static public DialogResult Show(string message, string title)
        {
            return Show(message, title, MessageBoxButtons.OK);
        }
        static public DialogResult Show(string message, string title,
            MessageBoxButtons buttons)
        {
            // Create a host form that is a TopMost window which will be the 
            // parent of the MessageBox.
            Form topmostForm = new Form();
            // We do not want anyone to see this window so position it off the 
            // visible screen and make it as small as possible
            topmostForm.Activate();
            topmostForm.Size = SystemInformation.PrimaryMonitorSize;
            topmostForm.StartPosition = FormStartPosition.Manual;
            System.Drawing.Rectangle rect = SystemInformation.VirtualScreen;
            topmostForm.Location = new System.Drawing.Point(rect.Bottom + 10,
                rect.Right + 10);
            topmostForm.Show();
            // Make this form the active form and make it TopMost
            topmostForm.Focus();
            topmostForm.BringToFront();
            topmostForm.TopMost = true;
            // Finally show the MessageBox with the form just created as its owner
            DialogResult result = System.Windows.Forms.MessageBox.Show(topmostForm, message, title,
                buttons);
            topmostForm.Dispose(); // clean it up all the way

            return result;
        }
    }


    class FacebookUtility
    {
        private const string AppId = "464598180299936";
        private const string AppSecret = "4e83b5251098211af91af69e6853d535";
        private const string ExtendedPermissions = "user_about_me,read_stream,publish_stream,publish_actions";
        //Just for Test;
        public static string _accessToken;
        public static bool Login()
        {

            if (IsNetworkAvailable())
            {
                FacebookLoginDialog fbLoginDialog = new FacebookLoginDialog(AppId,AppSecret, ExtendedPermissions);
                fbLoginDialog.FormBorderStyle = FormBorderStyle.FixedToolWindow;
                fbLoginDialog.TopMost = true;
                fbLoginDialog.ShowDialog();
                if (fbLoginDialog.FacebookOAuthResult != null)
                {
                    if (fbLoginDialog.FacebookOAuthResult.IsSuccess)
                    {
                        _accessToken = fbLoginDialog.FacebookOAuthResult.AccessToken;
                        new Data().Save(_accessToken, "Social\\Facebook.xml");

                        return true;
                    }
                }
            }
            return false;
        }
        public static dynamic GetInfo()
        {
            _accessToken = new Data().Load<string>("Social//Facebook.xml");
            var fb = new FacebookClient(_accessToken);
            return fb.Get("/me");
        }
        public static bool PostPhoto(string message, string urlPhoto)
        {
            _accessToken = new Data().Load<string>("Social//Facebook.xml");
            if (_accessToken != null)
            {
                var fb = new FacebookClient(_accessToken);
                try
                {
                    FileStream imgstream = File.OpenRead(urlPhoto);
                    FacebookMediaStream image = new FacebookMediaStream
                        {
                            ContentType = "image/jpg",
                            FileName = Path.GetFileName(urlPhoto)
                        }.SetValue(imgstream);
                    dynamic parameters = new ExpandoObject();
                    parameters.message = message;
                    parameters.file = image;
                    dynamic res = fb.Post("/me/photos", parameters);
                    return true;
                }
                catch (FacebookOAuthException)
                {
                    return false;
                }
            }
            return false;
        }
        public static void Logout()
        {
            var webBrowser = new WebBrowser();
            var fb = new FacebookClient();
            _accessToken = new Data().Load<string>("Social//Facebook.xml");
            var logouUrl = fb.GetLogoutUrl(new { access_token = _accessToken, next = "https://www.facebook.com/connect/login_success.html" });
            webBrowser.Navigate(logouUrl);
        }
        public static bool IsNetworkAvailable()
        {
            try
            {
                using (var client = new WebClient())
                using (var stream = client.OpenRead("http://www.google.com"))
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }
    }

}
