using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using RecycleRobo.Scenes;


namespace RecycleRobo
{

    public class Assistant
    {
        public Texture2D texture;
        public Texture2D textureAtBuildTeam;
        public string type;
        public int defenceBonus;
        public int speedBonus;
        public int damageBonus;
        public int healBonus;
        private SpriteEffects effect;
        private float scale = 0.625f;
        private Vector2 position;

        public bool isShow = true;

        public Assistant(String type, int defence, int speed, int damage, int heal)
        {
            this.type = type;
            this.defenceBonus = defence;
            this.speedBonus = speed;
            this.damageBonus = damage;
            this.healBonus = heal;

            if (type == "megadroid")
            {
                texture = Asset.Ass_megadroid;
                textureAtBuildTeam = Asset.megadroidCard;
            }
            else if (type == "gigadroid")
            {
                texture = Asset.Ass_gigadroid;
                textureAtBuildTeam = Asset.gigadroidCard;
            }
            else if (type == "defentant")
            {
                texture = Asset.Ass_defentant;
                textureAtBuildTeam = Asset.defentantCard;
            }
            else if (type == "steelKnight")
            {
                texture = Asset.Ass_steelKnight;
                textureAtBuildTeam = Asset.steelKnightCard;
            }
            else if (type == "thunderbolt1")
            {
                texture = Asset.Ass_thunderbolt1;
                textureAtBuildTeam = Asset.thunderbolt1Card;
            }
            else
            {
                texture = Asset.Ass_thunderbolt2;
                textureAtBuildTeam = Asset.thunderbolt2Card;
            }
        }

        public void Update(GameTime gameTime, Player player)
        {
            effect = player.sprite.SpriteEffect;
            if (player.sprite.SpriteEffect == SpriteEffects.FlipHorizontally)
            {
                position = player.Position + new Vector2(80, -5);
            } else {
                position = player.Position + new Vector2(-20, -5);
            }
            if (!player.isAlive || !player.isControl || !player.isVisible)
            {
                isShow = false;
            }
            else
            {
                isShow = true;
            }
            const float BounceHeight = 0.18f;
            const float BounceRate = 3.0f;
            const float BounceSync = -0.75f;
            
            double t = gameTime.TotalGameTime.TotalSeconds * BounceRate + position.X * BounceSync;
            float bounce = (float)Math.Sin(t) * BounceHeight * texture.Height * scale;
            if (!player.isWalking)
            {
                position += new Vector2(0.0f, bounce);
            }

        }
        public void Draw(SpriteBatch spriteBatch)
        {
            if (isShow)
            {
                spriteBatch.Draw(texture, position, null, Color.White, 0.1f, Vector2.Zero, scale, effect, 0.85f);
            }

        }

    }
}
