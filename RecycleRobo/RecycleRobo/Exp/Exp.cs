using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using RecycleRobo.Scenes;


namespace RecycleRobo
{
	/// <summary>
	/// This is a game component that implements IUpdateable.
	/// </summary>
	public class Exp
	{
		const float CO_BASE = 0.5f;
		Dictionary<string, int[]> ExpUpgrade = new Dictionary<string, int[]>();
		Data loadData = new Data();
		public Dictionary<string, PlayerAtt> dictPlayer;
		Dictionary<string, float> coefficient;
		int totalLevel = 0;

		int totalExp = 200;                                    
        public static int[] numberOne = { -1, -1, -1, -1, 0, 60, 100, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240};
        public static int[] numberTwo = { -1, -1, -1, -1, 0, 60, 100, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240 };
        public static int[] numberThree = { -1, -1, -1, -1, -1, -1, 0, 100, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230};
        public static int[] numberFour = { -1, -1, -1, -1, -1, -1, 0, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 250 };
        public static int[] numberFive = {-1, -1, -1, -1, -1, -1, -1, -1, -1, 0, 160, 170, 180, 190, 200, 210, 220, 230, 240, 250 };
        
		public Exp(Player[] players)
		{
			Init();
			dictPlayer = new Dictionary<string, PlayerAtt>();
			dictPlayer = Load(players);
            coefficient = new Dictionary<string, float>();
        }
        public static void InitExpNumber()
        {
            int baseExp = 15;
            for (int i = 5; i < 20; i++)
            {
                numberOne[i] = Convert.ToInt16(baseExp * Math.Pow(1.45, i));
            }
            for (int i = 5; i < 20; i++)
            {
                numberTwo[i] = Convert.ToInt16(baseExp * Math.Pow(1.45, i));
            }
            for (int i = 7; i < 20; i++)
            {
                numberThree[i] = Convert.ToInt16(baseExp * Math.Pow(1.45, i));
            }
            for (int i = 7; i < 20; i++)
            {
                numberFour[i] = Convert.ToInt16(baseExp * Math.Pow(1.45, i));
            }
            for (int i = 10; i < 20; i++)
            {
                numberFive[i] = Convert.ToInt16(baseExp * Math.Pow(1.45, i));
            }
        }
		public void Calcualate()
		{
            //foreach (string playerName in dictPlayer.Keys)
            //{
            //    dictPlayer[playerName].level = 20;
            //}
            totalExp = Convert.ToInt16(200 * Math.Pow(1.5, GlobalClass.curLevelPlaying - 1));

            foreach (string playerName in dictPlayer.Keys)
            {
                dictPlayer[playerName].exp += Convert.ToInt16(totalExp / dictPlayer.Count);
                for (int j = 0; j < ExpUpgrade[playerName].Length; j++)
                {
                    if (dictPlayer[playerName].exp >= ExpUpgrade[playerName][j] && j == dictPlayer[playerName].level)
                    {
                        dictPlayer[playerName].exp -= ExpUpgrade[playerName][j];
                        dictPlayer[playerName].level += 1;
                    }
                }
            }
		}
		public void Init()
		{
			ExpUpgrade.Add("NumberOne", numberOne);
			ExpUpgrade.Add("NumberTwo", numberTwo);
			ExpUpgrade.Add("NumberThree", numberThree);
			ExpUpgrade.Add("NumberFour", numberFour);
			ExpUpgrade.Add("NumberFive", numberFive);
		}
		private Dictionary<string, PlayerAtt> Load(Player[] players)
		{
			Data loadData = new Data();
			Dictionary<string, PlayerAtt> dictPlayers = new Dictionary<string, PlayerAtt>();
			Dictionary<string, dynamic> mapPlayer = new Dictionary<string, dynamic>();

			mapPlayer.Add("NumberOne", loadData.Load<NumberOneUpgrade>("Player\\NumberOne.xml"));
			mapPlayer.Add("NumberTwo", loadData.Load<NumberTwoUpgrade>("Player\\NumberTwo.xml"));
			mapPlayer.Add("NumberThree", loadData.Load<NumberThreeUpgrade>("Player\\NumberThree.xml"));
			mapPlayer.Add("NumberFour", loadData.Load<NumberFourUpgrade>("Player\\NumberFour.xml"));
			mapPlayer.Add("NumberFive", loadData.Load<NumberFiveUpgrade>("Player\\NumberFive.xml"));

			foreach (Player player in players)
			{
				if (player.isAlive)
				{
                    dictPlayers.Add(player.GetType().Name.Replace("Upgrade", String.Empty), mapPlayer[player.GetType().Name.Replace("Upgrade", String.Empty)]);
					totalLevel += mapPlayer[player.GetType().Name].level;
				}
			}
			return dictPlayers;
		}
		public void Save()
		{
			foreach (PlayerAtt player in dictPlayer.Values)
			{
				loadData.Save(player, "Player\\" + player.GetType().Name.Remove(player.GetType().Name.Length - 7,7) + ".xml");
			}
		}
	}
}
