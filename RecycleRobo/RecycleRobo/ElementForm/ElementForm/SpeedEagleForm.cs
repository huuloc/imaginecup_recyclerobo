﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;


namespace RecycleRobo
{
    public class SpeedEagleForm : ElementForm
    {

        public SpeedEagleForm(int[] bonus)
            : base(bonus)
        {
            areaAnimation = GlobalClass.getAnimationByName("NUM2_AREA_1");
        }

        public SpeedEagleForm(ContentManager Content, Vector2 deltaPos, int[] bonus)
            : base(Content, deltaPos, bonus)
        {
            areaAnimation = GlobalClass.getAnimationByName("NUM2_AREA_1");
            iconTile = new IconTile(Asset.speedTile, deltaPos, 800);
        }

        public override Animation getAnimationForm(String name)
        {
            return GlobalClass.getAnimationByName(name);
        }
    }
}
