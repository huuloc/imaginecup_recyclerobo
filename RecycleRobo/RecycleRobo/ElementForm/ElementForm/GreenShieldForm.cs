﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace RecycleRobo
{
    public class GreenShieldForm : ElementForm
    {
        public GreenShieldForm(int[] bonus)
            : base(bonus)
        {
            areaAnimation = GlobalClass.getAnimationByName("NUM5_AREA_1");
            deltaAreaPos = new Vector2(-5, -15);
        }

        public GreenShieldForm(ContentManager Content, Vector2 deltaPos, int[] bonus)
            : base(Content, deltaPos, bonus)
        {
            deltaAreaPos = new Vector2(-5, -15);
            iconTile = new IconTile(Asset.shieldTile, deltaPos, 600);
            areaAnimation = GlobalClass.getAnimationByName("NUM5_AREA_1");
        }

        public override Animation getAnimationForm(String name)
        {
            return GlobalClass.getAnimationByName(name);
        }
    }
}
