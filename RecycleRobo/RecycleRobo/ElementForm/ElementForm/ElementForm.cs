﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;


namespace RecycleRobo
{
    public abstract class ElementForm
    {
        public IconTile iconTile;
        public Vector2 Position;
        public Vector2 deltaAreaPos = Vector2.Zero;
        /// <summary>
        /// 0: defence
        /// 1: speed
        /// 2: attack or heal
        /// </summary> 
        public int[] bonus = new int[3];
        public bool isHover = false;
        public bool isGestureType;

        public Animation areaAnimation;
        public MouseState curState, prevState;

        public Animation gestureAnimation = null;
        public AnimationPlayer gestureAnimationPlayer;
        public SoundEffect sound;

        public ElementForm(int[] bonus)
        {
            this.bonus = bonus;
            Position = new Vector2(70, 10);
        }

        public ElementForm(ContentManager Content, Vector2 position, int[] bonus)
        {
            this.isGestureType = false;
            this.Position = position;
            this.bonus = bonus;
            gestureAnimationPlayer.PlayAnimation(gestureAnimation);
            gestureAnimationPlayer.color = Color.White;
            gestureAnimationPlayer.Scale = 0.625f;
            gestureAnimationPlayer.OrderLayer = 0.12f;
            gestureAnimationPlayer.SpriteEffect = SpriteEffects.None;
        }

        public void setHover( Point mousePoint)
        {
            if (iconTile.isHover(mousePoint))
                isHover = true;
            else
                isHover = false;
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            iconTile.Draw(spriteBatch);
        }

        public virtual void drawGestureAnimation(SpriteBatch spriteBatch, GameTime gameTime)
        {
            gestureAnimationPlayer.PlayAnimation(gestureAnimation);
        }

        public void updateTile(GameTime gameTime)
        {
            iconTile.Update(gameTime);
        }

        public virtual void Draw(SpriteBatch spriteBatch, GameTime gametime) { }
        public virtual void reset() { }
        public virtual void Update(Player robo, GameTime gameTime) { }
        public virtual void Update(Player robot, GameTime gameTime, Monster[] monsters) { }
        public virtual void Update(Player robot, GameTime gameTime, Player[] players) { }

        public abstract Animation getAnimationForm(String name);
    }
}
