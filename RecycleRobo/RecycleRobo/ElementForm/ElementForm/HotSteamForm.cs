﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace RecycleRobo
{
    public class HotSteamForm : ElementForm
    {
        private int timeDrag;
        private int time;
        private const float effectRadius = 100.0f;
        private const float dragTimethreshold = 200.0f;
        private const float triggerLength = 150.0f;
        private Vector2 beginPos;
        private Vector2[] offset;
        public HotSteamForm(ContentManager Content, Vector2 deltaPos, int[] bonus)
            : base(Content, deltaPos, bonus)
        {
            iconTile = new IconTile(Asset.steamTile, deltaPos, 700);
            gestureAnimation = Asset.gestureVerticle;
            isGestureType = true;
            areaAnimation = GlobalClass.getAnimationByName("NUM5_AREA_4");
            deltaAreaPos = new Vector2(-5, -40);
            offset = new Vector2[8];
            offset[0] = new Vector2(0,-triggerLength);
            for (int i = 1; i < offset.Length; i++) 
            {
                offset[i] = Vector2.Transform(offset[i-1], Matrix.CreateRotationZ((float)Math.PI / 4));
            }
            sound = Content.Load<SoundEffect>("sounds/elementForm/Fire_Form");
        }

        public override void drawGestureAnimation(SpriteBatch spriteBatch, GameTime gameTime)
        {
            base.drawGestureAnimation(spriteBatch, gameTime);
            gestureAnimationPlayer.Draw(gameTime, spriteBatch, Position + new Vector2(110, 27));
        }

        public override Animation getAnimationForm(String name)
        {
            String key = name+"_STEAM";
            return GlobalClass.getAnimationByName(key);
        }

        public override void Update(Player robot, GameTime gameTime, Monster[] monsters)
        {
            if (!robot.isSelected)
            {
                this.time = 0;
                this.timeDrag = 0;
                return;
            }
            List<Monster> TMPmonsters = new List<Monster>();
            foreach (Monster monster in monsters)
            {
                if (monster.isAlive && monster.isAttackable)
                    TMPmonsters.Add(monster);
            }
            monsters = TMPmonsters.ToArray();
            timeDrag += gameTime.ElapsedGameTime.Milliseconds;
            time += gameTime.ElapsedGameTime.Milliseconds;
            Random r = new Random();

            curState = Mouse.GetState();
            Point mousePoint = new Point(MouseHelper.MousePosition(curState).X, MouseHelper.MousePosition(curState).Y);
            Vector2 pos = new Vector2(mousePoint.X, mousePoint.Y);
            Vector2 prepos = new Vector2(MouseHelper.MousePosition(prevState).X, MouseHelper.MousePosition(prevState).Y);;

            if (prevState.LeftButton == ButtonState.Released && curState.LeftButton == ButtonState.Pressed)
                beginPos = new Vector2(curState.X, curState.Y);

            if (curState.LeftButton == ButtonState.Released) timeDrag = 0;
            else timeDrag += gameTime.ElapsedGameTime.Milliseconds;

            bool holding = curState.LeftButton == ButtonState.Pressed
                        && prevState.LeftButton == ButtonState.Pressed
                        && timeDrag > dragTimethreshold
                        && Vector2.Distance(beginPos,robot.Center) < 90.0f
                        && pos.Y < prepos.Y
                        && Math.Abs(pos.Y - prepos.Y) > Math.Abs(pos.X - prepos.X);
            Vector2 spread1 = new Vector2(-3, 0);
            Vector2 spread2 = new Vector2(3, 0);
            Vector2 spread3 = new Vector2(0, 3);
            Vector2 spread4 = new Vector2(3, 0);
            if (holding)
            {
                if (Vector2.Distance(beginPos, pos) > triggerLength)
                {
                    sound.Play();
                    beginPos = pos;
                    for (int i = 0; i < offset.Length; i++)
                    {
                        Scenes.GamePlayScene.sideEffect.sideEffectTrigger(robot.Center + offset[i], "hotWater");
                        foreach (Monster monster in monsters) 
                        {
                            if (Vector2.Distance(monster.Center, robot.Center + offset[i]) < effectRadius) 
                            {
                                if (monster is BossOne || monster is BossTwo)
                                    monster.curHealth = (int)(monster.curHealth * 0.95f);
                                else
                                    monster.curHealth = (int)(monster.curHealth * 0.80f);
                            }
                        }
                    }
                }
            }
            prevState = curState;
        }
    }
}
