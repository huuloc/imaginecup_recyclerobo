﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace RecycleRobo
{
    public class IceFangForm : ElementForm
    {
        int time;
        private SeriesPointRecognitor series;
        Monster[] monsters;
        // hinh cua so
        private static Vector2[] offset = { new Vector2(-50, 50), new Vector2(-50, 0), new Vector2(-50,-50),
                                          new Vector2(0, -50), new Vector2(50, -50),
                                          new Vector2(50, 0), new Vector2(50, 50)};
        public IceFangForm(ContentManager Content, Vector2 deltaPos, int[] bonus)
            : base(Content, deltaPos, bonus)
        {
            iconTile = new IconTile(Asset.iceFangTile, deltaPos,300);
            gestureAnimation = Asset.gestureU;
            time = 0;
            series = new SeriesPointRecognitor(offset, 70, 30);
            areaAnimation = GlobalClass.getAnimationByName("NUM3_AREA_3");
            deltaAreaPos = new Vector2(-15, -15);
            sound = Content.Load<SoundEffect>("sounds/elementForm/fang_form");
        }

        public override Animation getAnimationForm(String name)
        {
            String key = name+"_ICEFANG";
            return GlobalClass.getAnimationByName(key);
        }
        public override void drawGestureAnimation(SpriteBatch spriteBatch, GameTime gameTime)
        {
            base.drawGestureAnimation(spriteBatch, gameTime);
            gestureAnimationPlayer.Draw(gameTime, spriteBatch, Position + new Vector2(90, 27));
        }

        public override void Update(Player robot, GameTime gameTime, Monster[] monsters)
        {
            List<Monster> TMPmonsters = new List<Monster>();
            foreach (Monster monster in monsters)
            {
                if (monster.isAlive && monster.isAttackable)
                    TMPmonsters.Add(monster);
            }
            monsters = TMPmonsters.ToArray();
            this.monsters = monsters;
            time += gameTime.ElapsedGameTime.Milliseconds;
            const float effectRadius = 200.0f;
            const float damage = 100.0f;
            double[] d = new double[monsters.Length];
            Random  r = new Random();
            bool[] justSlowed = new bool[monsters.Length];
            for (int i = 0; i < justSlowed.Length; i++)
            {
                d[i] = r.NextDouble();
                justSlowed[i] = false;
            }
            int j = 0;
            if (series.recognize(gameTime, robot))
            {
                sound.Play();
                foreach (Monster monster in monsters)
                {
                    if (Vector2.Distance(robot.Center, monster.Center) < effectRadius)
                    {
                        monster.curHealth -= (int)damage + robot.levelOfRobot * 25;
                        if (d[j] > 0.7)
                        {
                            monster.movementEffectModifier["ice"] = 0.4f;
                            justSlowed[j] = true;
                            time = 0;
                        }
                        Scenes.GamePlayScene.sideEffect.sideEffectTrigger(monster.Center, "iceFang");
                        Scenes.GamePlayScene.sideEffect.sideEffectTrigger(monster.Center, "iceFang");
                        Scenes.GamePlayScene.sideEffect.sideEffectTrigger(monster.Center, "iceFang");
                        Scenes.GamePlayScene.sideEffect.sideEffectTrigger(monster.Center, "iceFang");
                        Scenes.GamePlayScene.sideEffect.sideEffectTrigger(monster.Center, "iceFang");
                        Scenes.GamePlayScene.sideEffect.sideEffectTrigger(monster.Center, "iceFang");
                        monster.effectToDraw += "ice";
                    }
                    j++;
                }
            }
            if (time >= 1500)
            {
                for (int k = 0; k < justSlowed.Length; k++)
                {
                    if (justSlowed[k]) continue;
                    else
                    {
                        monsters[k].movementEffectModifier["ice"] = 1.0f;
                    }
                }
                time = 0;
            }
        }

        public override void reset()
        {
            foreach (Monster monster in monsters)
                if (monster.effectToDraw.Contains("ice"))
                {
                    monster.movementEffectModifier["ice"] = 1.0f;
                }
        }
    }
}
