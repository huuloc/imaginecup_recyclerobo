﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
namespace RecycleRobo
{
    public class GreeHerbForm : ElementForm
    {
        int time;
        //chu V
        private static Vector2[] offset = { new Vector2(-40, -40), new Vector2(-20, 10),new Vector2(0, 60),
                                               new Vector2(20, 10), new Vector2(40, -40)};
        private SeriesPointRecognitor series;
        public GreeHerbForm(ContentManager Content, Vector2 deltaPos, int[] bonus)
            : base(Content, deltaPos, bonus)
        {
            iconTile = new IconTile(Asset.herbTile, deltaPos,200);
            gestureAnimation = Asset.gestureV;
            series = new SeriesPointRecognitor(offset, 50, 20);
            time = 0;
            areaAnimation = GlobalClass.getAnimationByName("NUM4_AREA_3");
            deltaAreaPos = new Vector2(-15, -10);
            sound = Content.Load<SoundEffect>("sounds/elementForm/GreeHerb");
        }

        public override Animation getAnimationForm(String name)
        {
            String key = name+"_HERB";
            return GlobalClass.getAnimationByName(key);
        }

        public override void drawGestureAnimation(SpriteBatch spriteBatch, GameTime gameTime)
        {
            base.drawGestureAnimation(spriteBatch, gameTime);
            gestureAnimationPlayer.Draw(gameTime, spriteBatch, Position + new Vector2(90, 27));
        }

        public override void Update(Player robot, GameTime gameTime, Player[] players)
        {
            List<Player> TMPplayers = new List<Player>();
            foreach (Player player in players)
            {
                if (player.isAlive)
                    TMPplayers.Add(player);
            }
            players = TMPplayers.ToArray();
            time += gameTime.ElapsedGameTime.Milliseconds;
            const float effectRadius = 250.0f;
            const int healthAmount = 200;
            if (series.recognize(gameTime, robot))
            {
                sound.Play();
                foreach (Player player in players)
                {
                    if (Vector2.Distance(robot.Center, player.Center) < effectRadius)
                    {
                        {
                            player.curHealth += healthAmount + robot.levelOfRobot * 20;
                            Scenes.GamePlayScene.sideEffect.sideEffectTrigger(player.Center, "healing");
                            Scenes.GamePlayScene.sideEffect.sideEffectTrigger(player.Center, "healing");
                            Scenes.GamePlayScene.sideEffect.sideEffectTrigger(player.Center, "healing");
                            Scenes.GamePlayScene.sideEffect.sideEffectTrigger(player.Center, "healing");
                        }
                    }
                }
            }
        }
    }
}
