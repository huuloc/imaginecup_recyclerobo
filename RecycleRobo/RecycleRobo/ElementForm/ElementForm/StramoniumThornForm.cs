﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace RecycleRobo
{
    public class StramoniumThornForm : ElementForm
    {
        private int time;
        private SeriesPointRecognitor series;
        private Animation effectAnimation;
        private Monster[] monsters;
        private static float scale = 0.55f;
        private static Vector2[] offset = { new Vector2(-50, 50), new Vector2(-50, 25), new Vector2(-50, 0),
                                              new Vector2(-50, -25), new Vector2(-50, -50),
                                              new Vector2(-25, -25), new Vector2(0, 0), 
                                              new Vector2(25, 25), new Vector2(50, 50), 
                                              new Vector2(50, 25),new Vector2(50, 0),
                                              new Vector2(50, -25), new Vector2(50, -50),};
        public StramoniumThornForm(ContentManager Content, String ofRobo, Vector2 deltaPos, int[] bonus)
            : base(Content, deltaPos, bonus)
        {
            iconTile = new IconTile(Asset.thornTile, deltaPos, 1000);
            gestureAnimation = Asset.gestureN;
            time = 0;
            series = new SeriesPointRecognitor(offset, 100, 40);
            this.effectAnimation = GlobalClass.getAnimationByName("THORN");
            monsters = new Monster[0];
            if (ofRobo == "thorn1")
            {
                areaAnimation = GlobalClass.getAnimationByName("NUM1_AREA_2");
                deltaAreaPos = new Vector2(-15, -15);
            }
            else
            {
                deltaAreaPos = new Vector2(-5, -35);
                areaAnimation = GlobalClass.getAnimationByName("NUM5_AREA_2");
            }
            isGestureType = true;
            sound = Content.Load<SoundEffect>("sounds/elementForm/Thorn_ForM");
        }

        public override Animation getAnimationForm(String name)
        {
            String key = name +"_THORN";
            return GlobalClass.getAnimationByName(key);
        }
        public override void Update(Player robot, GameTime gameTime, Monster[] monsters)
        {
            List<Monster> TMPmonsters = new List<Monster>();
            foreach (Monster monster in monsters)
            {
                if (monster.isAlive && monster.isAttackable)
                    TMPmonsters.Add(monster);
            }
            monsters = TMPmonsters.ToArray();
            time += gameTime.ElapsedGameTime.Milliseconds;
            const float effectRadius = 200.0f;
            const float damage = 250.0f;

            this.monsters = monsters;
            double[] d = new double[monsters.Length];
            Random r = new Random();
            bool[] justParalized = new bool[monsters.Length];
            bool[] dead = new bool[monsters.Length];
            for (int i = 0; i < justParalized.Length; i++)
            {
                d[i] = r.NextDouble();
                dead[i] = false;
                justParalized[i] = false;
            }

            int j = 0;
            if (series.recognize(gameTime,robot))
            {
                sound.Play();
                foreach (Monster monster in monsters)
                {
                    if (!monster.isAlive) continue;
                    monster.effect.PlayAnimation(effectAnimation);
                    monster.effect.Scale = scale;
                    monster.effect.OrderLayer = 0.0001f;
                    monster.effect.color = Color.White;
                    if (Vector2.Distance(robot.Center, monster.Center) < effectRadius)
                    {
                        if (d[j] > 0.7) 
                        {
                            monster.curHealth -= 40;
                        }
                        Scenes.GamePlayScene.sideEffect.sideEffectTrigger(monster.Center, "StramoniumThorn");
                        Scenes.GamePlayScene.sideEffect.sideEffectTrigger(monster.Center, "StramoniumThorn");
                        if (!monster.isAlive) continue;
                        justParalized[j] = true;
                        monster.curHealth -= (int)damage + robot.levelOfRobot * 20;
                        time = 0;
                        monster.effectToDraw += "thorn";
                    }
                    j++;
                }
            }
            if (time >= 1000)
            {
                for (int k = 0 ; k < justParalized.Length ; k++) 
                {
                    if (justParalized[k]) continue;
                    else
                    {
                        if (monsters[k].effectToDraw.Contains("thorn"))
                        {
                            monsters[k].effectToDraw = monsters[k].effectToDraw.Replace("thorn", "");
                        }
                    }
                }
                time = 0;
            }
        }
        public override void Draw(SpriteBatch spriteBatch, GameTime gametime)
        {
            int i = 0;
            foreach (Monster monster in monsters)
            {
                if (!monster.isAlive || !monster.isAttackable || !monster.isVisible)
                    continue;
                if (monster.effectToDraw.StartsWith("thorn"))
                {
                    Vector2 pos = new Vector2();
                    monster.effect.PlayAnimation(effectAnimation);
                    pos.X = monster.Center.X - effectAnimation.FrameWidth * scale / 2;
                    pos.Y = monster.Center.Y + monster.CurAnimation.FrameHeight * monster.sprite.Scale / 2
                        - effectAnimation.FrameHeight * scale / 2 - 50;
                    monster.effect.Draw(gametime, spriteBatch, pos);
                }
                i++;
            }
        }

        public override void drawGestureAnimation(SpriteBatch spriteBatch, GameTime gameTime)
        {
            base.drawGestureAnimation(spriteBatch, gameTime);
            //draw gesture animation on the tile.
            gestureAnimationPlayer.Draw(gameTime, spriteBatch, Position + new Vector2(90, 27));
        }
        public override void reset()
        {
            foreach(Monster monster in monsters)
                if (monster.effectToDraw.Contains("thorn"))
                {
                    monster.effectToDraw = monster.effectToDraw.Replace("thorn", "");
                }
        }
    }
}
