﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace RecycleRobo
{
    public class AngerBearForm : ElementForm
    {

        public AngerBearForm(int[] bonus)
            : base(bonus)
        {
            areaAnimation = GlobalClass.getAnimationByName("NUM3_AREA_1");
            deltaAreaPos = new Vector2(-10, 5);
        }

        public AngerBearForm(ContentManager Content, Vector2 deltaPos, int[] bonus)
            : base(Content, deltaPos, bonus)
        {
            areaAnimation = GlobalClass.getAnimationByName("NUM3_AREA_1");
            iconTile = new IconTile(Asset.angryTile, deltaPos, 250);
            deltaAreaPos = new Vector2(-10, 5);
        }
        public override Animation getAnimationForm(String name)
        {
            return GlobalClass.getAnimationByName(name);
        }
    }
}
