﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace RecycleRobo
{
    public class BalanceForm : ElementForm
    {

        public BalanceForm(int robo, int [] bonus) : base(bonus) 
        {
            if (robo == 1)
            {
                deltaAreaPos = new Vector2 (-15, 10);
                areaAnimation = GlobalClass.getAnimationByName("NUM1_AREA_1");
            }
            else areaAnimation = GlobalClass.getAnimationByName("NUM4_AREA_1");
        }

        public BalanceForm(ContentManager Content, String ofRobo, Vector2 deltaPos, int[] bonus)
            : base(Content, deltaPos, bonus)
        {
            iconTile = new IconTile(Asset.balanceTile, deltaPos,400);
            if (ofRobo == "balance1")
            {
                deltaAreaPos = new Vector2(-15, 10);
                areaAnimation = GlobalClass.getAnimationByName("NUM1_AREA_1");
            }
            else
                areaAnimation = GlobalClass.getAnimationByName("NUM4_AREA_1");
        }

        public override Animation getAnimationForm(String name)
        {
            return GlobalClass.getAnimationByName(name);
        }
    }
}
