﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace RecycleRobo
{
    public class SeriesPointRecognitor
    {
        private Vector2[] offsetPoint;
        private Vector2[] points;
        private int time;
        private int timeIlluminator;
        private MouseState curState, preState;
        private int counter;
        private int timeDrag;
        private bool[] fininished;
        private const float dragTimethreshold = 0.2f;
        private int Xtolarate;
        private int Ytolarate;

        public SeriesPointRecognitor(Vector2[] OffsetPoints, int Xtolarate, int Ytolarate) 
        {
            counter = 0;
            this.offsetPoint = OffsetPoints;
            points = new Vector2[offsetPoint.Length];
            time = 0;
            timeIlluminator = 0;
            timeDrag = 0;
            fininished = new bool[offsetPoint.Length];
            this.Xtolarate = Xtolarate;
            this.Ytolarate = Ytolarate;
            for (int i = 0; i < offsetPoint.Length; i++) 
            {
                fininished[i] = false;
            }
        }

        public bool recognize(GameTime gameTime, Player robot) 
        {
            for(int i = 0 ; i < points.Length ; i++)
            {
                this.points[i] = robot.Center + this.offsetPoint[i];
            }
            curState = Mouse.GetState();
            time += gameTime.ElapsedGameTime.Milliseconds;
            timeIlluminator += gameTime.ElapsedGameTime.Milliseconds;
            if (curState.LeftButton == ButtonState.Released) timeDrag = 0;
            else timeDrag += gameTime.ElapsedGameTime.Milliseconds;
            Point mousePoint = new Point(MouseHelper.MousePosition(curState).X, MouseHelper.MousePosition(curState).Y);
            Vector2 curPos = new Vector2(mousePoint.X, mousePoint.Y);

            bool holding = curState.LeftButton == ButtonState.Pressed
                        && preState.LeftButton == ButtonState.Pressed
                        && timeDrag > dragTimethreshold;

            if (holding) 
            {
                if (Math.Abs(curPos.X - points[counter].X) < Xtolarate && Math.Abs(curPos.Y - points[counter].Y) < Ytolarate)
                {
                    counter++;
                    time = 0;
                    //if(counter <= points.Length-2)
                        //Scenes.GamePlayScene.sideEffect.sideEffectTrigger(points[counter], "point");
                }
            }

            preState = curState;
            if (time > 2000) 
            {
                counter = 0;
                time = 0;
            }
            if (timeIlluminator >= 3000 && counter == 0 && robot.isSelected && !robot.isWalking) 
            {
                for (int i = 0; i < points.Length; i++ )
                    Scenes.GamePlayScene.sideEffect.sideEffectTrigger(points[i], "point");
                timeIlluminator = 0;
            }
            if (counter >= offsetPoint.Length ) 
            {
                time = 0;
                timeIlluminator = 0;
                counter = 0;
                return true;
            }
            return false;
        }
    }
}
