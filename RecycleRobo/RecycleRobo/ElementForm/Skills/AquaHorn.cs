﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using RecycleRobo.Scenes;
using Microsoft.Xna.Framework.Audio;

namespace RecycleRobo
{
    public class AquaHorn : Skill
    {
        public AnimationPlayer animationPlayer;
        public NumberFour num4;
        public SoundEffect soundSkill;
        private Animation animation;
        public Vector2 position;
        public bool isActive =false;

        //Quan can bang bien nay.
        public int damage = 1500;
 
        public AquaHorn(ContentManager Content)
        {
            num4 = getNumber4(GamePlayScene.player);
            //num4.isVisible = false;
            num4.isControl = false;
            isSkillComplete = false;

            if (num4.sprite.SpriteEffect==SpriteEffects.None)
                position = num4.Center + new Vector2 (-90, - 200);
            else
                position = num4.Center + new Vector2(-320, -200);

            animation = new Animation(Content.Load<Texture2D>("skills/aquahorn/num4_skill4_4x3"),4,3,0.09f,false);
            animationPlayer.PlayAnimation(animation);
            animationPlayer.color = Color.White;
            animationPlayer.OrderLayer = num4.sprite.OrderLayer;
            animationPlayer.Scale = 0.625f;
            animationPlayer.SpriteEffect = num4.sprite.SpriteEffect;
            SoundEffect.MasterVolume = GlobalClass.volumeSoundFx / 100;
        }

        public NumberFour getNumber4(Player[] player) 
        {
            NumberFour num4 = null;
            foreach (Player robo in player)
            {
                if (robo!=null && robo.GetType().Name == "NumberFour" && robo.isAlive)
                {
                    num4 = (NumberFour)robo;
                }
            }
            return num4;
        }

        public void Update(WaveManager waveManager)
        {
            SoundEffect.MasterVolume = (float)GlobalClass.volumeSoundFx / 100;

            if (animationPlayer.IsDone)
            {
                isSkillComplete = true;
                num4.isVisible = true;
                num4.isControl = true;
                return;
            }

            if (animationPlayer.FrameIndex==8 && !isActive)
            {
                isActive = true;
                foreach (Monster monster in waveManager.wave.monsters)
                {
                    if (monster != null && monster.curHealth > 0 && monster.getAttackRectangle().Intersects(bounding()))
                    {
                        monster.curHealth -= damage;
                    }
                }
            }
        }


        public Rectangle bounding()
        {
            if (num4.sprite.SpriteEffect == SpriteEffects.None)
            {
                Vector2 pos = position + new Vector2(120, 50);
                return new Rectangle((int)pos.X, (int)pos.Y, (int)(2 * animation.FrameWidth * animationPlayer.Scale / 3) - 35, 
                    (int)(animation.FrameHeight * animationPlayer.Scale) -45);
            }
            else
            {
                Vector2 pos = position + new Vector2(50, 50);
                return new Rectangle((int)pos.X, (int)pos.Y, (int)(2 * animation.FrameWidth * animationPlayer.Scale / 3) - 10,
                    (int)(animation.FrameHeight * animationPlayer.Scale) - 45);
            }
        }
        
        public void Draw(GameTime gametime, SpriteBatch spriteBatch)
        {
            animationPlayer.PlayAnimation(animation);

            //spriteBatch.Draw(Asset.healthTexture, bounding(), Color.Red);
            
            animationPlayer.Draw(gametime, spriteBatch, position);
        }

    }
}
