﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using RecycleRobo.Scenes;
using Microsoft.Xna.Framework.Graphics;

namespace RecycleRobo
{
    public class TileSkill
    {
        public IconTile tile;
        public String skillName;
        public int timeDisableSkill;
        public TileSkill(IconTile tile, String skillName, int timeDisableSkill)
        {
            this.tile = tile;
            tile.isActive = false;
            this.skillName = skillName;
            this.timeDisableSkill = timeDisableSkill;
        }
    }

    public class SkillTileManager
    {
        public List<TileSkill> tileSkills;
        public ContentManager Content;

        MouseState curState, prevState;

        public bool isActive = false;

        public SkillTileManager(ContentManager Content)
        {
            this.Content = Content;
            tileSkills = new List<TileSkill>();
        }

        public void createTileSkill(String skillName, Vector2 deltaPos)
        {
            TileSkill tileSkill = null;
            switch (skillName)
            {
                case "lightsword":
                    tileSkill = new TileSkill(new IconTile(Asset.swordTile, deltaPos, 1100), skillName, 50);
                    tileSkills.Add(tileSkill);
                    break;
                case "lightbomb":
                    tileSkill = new TileSkill(new IconTile(Asset.bombTile, deltaPos, 1100), skillName, 50);
                    tileSkills.Add(tileSkill);
                    break;
                case "gigaslash":
                    tileSkill = new TileSkill(new IconTile(Asset.slashTile, deltaPos, 1100), skillName, 2);
                    tileSkills.Add(tileSkill);
                    break;
                case "holyheal":
                    tileSkill = new TileSkill(new IconTile(Asset.healTile, deltaPos, 1100), skillName, 25);
                    tileSkills.Add(tileSkill);
                    break;
                case "aquaHorn":
                    tileSkill = new TileSkill(new IconTile(Asset.hornTile, deltaPos, 1100), skillName, 80);
                    tileSkills.Add(tileSkill);
                    break;
            }
        }

        public void Update(GameTime gameTime)
        {
            curState = Mouse.GetState();
            
            foreach (TileSkill tileskill in tileSkills)
            {
                tileskill.tile.Update(gameTime);
            }

            Point mousePoint = new Point(MouseHelper.MousePosition(curState).X, MouseHelper.MousePosition(curState).Y);

            if (curState.LeftButton == ButtonState.Released && prevState.LeftButton == ButtonState.Pressed && isActive)
            {
                foreach (TileSkill tileSkill in tileSkills)
                {
                    if (tileSkill.tile.isHover(mousePoint))
                    {
                        GamePlayScene.skillManager.isUsingSkill = true;
                        GamePlayScene.skillManager.skills.Add(initSkill(tileSkill.skillName));
                        tileSkill.tile.isEnable = false;
                        tileSkill.tile.isActive = true;
                        tileSkill.tile.timeEnableBack = tileSkill.timeDisableSkill;
                    }
                }
            }

            prevState = curState;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (TileSkill tileskill in tileSkills)
            {
                tileskill.tile.Draw(spriteBatch);
            }
        }

        public Skill initSkill(String name)
        {
            if (name == "lightsword")
                return new LightSword(Content);
            if (name == "lightbomb")
                return new LightBall(Content);
            if (name == "gigaslash")
                return new GigaSlash(Content);
            if (name == "holyheal")
                return new HolyHeal(Content);
            if (name == "aquaHorn")
                return new AquaHorn(Content);
            return null;
        }
    }
}
