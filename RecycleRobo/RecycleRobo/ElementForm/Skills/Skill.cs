
﻿using ProjectMercury;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework;

namespace RecycleRobo
{
    public class Skill
    {
        public bool isSkillComplete;
        protected ParticleEffect skillEffect;
        protected float timeDelay;
        public int indexSkill;
        protected SoundEffect skillSoundEffect;
        protected  SoundEffect skillSound;
        protected SoundEffectInstance soundInstance;

        public void RoboVisible(Player[] player, bool value)
        {
            for (int i = 0; i < player.Length; i++)
            {
                if (player[i] != null)
                {
                   // player[i].isSelected = false;
                    player[i].isVisible = value;
                    player[i].isControl = value;
                }
            }
        }

        public virtual void DamageAllEnemy(WaveManager waveManager, float damage)
        {
            for (int i = 0; i < waveManager.wave.monsters.Length; i++)
            {
                waveManager.wave.monsters[i].curHealth -= (int)damage;
            }
        }

        public void showParticles(WaveManager waveManager)
        {
            for (int i = 0; i < waveManager.wave.monsters.Length; i++)
            {
                if (waveManager.wave.monsters[i].isAttackable)
                {
                    skillEffect.Trigger(waveManager.wave.monsters[i].Center);
                    skillEffect.Trigger(waveManager.wave.monsters[i].Center);
                    skillEffect.Trigger(waveManager.wave.monsters[i].Center);
                    skillEffect.Trigger(waveManager.wave.monsters[i].Center);
                    skillEffect.Trigger(waveManager.wave.monsters[i].Center);
                    skillEffect.Trigger(waveManager.wave.monsters[i].Center);
                    skillEffect.Trigger(waveManager.wave.monsters[i].Center);
                    skillEffect.Trigger(waveManager.wave.monsters[i].Center);
                }
            }

            //Console.WriteLine("trigger");

            Random r = new Random();
            for (int j = 0; j <= 30; j++)
            {
                float y = r.Next(200, 480);
                float x = r.Next(200, (int) (GlobalClass.ScreenWidth - 15));

                //Console.WriteLine(x + "  " + y);
                skillEffect.Trigger(new Vector2 (x, y));
            }
        }
    }
}
