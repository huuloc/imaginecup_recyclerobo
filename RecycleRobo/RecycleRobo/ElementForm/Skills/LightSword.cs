using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProjectMercury;
using RecycleRobo.Scenes;
using Microsoft.Xna.Framework.Audio;

namespace RecycleRobo
{
    public class LightSword : Skill
    {
        AnimationPlayer numberoneAni;
        Animation curAniNum1;

        AnimationPlayer numbertwoAni;
        Animation curAniNum2;

        public Texture2D originTexture;
        public Texture2D lastHitTexture;

        public Texture2D announceNum1, announceNum2;

        float rotate = 0.0f;
        float pi = 3.14169f;
        float x, y;
        float x_num1, y_num1, x_num2, y_num2;
        bool isStop; float timecount = 0;
        bool isAttack;

        private float damage;

        SoundEffect attackSound;

        enum LightSwordState
        {
            ANNOUNCEMENT_SKILL,
            CREATE_SWORD,
            SWORD_COMPLETE,
            SKILL_COMPLETE
        }

        LightSwordState curState;

        public LightSword(ContentManager content)
        {
            damage = 400.0f;

            numbertwoAni.OrderLayer = 0.001f;
            numbertwoAni.Scale = 1f;
            numbertwoAni.color = Color.White;

            numberoneAni.OrderLayer = 0.001f;
            numberoneAni.Scale = 1f;
            numberoneAni.color = Color.White;

            curAniNum1 = GlobalClass.getAnimationByName("SKILL1_SHINE");

            lastHitTexture = content.Load<Texture2D>("skills/skill1/final wave");

            announceNum1 = content.Load<Texture2D>("skills/skill1/skill1_num1");
            x_num1 = -announceNum1.Width * 0.625f ; y_num1 = 25;

            announceNum2 = content.Load<Texture2D>("skills/skill1/skill1_num2");
            x_num2 = GlobalClass.ScreenWidth; y_num2 = 295;

            curState = LightSwordState.ANNOUNCEMENT_SKILL;
            isStop = false;

            isSkillComplete = false;
            isAttack = false;

            x = 10.0f; y = 270.0f;

            skillEffect = content.Load<ParticleEffect>("skills/skill1/hitBySkill1");
            skillEffect.LoadContent(content);
            skillEffect.Initialise();

            SoundEffect.MasterVolume = GlobalClass.volumeSoundFx / 100;

            skillSoundEffect = content.Load<SoundEffect>("skills/skill1/skill1");
            skillSoundEffect.Play();

            skillSound = content.Load<SoundEffect>("skills/skillSound");
            soundInstance = skillSound.CreateInstance();
            soundInstance.Play();

            attackSound = content.Load<SoundEffect>("skills/skill1/_skill1Attack");

            indexSkill = 1;

        }

        public void Update(GameTime gameTime, Player[] player, WaveManager waveManager)
        {
            SoundEffect.MasterVolume = (float)GlobalClass.volumeSoundFx / 100;

            if (curState == LightSwordState.ANNOUNCEMENT_SKILL)
            {
                RoboVisible(player, false);
                if (x_num1 >= GlobalClass.ScreenWidth + 300)
                {
                    curState = LightSwordState.CREATE_SWORD;
                    curAniNum2 = GlobalClass.getAnimationByName("CREATE_SWORD");
                }

                if (x_num2 <=0 && !isStop)
                {
                    //Console.WriteLine("stop");
                    x_num1 += 0;
                    x_num2 += 0;
                    timecount += gameTime.ElapsedGameTime.Milliseconds;
                    if (timecount >= 1900)
                    {
                        timecount = 0;
                        isStop = true;
                    }
                }
                else
                {
                    x_num1 += 40;
                    x_num2 -= 40;
                }
            }

            if (curState == LightSwordState.CREATE_SWORD)
            {
                if (numbertwoAni.IsDone)
                {
                    curAniNum2 = GlobalClass.getAnimationByName("SWORD");
                    curAniNum1 = GlobalClass.getAnimationByName("SKILL_ATTACK_1");
                    curState = LightSwordState.SWORD_COMPLETE;
                }
            }
            
            if (curState == LightSwordState.SWORD_COMPLETE)
            {
                if (numberoneAni.IsDone)
                {
                    curState = LightSwordState.SKILL_COMPLETE;
                    showParticles(waveManager);
                    curAniNum1 = GlobalClass.getAnimationByName("SKILL_ATTACK_2");
                    return;
                }
                rotate -= 0.009f;
                x -= 1.45f; 
                y += 3.75f;
            }

            // dieu kien nay phai co thi particles sau khi hien len moi bien mat!
            float s = (float)gameTime.ElapsedGameTime.TotalSeconds;
            skillEffect.Update(s);

            if (curState == LightSwordState.SKILL_COMPLETE)
            {
                if (!isAttack)
                {
                    attackSound.Play();
                    DamageAllEnemy(waveManager, damage);
                    isAttack = true;
                }
                
                timecount += gameTime.ElapsedGameTime.Milliseconds;
                if (timecount >= 1500)
                {
                    timecount = 0;
                    isSkillComplete = true;
                    soundInstance.Stop();
                    RoboVisible(player, true);
                }
            }

        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            numberoneAni.PlayAnimation(curAniNum1);
            numbertwoAni.PlayAnimation(curAniNum2);

            switch (curState)
            {
                case LightSwordState.ANNOUNCEMENT_SKILL:
                    spriteBatch.Draw(announceNum1, new Vector2(x_num1, y_num1), null, Color.White, 0f, Vector2.Zero, 0.625f, SpriteEffects.None, 0.1f);
                    spriteBatch.Draw(announceNum2, new Vector2(x_num2, y_num2), null, Color.White, 0f, Vector2.Zero, 0.625f, SpriteEffects.None, 0.1f);
                    break;
                case LightSwordState.CREATE_SWORD:
                    numberoneAni.Draw(gameTime, spriteBatch, new Vector2(50, 300));
                    numbertwoAni.Draw(gameTime, spriteBatch, new Vector2(x, y));
                    break;
                case LightSwordState.SWORD_COMPLETE:
                    numberoneAni.Draw(gameTime, spriteBatch, new Vector2(45, 301));
                    numbertwoAni.DrawRotate(gameTime, spriteBatch, new Vector2(x, y), rotate*pi);
                    break;
                case LightSwordState.SKILL_COMPLETE:
                    numberoneAni.Draw(gameTime, spriteBatch, new Vector2(45, 301));
                    spriteBatch.Draw(lastHitTexture, new Vector2(50, 83), null, Color.White, 0f, Vector2.Zero, 0.8f, SpriteEffects.None, 0.01f);
                    //render particles
                    spriteBatch.End();
                    spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend, null, null, null, null, Resolution.getTransformationMatrix());            //virtualScreen.Draw(spriteBatch);
                    GameManager.particlesRender.RenderEffect(skillEffect, ref GameManager.transform);

                    break;
            }
        }
    }
}