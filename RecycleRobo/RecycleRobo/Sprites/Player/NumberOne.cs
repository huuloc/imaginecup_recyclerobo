﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Audio;
using ProjectMercury;
using RecycleRobo.Scenes;
using LoadData;

namespace RecycleRobo
{
    public class NumberOne : Player
    {

        public NumberOne(ContentManager Content, Vector2 initialPos, Assistant assistant, GameManager game)
        : base (Content, initialPos, assistant, game)
        {
            soundAttack = game.Content.Load<SoundEffect>("sounds/_number1Attack");
            healthBar = Content.Load<Texture2D>("Icon/healthbar/healthBar1");
            particleEffect = Content.Load<ParticleEffect>("Particles/hitByNum1");
            particleEffect.LoadContent(Content);
            particleEffect.Initialise();
            changeStatsRobo();
        }

        public override void loadStats()
        {

            upgradeData = loadData.Load<NumberOneUpgrade>("Player/NumberOne.xml");
            baseData = game.Content.Load<NumberOneBase>("Data\\Player\\NumberOne");
            
            base.loadStats();

            NumberOneBase tmp = (NumberOneBase)baseData;
            damage = staticDamage = (int)(tmp.damage * (float)Math.Pow(tmp.coefficientDamage, upgradeData.level));

            levelOfRobot = upgradeData.level;

            if (getLevelOfEvolution() == 1) sprite.FrameToAction = 11;
            else if (getLevelOfEvolution() == 2) sprite.FrameToAction = 9;
            else sprite.FrameToAction = 14;
        }

        public override void initCurrentForm()
        {
            base.initCurrentForm();
            currentForm = new BalanceForm(1,formsData.ElementAt(0).bonus);
        }

        public override Rectangle getRectangle()
        {
            return new Rectangle((int)Position.X, (int)Position.Y, (int)(curAnimation.FrameWidth * sprite.Scale), (int)(sprite.Scale * curAnimation.FrameHeight));
        }

        public override Rectangle getRectangleWithBullet()
        {
            Rectangle tmp = base.getRectangleWithBullet();
            if (getLevelOfEvolution() == 1)
            {
                if (sprite.SpriteEffect == SpriteEffects.None)
                    return new Rectangle(tmp.X + 20, tmp.Y, tmp.Width - 90, tmp.Height);
                else
                    return new Rectangle(tmp.X + 60, tmp.Y, tmp.Width - 80, tmp.Height);
            }
            else if (getLevelOfEvolution() == 2)
            {
                if (sprite.SpriteEffect == SpriteEffects.None)
                    return new Rectangle(tmp.X + 55, tmp.Y, tmp.Width - 120, tmp.Height);
                else
                    return new Rectangle(tmp.X + 70, tmp.Y, tmp.Width - 120, tmp.Height);
            }
            else
            {
                if (sprite.SpriteEffect == SpriteEffects.None)
                    return new Rectangle(tmp.X + 50, tmp.Y, tmp.Width - 130, tmp.Height);
                else
                    return new Rectangle(tmp.X + 80, tmp.Y, tmp.Width - 130, tmp.Height);
            }
            
        }

        public override void setIdleAnimation()
        {
            string key = "NUMBER1_IDLE_" + getLevelOfEvolution();
            curAnimation = currentForm.getAnimationForm(key);
        }

        public override void setWalkingAnimation()
        {
            string key = "NUMBER1_WALK_" + getLevelOfEvolution();
            curAnimation = currentForm.getAnimationForm(key);
        }

        public override void setDeathAnimation()
        {
            string key = "NUMBER1_DIE_" + getLevelOfEvolution();
            curAnimation = GlobalClass.getAnimationByName(key);
        }

        public override void setAttackAnimation()
        {
            string key = "NUMBER1_ATTACK_" + getLevelOfEvolution();
            curAnimation = currentForm.getAnimationForm(key);
            sprite.PlayAnimation(curAnimation);
        }

        public override Texture2D getTexture()
        {
            return game.Content.Load<Texture2D>("Icon2/num1_icon");
        }

        public override Vector2 getCirclePosition()
        {
            return new Vector2(Position.X - 110, Position.Y - 95);
        }

        public override string getPathFormDataFile()
        {
            return "RobotForm/NumberOne.xml";
        }

        public override Rectangle getAttackRectangle()
        {
            Rectangle tmp = base.getAttackRectangle();
            return new Rectangle(tmp.X + 10, tmp.Y, tmp.Width - 50, tmp.Height);
        }

        public override void DrawPosition(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (staticAnimation == null)
            {
                staticAnimation = curAnimation;
            }
            Rectangle staticRectangle = new Rectangle((int)(Position.X), (int)Position.Y,
                    (int)(staticAnimation.FrameWidth * sprite.Scale), (int)(staticAnimation.FrameHeight * sprite.Scale));
            Vector2 Center = new Vector2(staticRectangle.X + staticRectangle.Width / 2, staticRectangle.Y + staticRectangle.Height / 2);
            Vector2 pos = new Vector2(Center.X - currentForm.areaAnimation.FrameWidth * areaAnimationPlayer.Scale / 2, Center.Y);
            areaAnimationPlayer.PlayAnimation(currentForm.areaAnimation);

            Vector2 sidePos = Vector2.Zero;
            Vector2 lvAreaPos = Vector2.Zero;

            if (sprite.SpriteEffect == SpriteEffects.None)
            {
                if (getLevelOfEvolution() == 2) lvAreaPos = new Vector2(10, 0);
                else if (getLevelOfEvolution() == 3) lvAreaPos = new Vector2(0, 5);
            }
            else
            {
                if (getLevelOfEvolution() == 2) lvAreaPos = new Vector2(-10, 0);
                else if (getLevelOfEvolution() == 3) lvAreaPos = new Vector2(0, 5);
                sidePos = new Vector2(25, 0);
            }

            areaAnimationPlayer.Draw(gameTime, spriteBatch, pos + currentForm.deltaAreaPos + sidePos + lvAreaPos);
            //spriteBatch.Draw(Asset.healthTexture, getRectangleWithBullet(), Color.Red);
        }

        public override int getLevelOfEvolution()
        {
            if (levelOfRobot < 9)
            {
                deltaHealthBarPos = new Vector2(-65, -80);
                return 1;
            }
            else if (levelOfRobot < 12) return 2;
            else return 3;
        }

        public override int getNumberOfForm()
        {
            if (levelOfRobot < 6) return 1;
            else if (levelOfRobot < 10) return 2;
            else if (levelOfRobot < 15) return 3;
            else return 4;
        }
    }
}
