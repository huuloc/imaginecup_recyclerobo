﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Audio;
using ProjectMercury;
using RecycleRobo.Scenes;
using LoadData;

namespace RecycleRobo
{
    // Tan cong tu xa
    public class NumberTwo : Ranger
    {
        public NumberTwo(ContentManager Content, Vector2 initialPos, Assistant assistant, GameManager game)
            : base(Content, initialPos, assistant, game) 
        {
            soundAttack = game.Content.Load<SoundEffect>("sounds/_number2Attack");
            healthBar = Content.Load<Texture2D>("Icon/healthbar/healthBar2");
            bulletTexture = Asset.bulletNum2;
            particleEffect = Content.Load<ParticleEffect>("Particles/hitByNum2");
            particleEffect.LoadContent(Content);
            particleEffect.Initialise();
            deltaBulletPos[0] = new Vector2(110, 50);
            deltaBulletPos[1] = new Vector2(22, 50);
            changeStatsRobo();
        }

        public override void loadStats()
        {
            upgradeData = loadData.Load<NumberTwoUpgrade>("Player/NumberTwo.xml");
            baseData = game.Content.Load<NumberTwoBase>("Data\\Player\\NumberTwo");

            base.loadStats();

            NumberTwoBase tmp = (NumberTwoBase)baseData;
            damage = staticDamage = (int)(tmp.damage * (float)Math.Pow(tmp.coefficientDamage, upgradeData.level));
            range = tmp.range * (float)Math.Pow(tmp.coefficientRange, upgradeData.level);

            levelOfRobot = upgradeData.level;
            if (getLevelOfEvolution() == 1) sprite.FrameToAction = 20;
            else if (getLevelOfEvolution() == 2) sprite.FrameToAction = 18;
            else sprite.FrameToAction = 17;
        }

        public override void initCurrentForm()
        {
            base.initCurrentForm();
            currentForm = new SpeedEagleForm(formsData.ElementAt(0).bonus);
        }

        public override Rectangle getRectangle()
        {
            return new Rectangle((int)Position.X, (int)Position.Y, (int)(curAnimation.FrameWidth * sprite.Scale), (int)(sprite.Scale * curAnimation.FrameHeight));
        }

        public override Rectangle getRectangleWithBullet()
        {
            Rectangle tmp = base.getRectangleWithBullet();
            if (getLevelOfEvolution() == 1)
            {
                if (sprite.SpriteEffect == SpriteEffects.None)
                    return new Rectangle(tmp.X + 35, tmp.Y + 15, tmp.Width - 80, tmp.Height - 45);
                else
                    return new Rectangle(tmp.X + 40, tmp.Y + 15, tmp.Width - 80, tmp.Height - 45);
            }
            else if (getLevelOfEvolution()==2)
            {
                if (sprite.SpriteEffect == SpriteEffects.None)
                    return new Rectangle(tmp.X + 30, tmp.Y + 15, tmp.Width - 100, tmp.Height - 60);
                else
                    return new Rectangle(tmp.X + 60, tmp.Y + 15, tmp.Width - 100, tmp.Height - 60);
            }
            else
            {
                if (sprite.SpriteEffect == SpriteEffects.None)
                    return new Rectangle(tmp.X + 60, tmp.Y + 15, tmp.Width - 110, tmp.Height - 80);
                else
                    return new Rectangle(tmp.X + 50, tmp.Y + 15, tmp.Width - 110, tmp.Height - 80);
            }
        }

        public override Rectangle getAttackRectangle()
        {
            Rectangle tmp = base.getAttackRectangle();
            if (sprite.SpriteEffect==SpriteEffects.None)
                return new Rectangle(tmp.X + 20, tmp.Y - 20, (int)(2 * tmp.Width / 3 - 20), tmp.Height);
            else
                return new Rectangle(tmp.X + 50, tmp.Y - 20, (int)(2 * tmp.Width / 3 - 20), tmp.Height);
        }

        public override void DrawPosition(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (staticAnimation == null)
            {
                staticAnimation = curAnimation;
            }
            Rectangle staticRectangle = new Rectangle((int)(Position.X), (int)Position.Y,
                    (int)(staticAnimation.FrameWidth * sprite.Scale), (int)(staticAnimation.FrameHeight * sprite.Scale));
            Vector2 Center = new Vector2(staticRectangle.X + staticRectangle.Width / 2, staticRectangle.Y + staticRectangle.Height / 2);
            Vector2 pos = new Vector2(Center.X - currentForm.areaAnimation.FrameWidth * areaAnimationPlayer.Scale / 2, Center.Y);

            Vector2 tmpAreaPos = Vector2.Zero;
            if (getLevelOfEvolution() == 2)
            {
                if(sprite.SpriteEffect == SpriteEffects.None)
                    tmpAreaPos = new Vector2(-15, 20);
                else
                    tmpAreaPos = new Vector2(10, 20);
            }
            else if (getLevelOfEvolution() == 3) tmpAreaPos = new Vector2(-5, 20);
            
            areaAnimationPlayer.PlayAnimation(currentForm.areaAnimation);
            areaAnimationPlayer.Draw(gameTime, spriteBatch, pos + currentForm.deltaAreaPos+tmpAreaPos);
        }

        public override void setIdleAnimation()
        {
            string key = "NUMBER2_IDLE_" + getLevelOfEvolution();
            curAnimation = currentForm.getAnimationForm(key);
        }

        public override void setWalkingAnimation()
        {
            string key = "NUMBER2_WALK_" + getLevelOfEvolution();
            curAnimation = currentForm.getAnimationForm(key);
        }

        public override void setDeathAnimation()
        {
            string key = "NUMBER2_DIE_" + getLevelOfEvolution();
            curAnimation = GlobalClass.getAnimationByName(key);
        }

        public override void setAttackAnimation()
        {
            string key = "NUMBER2_ATTACK_" + getLevelOfEvolution();
            curAnimation = currentForm.getAnimationForm(key);
            sprite.PlayAnimation(curAnimation);
        }

        public override Vector2 getCirclePosition()
        {
            return new Vector2(Position.X - 110, Position.Y - 120);
        }

        public override string getPathFormDataFile()
        {
            return "RobotForm/NumberTwo.xml";
        }

        public override Texture2D getTexture()
        {
            return game.Content.Load<Texture2D>("Icon2/num2_icon");
        }

        public override int getLevelOfEvolution()
        {
            if (levelOfRobot < 10) return 1;
            else if (levelOfRobot < 15) return 2;
            return 3;
        }
        public override int getNumberOfForm()
        {
            if (levelOfRobot < 7) return 1;
            else if (levelOfRobot < 10) return 2;
            else if (levelOfRobot < 15) return 3;
            else return 4;
        }
    }
}
