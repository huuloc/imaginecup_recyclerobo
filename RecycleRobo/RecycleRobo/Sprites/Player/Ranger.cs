﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Audio;
using ProjectMercury;
using RecycleRobo.Scenes;

namespace RecycleRobo
{
    // Tan cong tu xa
    public abstract class Ranger : Player
    {
        protected float range;
        protected List<NormalBullet> bullets = new List<NormalBullet>();
        public Vector2[] deltaBulletPos = new Vector2[2];
        public Texture2D bulletTexture;

        public Ranger(ContentManager Content, Vector2 initialPos, Assistant assistant, GameManager game)
            : base(Content, initialPos, assistant, game)
        {
         //   Console.WriteLine("range = " + range);
        }

        public override void moveToAttack(GameTime gameTime)
        {
            state = 3;
            TargetPos = Enemy.Center - this.Center + this.Position;
            initialDirection = TargetPos - Position;
            isWalking = true;
            isAttack = false;
            if (Enemy != null && Enemy.isAttackable && Enemy.isAlive)
            {
                float distance = Vector2.Distance(Center, Enemy.Center);
                float distanceX = Math.Abs(Center.X - Enemy.Center.X);
                float distanceY = Enemy.Center.Y - Center.Y;
                if (distance < range)
                {
                    attack(gameTime);
                }
                else
                {
                    setWalkingAnimation();
                    UpdatePosition(gameTime);
                }
            }
            else
                idle();
        }

        public override void attack(GameTime gameTime)
        {
            state = 2;
            TargetPos = Enemy.Center - this.Center + this.Position;
            initialDirection = TargetPos - Position;
            if (this.GetType().Name == "NumberFour")
                return;
            isAttack = true;
            isWalking = false;

            if (Enemy != null && Enemy.isAttackable && Enemy.isAlive && Vector2.Distance(Center, Enemy.Center) < range + 10 && Enemy.isVisible)
            {
                if (Enemy.Center.X < Center.X)
                    sprite.SpriteEffect = SpriteEffects.FlipHorizontally;
                else
                    sprite.SpriteEffect = SpriteEffects.None;

                setAttackAnimation();

                if (sprite.isFrameToAction)
                {
                    soundAttack.Play();

                    if (GetType().Name == "NumberTwo" && getLevelOfEvolution() >= 2)
                    {
                        NormalBullet bullet = null;

                        if (sprite.SpriteEffect == SpriteEffects.None)
                        {
                            bullet = new NormalBullet(bulletTexture, this.Position + deltaBulletPos[0], Enemy.Center, damage, 7);
                        }
                        if (sprite.SpriteEffect == SpriteEffects.FlipHorizontally)
                        {
                            bullet = new NormalBullet(bulletTexture, this.Position + deltaBulletPos[1], Enemy.Center, damage, 7);
                        }

                        bullets.Add(bullet);
                    }
                    else
                    {
                        Enemy.curHealth -= damage;
                        particleEffect.Trigger(new Vector2(Enemy.Center.X, Enemy.Center.Y));
                        particleEffect.Trigger(new Vector2(Enemy.Center.X, Enemy.Center.Y));
                        particleEffect.Trigger(new Vector2(Enemy.Center.X, Enemy.Center.Y));
                    }

                    sprite.isFrameToAction = false;
                }
            }
            else
            {
                idle();
            }
        }

        public override void UpdateEvent(GameTime gameTime, Monster[] monsters)
        {
            base.UpdateEvent(gameTime, monsters);

            foreach (NormalBullet bullet in bullets.ToArray())
            {
                bullet.Update(gameTime);
                for (int i = 0; i < monsters.Length; i++)
                {
                    if (monsters[i].curHealth <= 0) continue;
                    if (monsters[i] != null && monsters[i].isAlive)
                    {
                        if (bullet.getRectangle().Intersects(monsters[i].getRectangle()))
                        {
                            monsters[i].curHealth -= bullet.damage;
                            particleEffect.Trigger(monsters[i].Center);
                            particleEffect.Trigger(monsters[i].Center);
                            particleEffect.Trigger(monsters[i].Center);
                            bullets.Remove(bullet);
                        }
                    }
                }
            }
        }

        public override void Draw(GameTime gametime, SpriteBatch spriteBatch)
        {
            base.Draw(gametime, spriteBatch);
            foreach (NormalBullet bullet in bullets)
            {
                bullet.Draw(spriteBatch);
            }
        }
    }
}
