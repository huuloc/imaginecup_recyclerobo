﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Audio;

using ProjectMercury;
using ProjectMercury.Emitters;
using ProjectMercury.Modifiers;
using ProjectMercury.Renderers;
using RecycleRobo.Scenes;
using LoadData;


namespace RecycleRobo
{
    public abstract class Player
    {

        public GameManager game;

        public Vector2 initialDirection;
        public Vector2 Position;
        public Vector2 TargetPos;
        public Vector2 HealthBarPos;
        public Vector2 deltaHealthBarPos;

        public bool isSelected;
        public bool isWalking;
        public bool isControl;
        public bool isAttack;
        //public bool drawPath;
        public bool isVisible;
        public bool isAlive;
        public int state;

        public Vector2 pathTarget;

        /* declare variale load file data */
        public Data loadData = new Data();
        public PlayerAtt upgradeData;
        public RoboBaseData baseData;

        public int initialHealth;
        public int curHealth;
        public int staticHealth;
        public int damage;
        public int staticDamage;
        public int healAmount;
        public int staticHealAmount;
        public float speed;
        public float staticSpeed;

        public float curOrderLayer;

        public List<FormData> formsData;
        public ElementForm currentForm;

        public Assistant assistantItem;

        public FormManager formManager;

        public int levelOfRobot;

        protected Texture2D healthTexture;
        protected Texture2D healthBar;
        public Texture2D selectedTexture;
        public Texture2D elementCircle;
        public AnimationPlayer sprite;
        public AnimationPlayer areaAnimationPlayer;
        public AnimationPlayer effectAnimation;

        public Animation staticAnimation;
        protected Animation curAnimation;
        public Animation CurAnimation
        {
            set { curAnimation = value; }
            get { return curAnimation; }
        }

        protected SoundEffect soundAttack;
        protected SoundEffect soundDie;
        public Monster Enemy;

        protected Vector2[] pathPos;
        public ParticleEffect particleEffect;
        protected MouseState curState;
        protected MouseState prevState;

        protected Monster[] monsters;

        public Player(ContentManager Content, Vector2 initialPos, Assistant assistant,GameManager game)
        {
            this.game = game;
            deltaHealthBarPos = new Vector2(-75, -60);
            this.assistantItem = assistant;
            calculateFinalStats();
            this.TargetPos = new Vector2(-1, -1);
            staticAnimation = null;
            Position = initialPos;
            HealthBarPos = Vector2.Zero;
            sprite.Scale = 0.625f;
            sprite.color = Color.White;
            curOrderLayer = 1 - (float)Position.Y / GlobalClass.ScreenHeight;
            healthTexture = Asset.healthTexture;
            isSelected = false;
            isWalking = false;
            isAttack = false;
            selectedTexture = Content.Load<Texture2D>("Animations/slot_5x5");
            areaAnimationPlayer.color = Color.White;
            areaAnimationPlayer.Scale = 0.55f;
            areaAnimationPlayer.OrderLayer = 0.94f;
            isVisible = true;
            isControl = true;
            isAlive = true;
            particleEffect = new ParticleEffect();
            pathPos = new Vector2[4];
            //drawPath = false;
            pathTarget = new Vector2(-1, -1);
            initCurrentForm();
            formManager = new FormManager(game.Content, formsData, getNumberOfForm(), currentForm);
            setIdleAnimation();
            state = 0;
            soundDie = Content.Load<SoundEffect>("sounds/robo_die");
        }

        public abstract Rectangle getRectangle();
        public abstract void setIdleAnimation();
        public abstract void setWalkingAnimation();
        public abstract void setDeathAnimation();
        public virtual void setAttackAnimation() { }
        public virtual void setHealingAnimation() { }
        public abstract Texture2D getTexture();
        public abstract Vector2 getCirclePosition();
        public abstract String getPathFormDataFile();
        public abstract int getNumberOfForm();
        public abstract int getLevelOfEvolution();

        public virtual void initCurrentForm()
        {
            formsData = new Data().Load<List<FormData>>(getPathFormDataFile());
        }
        
        public virtual void loadStats()
        {
            staticHealth = initialHealth = curHealth = (int)(baseData.health * (float)Math.Pow(baseData.coefficientHealth, upgradeData.level));
            speed = staticSpeed = baseData.speed * (float)Math.Pow(baseData.coefficientSpeed, upgradeData.level);
        }

        public void bonusFromAssistant()
        {
            if (assistantItem != null)
            {
                // bonus defence
                initialHealth = staticHealth + assistantItem.defenceBonus;
                curHealth = staticHealth + assistantItem.defenceBonus;
                //bonus speed
                speed = staticSpeed + assistantItem.speedBonus;
                //bonus damage
                damage = staticDamage + assistantItem.damageBonus;
                //bonus heal
                healAmount = staticHealAmount + assistantItem.healBonus;
            }
        }

        public void calculateFinalStats()
        {
            loadStats();
            bonusFromAssistant();
        }

        public virtual void Draw(GameTime gametime, SpriteBatch spriteBatch)
        {
            if (isVisible)
            {
                sprite.OrderLayer = curOrderLayer;
                sprite.PlayAnimation(curAnimation);
                sprite.Draw(gametime, spriteBatch, Position);
                curHealth = (int)MathHelper.Clamp(curHealth, 0, initialHealth);

                if ((damage > 0 || healAmount > 0) && curHealth > 0)
                {
                    if (isSelected)
                        this.DrawPosition(gametime, spriteBatch);

                    if (!GlobalClass.isInMap)
                    {
                        if (getLevelOfEvolution() == 1 && this is NumberTwo) deltaHealthBarPos = new Vector2(-70, -90);
                        if (getLevelOfEvolution() == 2) deltaHealthBarPos = new Vector2(-70, -80);
                        else if (getLevelOfEvolution() == 3) deltaHealthBarPos = new Vector2(-70, -97);

                        HealthBarPos = Center + deltaHealthBarPos;
                        spriteBatch.Draw(healthBar, HealthBarPos, null, Color.White, 0f, Vector2.Zero, 0.525f, SpriteEffects.None, 0.1f);
                        spriteBatch.Draw(healthTexture, new Rectangle((int)HealthBarPos.X + 32, (int)HealthBarPos.Y + 2,
                                    (int)(healthTexture.Width * 0.53f), (int)(healthTexture.Height * 0.526f)), null, Color.Yellow,
                                    0f, Vector2.Zero, SpriteEffects.None, 0.14f);
                        spriteBatch.Draw(healthTexture, new Rectangle((int)HealthBarPos.X + 32, (int)HealthBarPos.Y + 2,
                                    (int)(healthTexture.Width * 0.53f * (double)curHealth / initialHealth), (int)(healthTexture.Height * 0.526f)),
                                    null, Color.Green, 0f, Vector2.Zero, SpriteEffects.None, 0.12f);

                        formManager.Draw(spriteBatch, gametime);
                        if (assistantItem != null) assistantItem.Draw(spriteBatch);

                        //spriteBatch.Draw(Asset.healthTexture, getAttackRectangle(), Color.White);
                    }

                }
            }
        }
        public virtual void DrawPosition(GameTime gameTime, SpriteBatch spriteBatch) {}

        public Vector2 Center
        {
            get
            {
                return new Vector2(Position.X + curAnimation.FrameWidth * sprite.Scale / 2.0f,
                                   Position.Y + curAnimation.FrameHeight * sprite.Scale / 2.0f);
            }
        }

        public virtual void UpdatePosition(GameTime gameTime)
        {
            Vector2 direction = TargetPos - Position;
            if (direction.Length() < 0.01)
            {
                if (state == 1) idle();
                else if (state == 3) {
                    attack(gameTime);
                }
                return;
            }
            direction.Normalize();
            if (Position.X < 3 && Position.X + direction.X < Position.X)
            {
                TargetPos.X = Position.X;
                direction.X = 0;
                initialDirection.X = 0;
            }
            if (Position.Y < 135 && Position.Y + direction.Y < Position.Y)
            {
                TargetPos.Y = Position.Y;
                direction.Y = 0;
                initialDirection.Y = 0;
            }
            if (Position.X > GlobalClass.ScreenWidth - sprite.Scale * curAnimation.FrameWidth && Position.X + direction.X > Position.X)
            {
                TargetPos.X = Position.X;
                direction.X = 0;
                initialDirection.X = 0;
            }
            if (Position.Y > GlobalClass.ScreenHeight - sprite.Scale * curAnimation.FrameHeight && Position.Y + direction.Y > Position.Y)
            {
                TargetPos.Y = Position.Y;
                direction.Y = 0;
                initialDirection.Y = 0;
            }

            if (Math.Abs(direction.X) <= 0.001 && Math.Abs(direction.Y) <= 0.001)
            {
                idle();
                return;
            }
            direction.Normalize();

            if (isWalking && Math.Sign(initialDirection.X) == Math.Sign(direction.X) && Math.Sign(initialDirection.Y) ==
                Math.Sign(direction.Y) && isControl)
            {

                if (TargetPos.X < Position.X)
                {
                    sprite.SpriteEffect = SpriteEffects.FlipHorizontally;
                }
                else
                {
                    sprite.SpriteEffect = SpriteEffects.None;
                }

                Position.X += (float)(direction.X * speed * gameTime.ElapsedGameTime.TotalSeconds);
                Position.Y += (float)(direction.Y * speed * gameTime.ElapsedGameTime.TotalSeconds);
            }
            else 
            {
                this.TargetPos = Position;
            }

            curOrderLayer = 1 - (float)Position.Y / GlobalClass.ScreenHeight;
        }

        public virtual void Effect() { }

        public void idle() 
        {
            state = 0;
            isWalking = false;
            isAttack = false;
            setIdleAnimation();
        }

        public void move(GameTime gameTime) 
        {
            state = 1;
            if (TargetPos.X != -1 && (TargetPos - Position).Length() > 5)
            {
                isWalking = true;
                isAttack = false;
                setWalkingAnimation();
                UpdatePosition(gameTime);
            }
            else
            {
                idle();
            }
        }

        public virtual void attack(GameTime gameTime) 
        {
            state = 2;
            if (this.GetType().Name == "NumberFour")
                return;
            isAttack = true;
            isWalking = false;

            if (Enemy != null && Enemy.isAttackable && Enemy.isAlive && Enemy.isVisible
                && this.getAttackRectangle().Intersects(Enemy.getAttackRectangle()))
            {
                if (Enemy.Center.X < Center.X)
                    sprite.SpriteEffect = SpriteEffects.FlipHorizontally;
                else
                    sprite.SpriteEffect = SpriteEffects.None;

                setAttackAnimation();
                if (sprite.isFrameToAction)
                {
                    soundAttack.Play();
                    Enemy.curHealth -= damage;
                    if (Enemy is Trash3 && (this is NumberOne || this is NumberThree))
                    {
                        this.curHealth -= 40;
                    }
             //       Console.WriteLine(isControl);
                    particleEffect.Trigger(Enemy.Center);
                    particleEffect.Trigger(Enemy.Center);
                    particleEffect.Trigger(Enemy.Center);
                    sprite.isFrameToAction = false;
                }
            }
            /*else if (Enemy != null && Enemy.isAlive && Enemy.isAttackable && Enemy.isVisible)
            {
                moveToAttack(gameTime);
                Console.WriteLine("cai gi the");
            }*/
            else
                idle();
        }

        public virtual void moveToAttack(GameTime gameTime)
        {
            state = 3;
            
            isWalking = true;
            isAttack = false;
            if (Enemy != null && Enemy.isAttackable && Enemy.isAlive)
            {
                TargetPos = Enemy.Center - this.Center + this.Position;
                initialDirection = TargetPos - Position;
                if (this.getAttackRectangle().Intersects(Enemy.getAttackRectangle()))
                {
                    attack(gameTime);
                }
                else
                {
                    setWalkingAnimation();
                    UpdatePosition(gameTime);
                }
            }
            else
                idle();
        }

        public virtual void UpdateEvent(GameTime gameTime, Monster[] monsters)
        {
            this.monsters = monsters;
            Effect();
            
            curState = Mouse.GetState();

            float s = (float)gameTime.ElapsedGameTime.TotalSeconds;
            particleEffect.Update(s);

            if (!isAttack && !isWalking)
            {
                foreach (Monster monster in monsters)
                {
                    if (Player.ReferenceEquals(monster.enemy, this) && monster.isCollide && monster.isAlive)
                    {
                        this.Enemy = monster;
                        attack(gameTime);
                    }
                }
            }

            if (isControl)
            {
                if (curState.LeftButton == ButtonState.Pressed && prevState.LeftButton == ButtonState.Released)
                {
                    Point mousePoint = new Point(MouseHelper.MousePosition(curState).X, MouseHelper.MousePosition(curState).Y);

                    if (curState.Y > 101 && isVisible)
                    {
                        if (this.getRectangle().Contains(mousePoint) && curState.Y > 101 && isVisible && curState.Y > 101)
                        {

                            foreach (RoboToSelect p in GlobalClass.RobotSelected)
                            {
                                if (p.correspondingRobot == null) continue;
                                if (!p.correspondingRobot.isAlive) continue;
                                if (!Player.ReferenceEquals(this, p.correspondingRobot))
                                {
                                    if (!p.correspondingRobot.getRectangle().Contains(mousePoint))
                                    {
                                        p.correspondingRobot.isSelected = false;
                                    }
                                }
                            }
                            this.isSelected = true;
                        }
                    }
                }

                formManager.Update(gameTime, isSelected);
                if (formManager.CurrentForm.GetType().Name != currentForm.GetType().Name)
                {
                    curHealth = curHealth *100 / (100 + currentForm.bonus[0]);
                    currentForm = formManager.CurrentForm;
                    changeStatsRobo();
                }
                currentForm.Update(this,gameTime);
                currentForm.Update(this, gameTime, monsters);

                if (curState.RightButton == ButtonState.Pressed && prevState.RightButton == ButtonState.Released)
                {
                    if (isSelected == true)
                    {
                        bool check = FindEnemy(monsters, curState);
                        TargetPos = new Vector2(MouseHelper.MousePosition(curState).X - Center.X + Position.X, MouseHelper.MousePosition(curState).Y - Center.Y + Position.Y);
                        initialDirection = TargetPos - Position;
                        if (check)
                        {
                            TargetPos = Enemy.Center - this.Center + this.Position;
                            initialDirection = TargetPos - Position;
                            state = 3;
                        }
                        else state = 1;
                    }
                }

                if (assistantItem != null) assistantItem.Update(gameTime, this);

                if (state == 0) idle();
                else if (state == 1) move(gameTime);
                else if (state == 2) attack(gameTime);
                else moveToAttack(gameTime);
            }

            // tai sao lai ko dua doan nay vao trong dieu kien isControl nhi???
            //if (state == 0) idle();
            //else if (state == 1) move(gameTime);
            //else if (state == 2) attack(gameTime);
            //else moveToAttack(gameTime);
            
            prevState = curState;
        }

        public virtual void CheckOrderWithEnemy(Monster[] monsters)
        {
            if (isControl)
            {
                for (int j = 0; j < monsters.Length; j++)
                {
                    if (monsters[j] != null)
                    {
                        Monster test1 = monsters[j];
                        if (getRectangle().Intersects(test1.getRectangle()))
                        {
                            if ((Position.Y < test1.Position.Y) && (curOrderLayer < test1.curOrderLayer))
                            {
                                float tmp = curOrderLayer;
                                curOrderLayer = test1.curOrderLayer;
                                test1.curOrderLayer = tmp;
                            }
                            else if ((Position.Y > test1.Position.Y) && (curOrderLayer > test1.curOrderLayer))
                            {
                                float tmp = curOrderLayer;
                                curOrderLayer = test1.curOrderLayer;
                                test1.curOrderLayer = tmp;
                            }
                        }
                    }
                }
            }
        }

        public virtual bool FindEnemy(Monster[] monsters, MouseState curState)
        {
            bool result = false;
            if (this.GetType().Name == "NumberFour")
                return false;
            Texture2D image = getTexture();
            Rectangle bound = new Rectangle((int)(MouseHelper.MousePosition(curState).X)
                , (int)(MouseHelper.MousePosition(curState).Y), image.Width * (int)sprite.Scale, image.Height * (int)sprite.Scale);
            float minDist = 1000;
            for (int i = 0; i < monsters.Length; i++)
            {
                if (monsters[i] != null && monsters[i].getRectangle().Intersects(bound) && isSelected && monsters[i].curHealth > 0.1 && monsters[i].isVisible)
                {
                    result = true;
                    if (((monsters[i]).Position - Position).Length() < minDist)
                    {
                        minDist = ((monsters[i]).Position - Position).Length();
                        this.Enemy = monsters[i];
                    }
                }
            }
            return result;
        }

        //Ham tan cong cua tanker
        public virtual void AttackEnemy(GameTime gameTime, Monster[] monsters)
        {
            
        }

        public virtual void HealingTeammate(GameTime gameTime, Player[] player, MouseState curState, MouseState preState) { }

        public virtual void checkCursorTarget(Monster[] monsters)
        {
            int countEle = 0;
            curState = Mouse.GetState();
            Point cursor = new Point(MouseHelper.MousePosition(curState).X, MouseHelper.MousePosition(curState).Y);
            
            if (isSelected && this.GetType().Name != "NumberFour")
            {
                foreach (Monster enemy in monsters)
                {
                    if (enemy.getRectangle().Contains(cursor) && enemy.isAttackable && enemy.isVisible)
                        GameManager.winForm.Cursor = GameManager.cursorAttack;
                    else
                        countEle++;
                }
            }

            if (countEle == monsters.Count())
            {
                GameManager.winForm.Cursor = GameManager.cursorDefault;
            }

            prevState = curState;
        }

        public void checkDead()
        {
            curHealth = (int)MathHelper.Clamp(curHealth, 0, initialHealth);
            if (curHealth <= 0)
            {
                setDeathAnimation();
                if (isControl) soundDie.Play();
                isControl = false;
                if (this.sprite.IsDone)
                    isAlive = false;
            }
        }
        public virtual Rectangle getAttackRectangle()
        {
            if (staticAnimation == null)
            {
                staticAnimation = curAnimation;
            }
            Rectangle staticRectangle = new Rectangle((int)(Position.X), (int)Position.Y,
                    (int)(staticAnimation.FrameWidth * sprite.Scale), (int)(staticAnimation.FrameHeight * sprite.Scale));
            Vector2 Center = new Vector2(staticRectangle.X + staticRectangle.Width / 2, staticRectangle.Y + staticRectangle.Height / 2);
            return new Rectangle((int)Center.X - (int)(staticRectangle.Width / 2), (int)Center.Y,
                staticRectangle.Width, staticRectangle.Height / 4);
        }

        public virtual Rectangle getRectangleWithBullet()
        {
            return getRectangle();
        }
        
        public virtual void changeStatsRobo()
        {
            // tang defence.
            initialHealth = staticHealth + staticHealth * currentForm.bonus[0] / 100;
            curHealth = curHealth  + curHealth * currentForm.bonus[0] / 100;
            //tang speed.
            speed = staticSpeed + staticSpeed * currentForm.bonus[1] / 100;
            //tang damage.
            damage = staticDamage + staticDamage * currentForm.bonus[2] / 100;
            //tang heal danh cho con num4 neu sau nay thich.
            healAmount = staticHealAmount + staticHealAmount * currentForm.bonus[2] / 100;
        }

    }
}
