﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.GamerServices;

using ProjectMercury;
using ProjectMercury.Emitters;
using ProjectMercury.Modifiers;
using ProjectMercury.Renderers;
using RecycleRobo.Scenes;
using LoadData;

namespace RecycleRobo
{
    public class NumberFour : Player
    {
        Player teammate;

        public NumberFour(ContentManager Content, Vector2 initialPos, Assistant assistant, GameManager game)
        : base (Content, initialPos, assistant, game)
        {
            sprite.FrameToAction = 18;
            teammate = null;
            healthBar = Content.Load<Texture2D>("Icon/healthbar/healthBar4");
            particleEffect = Content.Load<ParticleEffect>("Particles/healByNum4");
            particleEffect.LoadContent(Content);
            particleEffect.Initialise();
            changeStatsRobo();
        }

        public override void loadStats()
        {
            upgradeData = loadData.Load<NumberFourUpgrade>("Player/NumberFour.xml");
            baseData = game.Content.Load<NumberFourBase>("Data\\Player\\NumberFour");
            levelOfRobot = upgradeData.level;
            base.loadStats();

            NumberFourBase tmp = (NumberFourBase)baseData;
            healAmount = staticHealAmount = (int)(tmp.healAmount * (float)Math.Pow(tmp.coefficientHealAmount, upgradeData.level));
        }

        public override void initCurrentForm()
        {
            base.initCurrentForm();
            currentForm = new BalanceForm(4,formsData.ElementAt(0).bonus);
        }

        public override Rectangle getRectangle()
        {
            return new Rectangle((int)Position.X, (int)Position.Y, (int)(curAnimation.FrameWidth * sprite.Scale), (int)(sprite.Scale * curAnimation.FrameHeight));
        }

        public override Rectangle getRectangleWithBullet()
        {
            Rectangle tmp = base.getRectangleWithBullet();
            if (getLevelOfEvolution() == 1)
            {
                if (sprite.SpriteEffect == SpriteEffects.None)
                    return new Rectangle(tmp.X + 35, tmp.Y + 20, tmp.Width - 80, tmp.Height - 10);
                else
                    return new Rectangle(tmp.X + 50, tmp.Y + 20, tmp.Width - 80, tmp.Height - 10);
            }
            else
            {
                return new Rectangle(tmp.X + 50, tmp.Y + 20, tmp.Width - 100, tmp.Height - 10);
            }
            
        }

        public override Rectangle getAttackRectangle()
        {
            Rectangle tmp = base.getAttackRectangle();
            if (sprite.SpriteEffect == SpriteEffects.None)
            {
                if (getLevelOfEvolution() == 1)
                    return new Rectangle(tmp.X + 15, tmp.Y, (int)(tmp.Width / 2), tmp.Height);
                else if (getLevelOfEvolution() == 2)
                    return new Rectangle(tmp.X + 25, tmp.Y - 20, tmp.Width - 80, tmp.Height);
                else
                    return new Rectangle(tmp.X + 33, tmp.Y, tmp.Width - 65, tmp.Height);
            }
            else
            {
                if (getLevelOfEvolution() == 1)
                    return new Rectangle(tmp.X + 35, tmp.Y, (int)(tmp.Width / 2), tmp.Height);
                else if (getLevelOfEvolution() == 2)
                    return new Rectangle(tmp.X + 45, tmp.Y - 20, tmp.Width - 80, tmp.Height);
                else
                    return new Rectangle(tmp.X + 33, tmp.Y, tmp.Width - 65, tmp.Height);
            }
        }

        public override void setIdleAnimation()
        {
            string key = "NUMBER4_IDLE_" + getLevelOfEvolution();
            curAnimation = currentForm.getAnimationForm(key);
        }

        public override void setWalkingAnimation()
        {
            string key = "NUMBER4_WALK_" + getLevelOfEvolution();
            curAnimation = currentForm.getAnimationForm(key);
        }

        public override void setDeathAnimation()
        {
            string key = "NUMBER4_DIE_" + getLevelOfEvolution();
            curAnimation = GlobalClass.getAnimationByName(key);
        }

        public override void setHealingAnimation()
        {
            string key = "NUMBER4_HEALING_" + getLevelOfEvolution();
            curAnimation = currentForm.getAnimationForm(key);
            sprite.PlayAnimation(curAnimation);
        }

        public void UpdateEvent(GameTime gameTime, Player[] player)
        {
            curState = Mouse.GetState();
            float s = (float)gameTime.ElapsedGameTime.TotalSeconds;
            particleEffect.Update(s);
            if (isControl)
            {
                if (assistantItem != null) assistantItem.Update(gameTime, this);

                Point mousePoint = new Point(MouseHelper.MousePosition(curState).X, MouseHelper.MousePosition(curState).Y);
                if (isSelected)
                {
                    if (isHoverOnRobot(player, mousePoint))
                    {
                        GameManager.winForm.Cursor = GameManager.cursorHealing;
                    }
                    else
                    {
                        GameManager.winForm.Cursor = GameManager.cursorDefault;
                    }
                }

                if (curState.LeftButton == ButtonState.Pressed && prevState.LeftButton == ButtonState.Released)
                {
                    if (this.getRectangle().Contains(mousePoint) && curState.Y > 101 && isVisible && curState.Y > 101)
                    {

                        foreach (RoboToSelect p in GlobalClass.RobotSelected)
                        {
                            if (p.correspondingRobot == null) continue;
                            if (!p.correspondingRobot.isAlive) continue;
                            if (!Player.ReferenceEquals(this, p.correspondingRobot))
                            {
                                if (!p.correspondingRobot.getRectangle().Contains(mousePoint))
                                {
                                    p.correspondingRobot.isSelected = false;
                                }
                            }
                        }
                        this.isSelected = true;
                    }

                }

                formManager.Update(gameTime, isSelected);
                if (formManager.CurrentForm.GetType().Name != currentForm.GetType().Name)
                {
                    curHealth = curHealth * 100 / (100 + currentForm.bonus[0]);
                    currentForm = formManager.CurrentForm;
                    changeStatsRobo();
                }
                currentForm.Update(this, gameTime, player);

                if (curState.RightButton == ButtonState.Pressed && prevState.RightButton == ButtonState.Released)
                {
                    if (isSelected == true)
                    {
                        Texture2D image = getTexture();
                        Vector2 tmpPos = new Vector2(MouseHelper.MousePosition(curState).X - Center.X + Position.X, MouseHelper.MousePosition(curState).Y - Center.Y + Position.Y);
                        Vector2 Pos = new Vector2(MouseHelper.MousePosition(curState).X, MouseHelper.MousePosition(curState).Y);

                        bool check = false;
                        for (int i = 0; i < player.Length; i++)
                        {
                            if (player[i] != null && player[i].GetType().Name != "NumberFour"
                                && player[i].getRectangle().Contains(new Point((int)Pos.X, (int)Pos.Y)) 
                                && player[i].curHealth > 0.1f)
                            {
                                
                                check = true;
                                state = 5;
                                teammate = player[i];
                                break;
                            }
                        }
                        if (!check) 
                        {
                            TargetPos = tmpPos;
                            initialDirection = TargetPos - Position;
                            state = 1;
                        }
                    }
                }
            }
            if (state == 1) move(gameTime);
            else if (state == 5) heal();
            else idle();
            prevState = curState;
        }

        public void heal() 
        {
            state = 5;
            isWalking = false;
            if (teammate != null && teammate.isControl)
            {
                if (this.Center.X > teammate.Center.X)
                    this.sprite.SpriteEffect = SpriteEffects.FlipHorizontally;
                else
                    this.sprite.SpriteEffect = SpriteEffects.None;

                if (teammate.curHealth > teammate.initialHealth - 5)
                {
                    idle();
                }
                else
                {
                    setHealingAnimation();
                    if (sprite.isFrameToAction)
                    {
                        particleEffect.Trigger(teammate.Center);
                        teammate.curHealth += healAmount;
                        if (teammate.curHealth > teammate.initialHealth) teammate.curHealth = teammate.initialHealth;
                        sprite.isFrameToAction = false;
                    }
                }
            }
        }

        public bool isHoverOnRobot(Player[] player, Point mousePoint)
        {
            for (int i = 0; i < player.Length; i++)
            {
                if (player[i] != null && player[i].getRectangle().Contains(mousePoint) &&
                    player[i].GetType().Name != "NumberFour" && player[i].curHealth > 0.1f)
                {
                    return true;
                }
            }
            return false;
        }

        public override void DrawPosition(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (staticAnimation == null)
            {
                staticAnimation = curAnimation;
            }
            Rectangle staticRectangle = new Rectangle((int)(Position.X), (int)Position.Y,
                    (int)(staticAnimation.FrameWidth * sprite.Scale), (int)(staticAnimation.FrameHeight * sprite.Scale));
            Vector2 Center = new Vector2(staticRectangle.X + staticRectangle.Width / 2, staticRectangle.Y + staticRectangle.Height / 2);
            Vector2 pos = new Vector2(Center.X - currentForm.areaAnimation.FrameWidth * areaAnimationPlayer.Scale / 2, Center.Y + 20);
            areaAnimationPlayer.PlayAnimation(currentForm.areaAnimation);
            
            Vector2 tmpDeltaPos = Vector2.Zero;
            if (getLevelOfEvolution() == 3) tmpDeltaPos = new Vector2(2, 25);
            
            if (sprite.SpriteEffect==SpriteEffects.FlipHorizontally)
                areaAnimationPlayer.Draw(gameTime, spriteBatch, pos + currentForm.deltaAreaPos+ tmpDeltaPos + new Vector2 (15,0));
            else
                areaAnimationPlayer.Draw(gameTime, spriteBatch, pos + currentForm.deltaAreaPos + tmpDeltaPos);
        }

        public override Texture2D getTexture()
        {
            return game.Content.Load<Texture2D>("Icon2/num4_icon");
        }

        public override Vector2 getCirclePosition()
        {
            return new Vector2(Position.X - 100, Position.Y - 80);
        }
        public override string getPathFormDataFile()
        {
            return "RobotForm/NumberFour.xml";
        }

        public override int getLevelOfEvolution()
        {
            if (levelOfRobot < 10)
            {
                return 1;
            }
            else if (levelOfRobot < 15)
            {
                return 2;
            }
            else
            {
                return 3;
            }
        }

        public override int getNumberOfForm()
        {
            if (levelOfRobot < 10) return 2;
            else if (levelOfRobot < 15) return 3;
            else return 4;
        }
    }
}
