﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using ProjectMercury;
using RecycleRobo.Scenes;

namespace RecycleRobo
{
    public class Trash4 : Monster
    {
        public Trash4(GameManager game, ContentManager Content, Vector2 initialPos, float initialHealth, float initialDamage, float initialSpeed, Player[] player)
            : base(game, Content, initialPos, initialHealth, initialDamage, initialSpeed, player)
        {
            sprite.FrameToAction = 9;
            
            soundAttack = Content.Load<SoundEffect>("sounds/trash/Trash4_attack");
            soundDie = Content.Load<SoundEffect>("sounds/trash/Trash4_die");

            sewagesTexture = Content.Load<Texture2D>("Items/trash4_die");
            particleEffect = Content.Load<ParticleEffect>("Particles/hitByTrash4");
            particleEffect.LoadContent(Content);
            particleEffect.Initialise();
        }

        public override void setWalkAnimation()
        {
            CurAnimation = GlobalClass.getAnimationByName("TRASH4_WALK");
        }

        public override void setAttackAnimation()
        {
            CurAnimation = GlobalClass.getAnimationByName("TRASH4_ATTACK");
            sprite.PlayAnimation(CurAnimation);
        }

        public override void setDeathAnimation()
        {
            CurAnimation = GlobalClass.getAnimationByName("TRASH4_DIE");
        }

        public override Rectangle getRectangle()
        {
            return new Rectangle((int)Position.X, (int)Position.Y, (int)(CurAnimation.FrameWidth * sprite.Scale), (int)(sprite.Scale * CurAnimation.FrameHeight));
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            base.Draw(gameTime, spriteBatch);
            if (isAttackable)
            {
                spriteBatch.Draw(healthbar, new Vector2((int)(Position.X + sprite.Animation.FrameWidth * sprite.Scale / 4.5), (int)Position.Y - 10),
                    null, Color.LightGray, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.17f);
                spriteBatch.Draw(healthbar, new Rectangle((int)(Position.X + sprite.Animation.FrameWidth * sprite.Scale / 4.5), (int)Position.Y - 10,
                            (int)(healthbar.Width * (double)curHealth / Health), 4), null, Color.Red, 0f, Vector2.Zero, SpriteEffects.None, 0.15f);
            }
        }

        public override Player getNearestRobot()
        {
            if (enemy == null || enemy.curHealth <= 0)
            {
                float minHealth = 100000;
                Player r = null;
                for (int i = 0; i < player.Length; i++)
                {
                    if (player[i] == null || !player[i].isVisible || !player[i].isControl || player[i].curHealth <= 0 ) continue;
                    if (player[i].curHealth < minHealth)
                    {
                        r = player[i];
                        minHealth = curHealth;
                    }
                }
                this.enemy = r;
                return r;
            }
            else
            {
                return enemy;
            }
        }
    }

}
