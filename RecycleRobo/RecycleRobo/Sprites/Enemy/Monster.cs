﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

using ProjectMercury;
using ProjectMercury.Emitters;
using ProjectMercury.Modifiers;
using ProjectMercury.Renderers;
using RecycleRobo.Scenes;

namespace RecycleRobo
{
    public abstract class Monster
    {

        public GameManager game;

        public Vector2 Position;   
        protected Texture2D healthbar;
        protected Texture2D sewagesTexture;

        public Player[] player;

        public float initialspeed;
        public float speed;

        public int Health;
        public int curHealth;
        public int damage;

        public float curOrderLayer;
        public float waitTime;

        public bool isAlive;
        public bool isAttackable;
        public bool isVisible = true;

        Animation curAnimation;
        Animation staticAnimation;
        public Animation CurAnimation
        {
            set { curAnimation = value; }
            get { return curAnimation; }
        }
        public AnimationPlayer sprite;
        public AnimationPlayer effect;
        protected ContentManager Content;

        protected SoundEffect soundAttack;
        protected SoundEffect soundDie;

        public ParticleEffect particleEffect;

        public bool isCollide;
        public bool isAttack;
        public int time;
        public String effectToDraw;
        public Player enemy;

        public Dictionary<String, float > movementEffectModifier;
        public static List<Monster> allMonsters;

        public Texture2D emotionTexture = null;
        public Random allowEmotion;
        public bool isShowEmotion = false, isShowComeback = false;
        public float delayShowEmo = 0;

        public Monster(GameManager game, ContentManager Content, Vector2 initialPos, float initialHealth, float initialDamage, float initialSpeed, Player[] player)
        {
            this.game = game;
            time = 0;
            this.Content = Content;
            this.player = player;
            Position = initialPos;
            sprite.Scale = 0.625f;
            sprite.color = Color.White;
            curOrderLayer = 0.2f;
            Health = curHealth = (int)(initialHealth);
            damage = (int)initialDamage;
            speed = initialSpeed;
            initialspeed = initialSpeed;
            waitTime = 0;
            isAttackable = true;
            isAlive = true;
            healthbar = Content.Load<Texture2D>("HealthBar");
            isCollide = false;
            effectToDraw = "";
            setWalkAnimation();
            enemy = null;
            movementEffectModifier = new Dictionary<string, float>();
            movementEffectModifier.Add("ocean",1.0f);
            movementEffectModifier.Add("ice", 1.0f);
            movementEffectModifier.Add("wave", 1.0f);
            if (allMonsters == null) allMonsters = new List<Monster>();
            if (!(this is BossOne) && !(this is BossTwo))
            {
                allMonsters.Add(this);

                allowEmotion = new Random();
                int valueRandom = allowEmotion.Next(0, 100);
                if (valueRandom >= 65)
                {
                    int index = allowEmotion.Next(1, 12);
                    emotionTexture = Content.Load<Texture2D>("Emotion/emo" + index);
                }
            }
                        
            isAttack = false;
        }

        public abstract void setWalkAnimation();
        public abstract void setAttackAnimation();
        public abstract void setDeathAnimation();

        public virtual void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {

            if (isAlive && isVisible)
            {
                sprite.OrderLayer = curOrderLayer;
                sprite.PlayAnimation(curAnimation);
                sprite.Draw(gameTime, spriteBatch, Position);


                if (emotionTexture != null && isShowEmotion)
                {
                    spriteBatch.Draw(emotionTexture, Center + new Vector2(0, -90), 
                        new Rectangle (0,0,118,116), Color.White,
                        0f, new Vector2(emotionTexture.Width / 2, emotionTexture.Height / 2),
                        0.625f, SpriteEffects.None, 0.099f);
                }
            }
        }

        public void updateEmotion(GameTime gametime)
        {
            if (Center.X >= 20 && Center.X <= GlobalClass.ScreenWidth - 25
                && Center.Y >= 150 &&Center.Y <= GlobalClass.ScreenHeight - 25 
                && delayShowEmo < 1500)
            {
                delayShowEmo += gametime.ElapsedGameTime.Milliseconds;
                isShowEmotion = true;
                if (delayShowEmo >= 1500)
                {
                    isShowEmotion = false;
                }
            }

            if (curHealth < Health / 2 && curHealth > Health / 3 
                && !isShowComeback && emotionTexture!=null
                && !isCollide)
            {
                int index = allowEmotion.Next(1, 12);
                emotionTexture = Content.Load<Texture2D>("Emotion/emo" + index);
                delayShowEmo = 0f;
                isShowComeback = true;
            }
        }

        public virtual void updatePosition(GameTime gameTime)
        {
            float s = (float)gameTime.ElapsedGameTime.TotalSeconds;
            particleEffect.Update(s);

            if (isAttackable)
            {
                updateEmotion(gameTime);
                Player r = getNearestRobot();
                if (!isAttack)
                {
                    for (int i = 0; i < player.Length; i++)
                    {
                        if (player[i] == null || !player[i].isVisible || !player[i].isControl) continue;
                        if (Monster.ReferenceEquals(player[i].Enemy, this) && (player[i].state == 3 || player[i].state == 2))
                            if (Monster.ReferenceEquals(player[i].Enemy, this) 
                                && this.GetType().Name != "BossOne"
                                && this.GetType().Name != "BossTwo"
                                && (player[i].state == 3 || player[i].state == 2))
                        {
                            r = player[i];
                            isAttack = false;
                            break;
                        }
                    }
                }
                this.enemy = r;
                if (enemy == null || !enemy.isVisible || enemy.curHealth<= 0)
                {
                    setWalkAnimation();
                    return;
                }

                if (this.getAttackRectangle().Intersects(enemy.getAttackRectangle()) && enemy.isVisible)
                {
                    isCollide = true;
                    waitTime = 7;
                    if (waitTime > 0)
                    {
                        waitTime -= (float)gameTime.ElapsedGameTime.Milliseconds;
                        if (waitTime < 0)
                        {
                            waitTime = 0;
                            setAttackAnimation();
                        }
                    }
                    isAttack = true;
                    if (sprite.isFrameToAction)
                    {
                        enemy.curHealth -= damage;
                        soundAttack.Play();
                        particleEffect.Trigger(enemy.Center);
                        particleEffect.Trigger(enemy.Center);
                        sprite.isFrameToAction = false;
                    }
                }
                else
                {
                    isAttack = false;
                    isCollide = false;
                    waitTime = 7;
                    if (waitTime > 0)
                    {
                        waitTime -= (float)gameTime.ElapsedGameTime.Milliseconds;
                        if (waitTime < 0)
                        {
                            waitTime = 0;
                            setWalkAnimation();
                        }
                    }
                }
                if (!isCollide)
                {
                    if ((enemy.Center.X - Center.X) < 2)
                    {
                        sprite.SpriteEffect = SpriteEffects.FlipHorizontally;
                    }
                    else if (enemy.Center.X < Center.X)
                    {
                        sprite.SpriteEffect = SpriteEffects.FlipHorizontally;
                    }
                    else
                    {
                        sprite.SpriteEffect = SpriteEffects.None;
                    }
                }
                if(!isAttack)
                    MoveTo(enemy.Center, speed, gameTime);

                curOrderLayer = 1 - (float)Position.Y / GlobalClass.ScreenHeight;
            }
        }

        public virtual void MoveTo(Vector2 target, float Speed, GameTime gameTime)
        {
            Vector2 direction = target - Center;
            direction.Normalize();
            float realSpeed = Speed;
            for (int i = 0; i < movementEffectModifier.Count; i++) 
            {
                realSpeed = realSpeed * movementEffectModifier.Values.ElementAt(i);
            }
            Position.X += (float)(direction.X * realSpeed * gameTime.ElapsedGameTime.TotalSeconds);
            Position.Y += (float)(direction.Y * realSpeed * gameTime.ElapsedGameTime.TotalSeconds);
            foreach (Monster m in allMonsters)
            {
                if (!m.isAlive || !m.isAttackable) continue;
                if (Monster.ReferenceEquals(this, m)) continue;
                if (Vector2.Distance(this.Position, m.Position) < 20)
                {
                    Vector2 move = (this.Position - m.Position);
                    move.Normalize();
                    this.Position += move * 2 + direction * 2;
                    break;
                }
            }
        }

        public virtual Player getNearestRobot()
        {
            float minLength = 100000;
            Player r = null;
            for (int i = 0; i < player.Length; i++)
            {
                if (player[i] == null || !player[i].isVisible || !player[i].isControl || player[i].curHealth <= 0) continue;
                Vector2 dis = player[i].Position - Position;
                if (dis.Length() < minLength)
                {
                    r = player[i];
                    minLength = dis.Length();
                }
            }
            this.enemy = r;
            return r;
        }

        public virtual Vector2 Center
        {
            get
            {
                return new Vector2(Position.X + curAnimation.FrameWidth * sprite.Scale / 2.0f,
                                   Position.Y + curAnimation.FrameHeight * sprite.Scale / 2.0f);
            }
            set { }
        }

        public abstract Rectangle getRectangle();

        public virtual Rectangle getAttackRectangle()
        {
            if (staticAnimation == null)
            {
                staticAnimation = curAnimation;
            }
            Rectangle staticRectangle = new Rectangle((int)(Position.X), (int)Position.Y,
                    (int)(staticAnimation.FrameWidth * sprite.Scale), (int)(staticAnimation.FrameHeight * sprite.Scale));
            Vector2 Center = new Vector2(staticRectangle.X + staticRectangle.Width / 2, staticRectangle.Y + staticRectangle.Height / 2);
            return new Rectangle((int)Center.X - (int)(staticRectangle.Width / 2), (int)Center.Y,
                staticRectangle.Width, staticRectangle.Height / 4);
        }

        public void checkAlive(List<Item> sewages)
        {
            curHealth = (int)MathHelper.Clamp(curHealth, 0, Health);
            if (curHealth <= 0)
            {
                if (isAttackable) soundDie.Play();
                setDeathAnimation();
                isAttackable = false;
                if (sprite.IsDone)
                {
                    createSewage(sewages);
                    isAlive = false;
                }
            }
        }

        public virtual void createSewage(List<Item> sewages)
        {
            Item item = new Item(sewagesTexture, Position, sprite.Scale, sprite.SpriteEffect, sprite.OrderLayer, this.Health);
            sewages.Add(item);
        }
        public void stunEffect(int duration, GameTime gameTime) 
        {
            
        }
    }
}
