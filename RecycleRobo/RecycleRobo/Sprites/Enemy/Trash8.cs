﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using ProjectMercury;
using RecycleRobo.Scenes;

namespace RecycleRobo
{
    public class Trash8 : Monster
    {
        public int robot = -10;
        public Random rd = new Random(2000);

        public Trash8(GameManager game, ContentManager Content, Vector2 initialPos, float initialHealth, float initialDamage, float initialSpeed, Player[] player)
            : base(game, Content, initialPos, initialHealth, initialDamage, initialSpeed, player)
        {
            sprite.FrameToAction = 7;
            
            soundAttack = Content.Load<SoundEffect>("sounds/trash/Trash8_attack");
            soundDie = Content.Load<SoundEffect>("sounds/trash/Trash8_die");
            
            sewagesTexture = Content.Load<Texture2D>("Items/trash8_die");
            particleEffect = Content.Load<ParticleEffect>("Particles/hitByTrash2");
            particleEffect.LoadContent(Content);
            particleEffect.Initialise();
        }

        public override void setWalkAnimation()
        {
            CurAnimation = GlobalClass.getAnimationByName("TRASH8_WALK");
        }

        public override void setAttackAnimation()
        {
            CurAnimation = GlobalClass.getAnimationByName("TRASH8_ATTACK");
            sprite.PlayAnimation(CurAnimation);
        }

        public override void setDeathAnimation()
        {
            CurAnimation = GlobalClass.getAnimationByName("TRASH8_DIE");
        }

        public override Rectangle getRectangle()
        {
            return new Rectangle((int)Position.X, (int)Position.Y, (int)(CurAnimation.FrameWidth * sprite.Scale), (int)(sprite.Scale * CurAnimation.FrameHeight));
        }

        public override Rectangle getAttackRectangle()
        {
            Rectangle tmp = base.getAttackRectangle();
            if (sprite.SpriteEffect == SpriteEffects.None)
                return new Rectangle(tmp.X + 55, tmp.Y - 10, tmp.Width - 80, tmp.Height + 15);
            else
                return new Rectangle(tmp.X + 20, tmp.Y - 10, tmp.Width - 65, tmp.Height + 15);
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            base.Draw(gameTime, spriteBatch);
            if (isAttackable)
            {
                spriteBatch.Draw(healthbar, new Vector2((int)(Position.X + sprite.Animation.FrameWidth * sprite.Scale / 4.5), (int)Position.Y - 10), null, Color.LightGray, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.17f);
                spriteBatch.Draw(healthbar, new Rectangle((int)(Position.X + sprite.Animation.FrameWidth * sprite.Scale / 4.5), (int)Position.Y - 10,
                            (int)(healthbar.Width * (double)curHealth / Health), 4), null, Color.Red, 0f, Vector2.Zero, SpriteEffects.None, 0.15f);
            }
        }

        public override Player getNearestRobot()
        {
            if (enemy == null || enemy.curHealth <= 0)
            {
                int check = 0;
                for (int i = 0; i < player.Length; i++)
                {
                    if (player[i].curHealth <= 0) check++;
                    else break;
                    if (check == player.Length) return null;
                }

                if (robot >= 0 && player[robot].curHealth > 0)
                {
                    this.enemy = player[robot];
                    return player[robot];
                }

                robot = rd.Next(0, player.Length);

                while (player[robot].curHealth <= 0)
                    robot = rd.Next(0, player.Length);

                //    Console.WriteLine("danh robot" + robot);
                this.enemy = player[robot];
                return player[robot];
            }
            else
            {
                return enemy;
            }
        }
    }

}


