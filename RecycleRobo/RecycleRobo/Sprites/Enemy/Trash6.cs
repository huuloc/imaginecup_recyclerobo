﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using ProjectMercury;
using RecycleRobo.Scenes;

namespace RecycleRobo
{
    public class Trash6 : RangMonster
    {
        public Trash6(GameManager game, ContentManager Content, Vector2 initialPos, float initialHealth, float initialDamage, float initialSpeed, Player[] player)
            : base(game, Content, initialPos, initialHealth, initialDamage, initialSpeed, player)
        {
            range = 400;
            sprite.FrameToAction = 15;
            bulletTexture = Asset.bulletTrash6;

            soundReleaseBullet = Content.Load<SoundEffect>("sounds/trash/Trash6_attack");
            soundAttack = Content.Load<SoundEffect>("sounds/trash/Trash6_attack");
            soundDie = Content.Load<SoundEffect>("sounds/trash/Trash6_die");

            sewagesTexture = Content.Load<Texture2D>("Items/trash6_die");
            particleEffect = Content.Load<ParticleEffect>("Particles/hitByTrash4");
            particleEffect.LoadContent(Content);
            particleEffect.Initialise();
        }

        public override void setWalkAnimation()
        {
            CurAnimation = GlobalClass.getAnimationByName("TRASH6_WALK");
        }

        public override void setAttackAnimation()
        {
            CurAnimation = GlobalClass.getAnimationByName("TRASH6_ATTACK");
            sprite.PlayAnimation(CurAnimation);
        }

        public override void setDeathAnimation()
        {
            CurAnimation = GlobalClass.getAnimationByName("TRASH6_DIE");
        }

        public override Rectangle getRectangle()
        {
            return new Rectangle((int)Position.X, (int)Position.Y, (int)(CurAnimation.FrameWidth * sprite.Scale), (int)(sprite.Scale * CurAnimation.FrameHeight));
        }

        public override Rectangle getAttackRectangle()
        {
            Rectangle tmp = base.getAttackRectangle();
            return new Rectangle(tmp.X+30, tmp.Y-20, (int)(tmp.Width/2), tmp.Height);
        }

        public override Bullet createBullet(Vector2 target, Vector2 limitPoint)
        {
            return new PhysicalBullet(bulletTexture, world, Center, target, new Vector2(0, -220), limitPoint, damage, sprite.SpriteEffect);
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            base.Draw(gameTime, spriteBatch);
            if (isAttackable)
            {
                Vector2 pos = new Vector2((int)(Position.X + 60), (int)Position.Y + 30);
                spriteBatch.Draw(healthbar, pos, null, Color.LightGray, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0.17f);
                spriteBatch.Draw(healthbar, new Rectangle((int)pos.X, (int)pos.Y, (int)(healthbar.Width * (double)curHealth / Health), 4), 
                    null, Color.Red, 0f, Vector2.Zero, SpriteEffects.None, 0.15f);
            }
        }
    }
}
