﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;

namespace RecycleRobo
{
    public class SpriteManager
    {
        public Texture2D Texture;
        public Vector2 Position;
        public Color Color = Color.White;
        public Vector2 Origin;
        public float Rotation = 0f;
        public float Scale = 0.625f;
        public SpriteEffects SpriteEffect;

        public SpriteManager(ContentManager Content, String fileName)
        {
            Texture = Content.Load<Texture2D>(fileName);
        }
        public SpriteManager(Texture2D t){
            Texture = t;
        }

        virtual public void Update(GameTime gametime) { }

        virtual public void Draw(SpriteBatch spriteBatch) { }
    }
}
