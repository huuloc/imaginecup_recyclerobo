﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using RecycleRobo.Scenes;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
namespace RecycleRobo
{

    /// <summary>
    /// Stores the appearance and functionality of a button.
    /// </summary>
    public class Button
    {

        /// <summary>
        /// Describes the state of the button.
        /// </summary>
        public enum ButtonStatus
        {
            Normal,
            MouseOver,
            Pressed,
        }

        // Store the MouseState of the last frame.
        private MouseState previousState;

        // The the different state textures.
        public Texture2D hoverTexture;
        public Texture2D pressedTexture;
        public Texture2D texture;

        // A rectangle that covers the button.
        private Rectangle bounds;
        public float layer = 0.22f;
        public Vector2 position;
        public float scale = 1.0f;
        private float rotation = 0.0f;
        private float orderLayer = 1.0f;
        // Store the current state of the button.
        private ButtonStatus state = ButtonStatus.Normal;

        // Gets fired when the button is pressed.
        public event EventHandler Clicked;
        // Gets fired when the button is held down.
        public event EventHandler OnPress;
        public event EventHandler Hover;
        public event EventHandler NotHover;
        public bool isEnable = true;
        private bool isTime = false;
        private int timeEnable;
        private int timeCount = 0;
        public SpriteFont font;
        private Color color = Color.LightSlateGray;
        private Vector2 positionAlign = new Vector2(11f, 11f);

        //sound
        public static ContentManager Content;
        public SoundEffect soundButton;
        public bool isEnableSound = true;

        /// <summary>
        /// Constructs a new button.
        /// </summary>
        /// <param name="texture">The normal texture for the button.</param>
        /// <param name="hoverTexture">The texture drawn when the mouse is over the button.</param>
        /// <param name="pressedTexture">The texture drawn when the button has been pressed.</param>
        /// <param name="position">The position where the button will be drawn.</param>
        public Button(Texture2D texture, Texture2D hoverTexture, Texture2D pressedTexture, float scale, float rotation, Vector2 position)
        {
            this.hoverTexture = hoverTexture;
            this.pressedTexture = pressedTexture;
            this.texture = texture;
            this.position = position;
            this.scale = scale;
            this.rotation = rotation;
            this.bounds = new Rectangle((int)position.X - Convert.ToInt16(texture.Width * scale / 2), (int)position.Y - Convert.ToInt16(texture.Height * scale / 2), (int)(texture.Width * scale), (int)(texture.Height * scale));
            soundButton = Asset.soundButton;
            SoundEffect.MasterVolume = (float)GlobalClass.volumeSoundFx / 100;
        }

        /// <summary>
        /// Updates the buttons state.
        /// </summary>
        /// <param name="gameTime">The current game time.</param>
        public void Update(GameTime gameTime)
        {

            SoundEffect.MasterVolume = (float)GlobalClass.volumeSoundFx / 100;
            this.bounds = new Rectangle((int)position.X - Convert.ToInt16(texture.Width * scale / 2), (int)position.Y - Convert.ToInt16(texture.Height * scale / 2), (int)(texture.Width * scale), (int)(texture.Height * scale));
            // Determine if the mouse if over the button.
            MouseState mouseState = Mouse.GetState();

            int mouseX = MouseHelper.MousePosition(mouseState).X;
            int mouseY = MouseHelper.MousePosition(mouseState).Y;

            bool isMouseOver;
            if (bounds.Contains(mouseX, mouseY) && mouseX >= 0 && mouseY >= 0 && mouseX <= GlobalClass.ScreenWidth && mouseY <= GlobalClass.ScreenHeight)
            {
                isMouseOver = true;
            }
            else
                isMouseOver = false;

            if (timeEnable > 0)
            {
                timeCount += gameTime.ElapsedGameTime.Milliseconds / 16;
                if (timeCount % 60 == 59)
                {
                    timeEnable -= 1;
                    color = Color.LightSkyBlue;
                }
                else if (timeCount % 60 == 30)
                {
                    color = Color.LightSeaGreen;
                }
                else if (timeCount % 60 == 45)
                {
                    color = Color.LightCyan;
                }
                if (timeEnable < 10)
                    this.positionAlign = new Vector2(9f, 11f);
            }
            if (timeEnable == 0 && isTime)
                isEnable = true;
            // Update the button state.
            if (isEnable)
            {
                if (!isMouseOver)
                {
                    if (NotHover != null)
                    {
                        NotHover(this, EventArgs.Empty);
                    }
                }
                if (isMouseOver && state != ButtonStatus.Pressed)
                {
                    state = ButtonStatus.MouseOver;
                    if (Hover != null)
                    {
                        Hover(this, EventArgs.Empty);
                    }
                }

                else if (isMouseOver == false && state != ButtonStatus.Pressed)
                {

                    state = ButtonStatus.Normal;
                }

                // Check if the player holds down the button.
                if (mouseState.LeftButton == ButtonState.Pressed &&
                    previousState.LeftButton == ButtonState.Released)
                {
                    if (isMouseOver == true)
                    {
                        // Update the button state.
                        state = ButtonStatus.Pressed;

                        if (OnPress != null)
                        {
                            // Fire the OnPress event.
                            OnPress(this, EventArgs.Empty);
                        }
                    }
                }

                // Check if the player releases the button.
                if (mouseState.LeftButton == ButtonState.Released &&
                    previousState.LeftButton == ButtonState.Pressed)
                {
                    if (isMouseOver == true)
                    {
                        // update the button state.
                        state = ButtonStatus.MouseOver;

                        if (Clicked != null)
                        {
                            if (isEnableSound) soundButton.Play();
                            // Fire the clicked event.
                            Clicked(this, EventArgs.Empty);
                        }
                    }

                    else if (state == ButtonStatus.Pressed)
                    {
                        state = ButtonStatus.Normal;
                    }
                }
            }

            previousState = mouseState;
        }

        /// <summary>
        /// Draws the button.
        /// </summary>
        /// <param name="spriteBatch">A SpriteBatch that has been started</param>
        public void Draw(SpriteBatch spriteBatch)
        {
            switch (state)
            {
                case ButtonStatus.Normal:
                    this.DrawScale(spriteBatch, this.texture);
                    break;
                case ButtonStatus.MouseOver:
                    this.DrawScale(spriteBatch, this.hoverTexture);
                    break;
                case ButtonStatus.Pressed:
                    this.DrawScale(spriteBatch, this.pressedTexture);
                    break;
                default:
                    this.DrawScale(spriteBatch, this.texture);
                    break;
            }
        }
        public void DrawScale(SpriteBatch spriteBatch, Texture2D texture)
        {
            if (isEnable)
                spriteBatch.Draw(texture, this.position, null, Color.White, this.rotation, new Vector2(texture.Width / 2, texture.Height / 2), this.scale, SpriteEffects.None, layer);
            else
            {
                if (isTime)
                    spriteBatch.DrawString(font, Convert.ToString(timeEnable), this.position - this.positionAlign + new Vector2(texture.Width * scale / 2, texture.Height * scale / 2), Color.WhiteSmoke, 0f, Vector2.Zero, 0.75f, SpriteEffects.None, 0.23f);
                spriteBatch.Draw(texture, this.position, null, color, this.rotation, new Vector2(texture.Width / 2, texture.Height / 2), this.scale, SpriteEffects.None, layer);
            }
        }
        public void Disable()
        {
            isTime = false;
            isEnable = false;
        }
        public void Enable()
        {
            isTime = false;
            isEnable = true;
        }
        public void DisableWithTime(int time)
        {
            timeEnable = time;
            isTime = true;
            isEnable = false;
        }
        public Rectangle getBound()
        {
            return this.bounds;
        }
        public void setTexture(Texture2D tmp, Texture2D hover)
        {
            this.hoverTexture = hover;
            this.pressedTexture = tmp;
            this.texture = tmp;
        }
    }
}
