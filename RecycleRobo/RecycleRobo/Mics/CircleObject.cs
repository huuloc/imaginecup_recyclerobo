﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace RecycleRobo
{
    public class CircleObject
    {
        public Texture2D texture;
        public Vector2 position;
        public Vector2 center;
        public bool isAllowHover;
        public float scale = 0.625f;
        public float timeDelayRotate = 0, angleRotation = 0, PI = 3.14159f;
        public float layer = 0.96f;
        public float staticSpeed = 0.005f, speed;

        public CircleObject (Texture2D texture, Vector2 position, Vector2 center, bool isAllowHover)
        {
            this.texture = texture;
            this.position = position;
            if (center != Vector2.Zero) this.center = center;
            else this.center = new Vector2(texture.Width / 2, texture.Height / 2);
            this.isAllowHover = isAllowHover;
            speed = staticSpeed;
        }

        public void update(GameTime gameTime)
        {
            timeDelayRotate += gameTime.ElapsedGameTime.Milliseconds;
            if (timeDelayRotate > 9)
            {
                timeDelayRotate = 0;
                angleRotation -= speed;
            }
            Point mousePoint = new Point(MouseHelper.MousePosition(Mouse.GetState()).X, MouseHelper.MousePosition(Mouse.GetState()).Y);
            if (isHover(mousePoint)) speed = staticSpeed * 3;
            else speed = staticSpeed;
        }

        public void draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, position, null, Color.White, PI * angleRotation, center, scale, SpriteEffects.None, layer);
        }

        public bool isHover (Point mousePoint) 
        {
            if (!isAllowHover) return false;
            return getRectangle().Contains(mousePoint);
        }

        public Rectangle getRectangle()
        {
            return new Rectangle((int)(position.X - texture.Width * scale / 2), (int)(position.Y - texture.Height * scale / 2),
                                            (int)(texture.Width * scale), (int)(texture.Height * scale));
        }
    }
}
