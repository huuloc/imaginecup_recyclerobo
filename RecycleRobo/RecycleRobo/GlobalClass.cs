﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows.Forms;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Reflection;
using Microsoft.Xna.Framework;
using LoadData;

namespace RecycleRobo
{
    public class GlobalClass
    {
        public static bool isGotItFalse = false;
        public static Rectangle clippingRec;
        public static float fullWidthDisplay;
        public static float fullHeightDisplay;
        public static LevelData LevelInformation;
        public static int percentage = 0;
        public static bool isSetting = false;

        private static float screenWidth;
        public static float ScreenWidth
        {
            get { return screenWidth; }
            set { screenWidth = value; }
        }
        public static bool isFullScreen = false;
        private static float screenHeight;
        public static float ScreenHeight
        {
            get { return screenHeight; }
            set { screenHeight = value; }
        }
        public static bool isPause = false;
        public static bool isInMap = false;
        private static List<Node> totalAnimation;
        public static List<Node> ToTalAnimation
        {
            set { totalAnimation = value; }
        }

        private static List<RoboToSelect> robotSelected;
        public static List<RoboToSelect> RobotSelected
        {
            set { robotSelected = value; }
            get { return robotSelected; }
        }
        public static int volumeMusic = 80;
        public static int volumeSoundFx = 60;
        public static int challengeAccept = 0;
        public static int curLevelPlaying = 1;
        public static int curMapPlaying = 1;
        public static int timePlaying = 0;

        public static Animation getAnimationByName(String name)
        {
            foreach (Node node in totalAnimation)
            {
                if (node.AnimationName == name)
                {
                    return node.Animation;
                }
            }
            return null;
        }


        //change cursor
        public static Cursor LoadCustomCursor(string path)
        {
            IntPtr hCurs = LoadCursorFromFile(path);
            if (hCurs == IntPtr.Zero) throw new Win32Exception();
            var curs = new Cursor(hCurs);
            // Note: force the cursor to own the handle so it gets released properly
            var fi = typeof(Cursor).GetField("ownHandle", BindingFlags.NonPublic | BindingFlags.Instance);
            fi.SetValue(curs, true);
            return curs;
        }
        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        private static extern IntPtr LoadCursorFromFile(string path);
    }
}
